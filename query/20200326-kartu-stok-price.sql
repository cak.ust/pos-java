﻿CREATE OR REPLACE FUNCTION public.fn_rpt_kartu_stok_formula(
    varchar,
    character varying,
    character varying,
    character varying)
  RETURNS SETOF record AS
$BODY$
declare
	v_item	 	alias for $1;
	v_gudang	alias for $2;
	v_tanggal1	alias for $3;
	v_tanggal2	alias for $4;
	ritem	record;
	rcd		record;
	v_saldo_total	double precision;
	v_saldo_gudang	double precision;
	v_nilai_total	double precision;
	v_nilai_gudang	double precision;
	v_litem	int;
	v_lgudang int;
	
begin
for ritem in
	select i.id id_barang, coalesce(i.plu,'') plu, coalesce(i.nama_barang, '') nama_barang, 
	case when v_gudang='' then 0 else g.id end id_gudang,
	case when v_gudang='' then 'Semua Gudang' else coalesce(g.nama_gudang,'') end::varchar nama_gudang, null::date tanggal, 'SAW'::varchar txtype, '-'::varchar no_bukti, 
	'Saldo Awal'::varchar keterangan, 
	coalesce((select coalesce(h.sellingprice,0) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end order by itemhistid desc limit 1),0) sellingprice, 
	coalesce((select coalesce(h.item_cost,0) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end order by itemhistid desc limit 1),0) item_cost, 
	coalesce((select sum(coalesce(h.quantity,0)) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end),0) quantity, 0::bigint  itemhistid
	from m_item i, 
	(select id, nama_gudang from m_gudang where v_gudang<>'' and id = v_gudang::int UNION select 0::int id, ''::varchar nama_gudang from m_gudang where v_gudang='')g 
	WHERE coalesce(v_item,'')='' or i.id =v_item::int
	order by coalesce(i.nama_barang, ''), case when v_gudang='' then '' else coalesce(g.nama_gudang,'') end::varchar
	
	
LOOP
	if (ritem.id_gudang <> v_lgudang AND ritem.id_barang<>v_litem) then
		v_nilai_total = 0;
	end if;
	
	v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
	v_nilai_total=coalesce(v_nilai_total,0)+v_nilai_gudang;
	
	return QUERY select ritem.id_barang, ritem.plu, ritem.nama_barang, ritem.id_gudang, ritem.nama_gudang, --5
			ritem.tanggal, ritem.txtype, ritem.no_bukti, ritem.keterangan keterangan, ritem.sellingprice, ritem.item_cost, ritem.quantity, --12
			ritem.itemhistid itemhistid, 
			case when ritem.quantity>0 then ritem.quantity else 0 end masuk, 
			case when ritem.quantity<0 then abs(ritem.quantity) else 0 end keluar, 
			ritem.quantity, ritem.quantity, v_nilai_gudang, v_nilai_total;
	for rcd in
		select id_barang, id_gudang, tanggal, txtype, no_bukti, (coalesce(x.keterangan,'') || case when v_gudang='' then ' ['||coalesce(g.nama_gudang,'')||']' else '' end)::varchar keterangan, sum(sellingprice) sellingprice, 
		sum(item_cost) item_cost, sum(quantity) quantity, max(itemhistid) itemhistid
		from(
			select h.id_barang, h.warehouseid id_gudang, 
			h.txdate tanggal, h.txtype, coalesce(h.no_bukti,'') no_bukti, 
			h.description keterangan, coalesce(h.sellingprice,0) sellingprice, coalesce(h.item_cost,0) item_cost, 
			coalesce(h.quantity,0) quantity, h.itemhistid
			from item_history h 
			where h.id_barang=ritem.id_barang
			AND h.txdate>=v_tanggal1::date and h.txdate<=v_tanggal2::date 
			AND (v_gudang='' OR h.warehouseid=ritem.id_gudang)
			order by h.txdate, h.itemhistid
		)x
		left join m_gudang g on g.id = x.id_gudang
		group by id_barang, id_gudang, tanggal, txtype, no_bukti, (coalesce(x.keterangan,'') || case when v_gudang='' then ' ['||coalesce(g.nama_gudang,'')||']' else '' end)::varchar
		order by tanggal	
		
	LOOP
		raise notice 'v_litem: %, itemskg: %, lgudang: %, gdgskg: %', v_litem, rcd.id_barang, v_lgudang, rcd.id_gudang;
		
		if(coalesce(v_litem,0)<>rcd.id_barang) then
			v_saldo_total=coalesce(ritem.quantity,0);
			v_saldo_gudang=coalesce(ritem.quantity,0);
			v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
			v_nilai_total=v_nilai_gudang;
			v_litem = rcd.id_barang;
		end if;
		if(coalesce(v_lgudang,0)<>rcd.id_gudang) then
			v_saldo_gudang=coalesce(ritem.quantity,0);
			v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
			v_lgudang=rcd.id_gudang;
		end if;
		v_saldo_total=v_saldo_total+coalesce(rcd.quantity,0);
		v_saldo_gudang=v_saldo_gudang+coalesce(rcd.quantity,0);

		v_nilai_total = v_nilai_total+coalesce(rcd.quantity,0)*coalesce(rcd.item_cost,0);
		v_nilai_gudang = v_nilai_gudang+coalesce(rcd.quantity,0)*coalesce(rcd.item_cost,0);

		return QUERY select rcd.id_barang, ritem.plu, ritem.nama_barang, ritem.id_gudang, ritem.nama_gudang, 
		rcd.tanggal, rcd.txtype, rcd.no_bukti, rcd.keterangan, rcd.sellingprice, rcd.item_cost, rcd.quantity, 
		rcd.itemhistid, 
		case when rcd.quantity>0 then rcd.quantity else 0 end masuk, 
		case when rcd.quantity<0 then abs(rcd.quantity) else 0 end keluar, 
		v_saldo_gudang, v_saldo_total, v_nilai_gudang, v_nilai_total;

		v_litem = rcd.id_barang;
		v_lgudang=rcd.id_gudang;
		
	END LOOP;	
END LOOP;		
/*

select id_barang, count(*) from ar_inv_det group by id_barang order by count(*) desc
select id_barang, count(*) from item_history   group by id_barang order by count(*) desc

select * from fn_rpt_kartu_stok_formula('945', '', '2020-01-01', '2020-12-31') as (id_barang integer, plu varchar, nama_barang varchar, id_gudang integer,
nama_gudang varchar, tanggal date, trx_type varchar, no_bukti varchar, keterangan varchar, selling_price double precision, item_cost double precision,
quantity double precision, id bigint, masuk double precision, keluar double precision, saldo_gudang double precision, 
saldo_total double precision, nilai_gudang double precision, nilai_total double precision)

select id_barang, count(distiNct warehouseid), COUNT(*) from item_history
group by id_barang
having count(distiNct warehouseid) > 1
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE OR REPLACE FUNCTION public.fn_rpt_stok_saldo(
    character varying,
    character varying,
    character varying,
    character varying)
  RETURNS SETOF record AS
$BODY$
declare
	v_item_awal	alias for $1;
	v_item_akhir	alias for $2;
	v_gudang	alias for $3;
	v_periode	alias for $4;
	rcd		record;
begin
for rcd in
	select case when v_gudang='' then 'Semua Gudang' else coalesce(g.nama_gudang,'') end ::varchar as nama_gudang, coalesce(i.plu,'') as plu, coalesce(i.nama_barang,'') as nama_barang, coalesce(i.satuan,'') as satuan, 
	--sum(coalesce(ih.qtycontrol,0)) as qty, sum(coalesce(ih.qtycontrol,0)*coalesce(ih.item_cost,0)) as harga
	sum(coalesce(ih.quantity,0)) as qty, sum(coalesce(ih.quantity,0)*coalesce(ih.item_cost,0)) as harga
	from item_history ih
	left join m_item i on i.id=ih.id_barang
	left join m_gudang g on g.id=ih.warehouseid
	where --ih.qtycontrol > 0 and 
	to_char(txdate, 'yyyy-MM-dd')<=v_periode
	--and coalesce(i.plu,'')>=v_item_awal
	--and coalesce(i.plu,'')<=v_item_akhir
	and case when v_gudang='' then 1=1 else ih.warehouseid=v_gudang::integer end
	group by case when v_gudang='' then 'Semua Gudang' else coalesce(g.nama_gudang,'') end ::varchar, coalesce(i.plu,''), coalesce(i.nama_barang,'') , coalesce(i.satuan,'')
	order by case when v_gudang='' then 'Semua Gudang' else coalesce(g.nama_gudang,'') end ::varchar, coalesce(i.nama_barang,'')
LOOP
	return next rcd;
END LOOP;	
/*
select * from fn_rpt_stok_saldo('S 293-20', 'S 293-20', '', '2020-12-31') as (nama_gudang varchar, plu varchar, nama_barang varchar, satuan varchar, qty double precision, harga double precision)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


  CREATE OR REPLACE FUNCTION public.fn_rpt_stok_mutasi(
    character varying,
    character varying,
    character varying,
    character varying,
    character varying)
  RETURNS SETOF record AS
$BODY$
declare
	v_tanggal1	alias for $1;
	v_tanggal2	alias for $2;
	v_gudang	alias for $3;
	v_item1		alias for $4;
	v_item2		alias for $5;
	rcd		record;
begin
for rcd in
	select coalesce(g.nama_gudang,'') as nama_gudang, coalesce(i.plu,'') as plu, i.nama_barang, i.satuan, sum(x.saldo_awal) as saldo_awal, sum(x.masuk) as masuk, sum(x.keluar) as keluar, 
	sum(x.saldo_awal+x.masuk-x.keluar) as saldo_sblm_penyesuaian, sum(penyesuaian) as penyesuaian, 
	sum(x.saldo_awal+x.masuk-x.keluar+penyesuaian) as saldo_akhir
	from(
		select case when v_gudang='' then '' else warehouseid::varchar end as id_gudang, ks.id_barang, sum(quantity)  as saldo_awal, 0 as masuk, 0 as keluar, 0 as penyesuaian 
		from item_history ks
		where to_Char(txdate, 'yyyy-MM-dd')<v_tanggal1
		and case when v_gudang='' then 1=1 else ks.warehouseid::varchar=v_gudang end
		group by ks.id_barang, case when v_gudang='' then '' else warehouseid::varchar end

		UNION ALL

		select case when v_gudang='' then '' else warehouseid::varchar end, ks.id_barang, 0 as saldo_awal, sum(case when quantity>0 then quantity else 0 end) as masuk, 
		sum(case when quantity<0 then abs(quantity) else 0 end) as keluar, 0 as penyesuaian 
		from item_history ks
		where to_Char(txdate, 'yyyy-MM-dd')>=v_tanggal1
		and to_Char(txdate, 'yyyy-MM-dd')<=v_tanggal2
		and case when v_gudang='' then 1=1 else ks.warehouseid::varchar=v_gudang end
		and txtype<>'TRF'
		group by ks.id_barang, case when v_gudang='' then '' else warehouseid::varchar end

		UNION ALL

		select case when v_gudang='' then '' else warehouseid::varchar end, ks.id_barang, 0  as saldo_awal, 0 as masuk, 0 as keluar, sum(quantity) as penyesuaian from item_history ks
		where to_Char(txdate, 'yyyy-MM-dd')>=v_tanggal1
		and to_Char(txdate, 'yyyy-MM-dd')<=v_tanggal2
		and case when v_gudang='' then 1=1 else ks.warehouseid::varchar=v_gudang end
		and txtype IN ('TRF')
		group by ks.id_barang, case when v_gudang='' then '' else warehouseid::varchar end
	)x
	inner join m_item i on i.id=x.id_barang
	left join m_gudang g on g.id::varchar=x.id_gudang
	where case when v_item1='' then true else i.id=v_item1::int end
	--and coalesce(i.plu,'')<=v_item2
	group by coalesce(g.nama_gudang,''), coalesce(i.plu,''), i.nama_barang, i.satuan
	order by coalesce(g.nama_gudang,''), i.nama_barang
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_rpt_stok_mutasi('2013-04-05', '2013-05-01', '4', '', 'Z') as (nama_gudang varchar, plu varchar, nama_barang varchar, satuan varchar, saldo_awal double precision, 
masuk double precision, keluar double precision, saldo_sebelum_penyesuaian double precision, penyesuaian double precision, saldo_akhir double precision)
*/
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
  