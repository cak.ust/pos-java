﻿create or replace function fn_rpt_cetak_barcode(p_id text, p_jml int) 
returns setof record
as
$$
declare
	rcd record;
	ritem record;
	v_i int;
begin
for ritem in execute
	'select plu, nama_barang, coalesce(harga_jual, 0) harga_jual from m_item where id in (select unnest('||p_id||')) 
	order by nama_barang'
LOOP	
	--raise notice 'Item %', ritem.plu;
	v_i = 0;
	LOOP 
	--	raise notice 'v_i: %, p_jml: %', v_i, p_jml;
		exit when v_i = p_jml;
		v_i = v_i + 1;
		return QUERY select ritem.plu, ritem.nama_barang, ritem.harga_jual;
		
	end loop;	
END LOOP;	
--select * from fn_rpt_cetak_barcode(ARRAY[1, 2, 3], 10) as (kode varchar, nama_barang varchar, harga double precision)
end
$$
language 'plpgsql';
