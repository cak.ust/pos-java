﻿CREATE OR REPLACE FUNCTION public.fn_get_no_opname(varchar)
  RETURNS character varying AS
$BODY$
begin
return to_char($1::date, 'yyMMdd')||trim(to_Char((select count(*) from opname where tanggal = $1::date)+1, '00'));
end
--select fn_get_no_opname()
$BODY$
  LANGUAGE plpgsql VOLATILE;

  