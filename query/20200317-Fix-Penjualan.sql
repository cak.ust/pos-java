﻿alter table m_setting_penjualan  add column jumlah_cetak_faktur int default 1;

CREATE OR REPLACE FUNCTION public.fn_ar_retur_lookup(integer)
  RETURNS SETOF record AS
$BODY$
declare
	v_arid	alias for $1;
	rcd	record;
begin
for rcd in
	select coalesce(i.plu,'') as plu, coalesce(i.nama_barang,'') as nama_barang, sum(qty) as qty_sisa, coalesce(i.satuan,'') as satuan,
	d.unit_price-case when isnumeric(d.disc) then d.disc::double precision else 0 end, coalesce(d.biaya,0) as biaya, coalesce(d.unit_cost,0) as unit_cost, coalesce(d.biaya_item,0) as biaya_item, d.id_barang 
	from ar_inv_det  d
	inner join ar_inv h on h.id=d.ar_id
	inner join m_item i on i.id=d.id_barang
	where (h.id=v_arid and h.trx_type='PJL') or (h.trx_type='RJL' and h.retur_dari=v_arid and coalesce(h.retur_dari,0)<>0)
	group by d.id_barang, coalesce(i.plu, '') , coalesce(i.nama_barang,''), d.unit_price, d.seq, coalesce(i.satuan,''), 
	coalesce(d.biaya,0), coalesce(d.unit_cost,0), case when isnumeric(d.disc) then d.disc::double precision else 0 end, coalesce(d.biaya_item,0)
	having sum(qty)>0
	order by d.seq
	
LOOP
	return next rcd;
END LOOP;

/*
select * from fn_ar_retur_lookup(416) as (kode varchar, nama_barang varchar, qty double precision, satuan varchar, 
harga double precision, biaya double precision, hpp double precision, biaya_item double precision, id_barang integer)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;