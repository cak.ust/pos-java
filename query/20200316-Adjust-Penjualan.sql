﻿CREATE OR REPLACE FUNCTION public.fn_get_no_ar_inv_dj(
    boolean,
    character varying)
  RETURNS character varying AS
$BODY$
declare 
	v_is_kredit 	alias for $1;
	v_tanggal	alias for $2;
	v_debet_cr 	varchar;
	v_new_code	varchar;
begin
v_debet_cr := case when v_is_kredit=false then 'CS' else 'CR' end;

select into v_new_code v_debet_cr || to_char(v_tanggal::date, 'yyMMdd')||'-'||ltrim(to_char(substring(invoice_no from '....$')::int +1 , '0000'))
	from ar_inv 
	where invoice_no iLike v_debet_cr || to_char(v_tanggal::date, 'yyMMdd')||'%'
	order by invoice_no DESC limit 1;
--select fn_r_get_sales_no(false, '2020-03-01')
IF v_new_code is null THEN
	v_new_code := v_debet_cr||to_char(v_tanggal::date, 'yyMMdd')||'-'||'0001';
END IF;

return v_new_code;

end
$BODY$
  LANGUAGE plpgsql VOLATILE;
