﻿--create extension dblink;

insert into m_item_kategori(nama_kategori)
SELECT kat.*
FROM dblink('dbname=dutajaya port=5432 user=test password=test','select kategori from(select distinct upper(trim(coalesce(kategori,''''))) kategori from r_item i order by 1) x')
    AS kat(nama varchar);

insert into m_satuan(satuan)
SELECT u.*
FROM dblink('dbname=dutajaya port=5432 user=test password=test','select unit from(select distinct upper(trim(coalesce(unit,''''))) unit from r_item i order by 1) x')
    AS u(unit varchar);

insert into m_item (plu, nama_barang, id_kategori, satuan, 
       id_satuan, harga_jual, hpp, biaya_item)
select item.kode_item, item.nama_item, (select id from m_item_kategori where nama_kategori=upper(trim(coalesce(item.kategori,'''')))) as id_kategori, item.unit, 
(select id from m_satuan where satuan=upper(trim(coalesce(item.unit,''''))))  id_satuan, item.harga_jual , item.hpp, item.biaya_item
FROM dblink('dbname=dutajaya port=5432 user=test password=test','select kode_item, nama_item, kategori, unit, harga_jual, hpp, biaya_item from r_item order by kode_item')
    AS item(kode_item varchar, nama_item varchar, kategori varchar, unit varchar, harga_jual double precision, hpp double precision, biaya_item double precision);

insert into m_item_harga_jual (id_item, id_tipe_harga_jual, harga_jual, margin)
select id, 1, harga_jual, case when coalesce(hpp,0)=0 or coalesce(harga_jual,0)=0 then 0 else (coalesce(harga_jual,0)-coalesce(hpp,0))/coalesce(hpp,0)*100 end
from m_item;

INSERT INTO public.m_relasi(
            tipe_relasi, nama_relasi, alamat, id_kota, kontak, 
            telp, email, no_npwp, active, fax, kode, 
            is_cust, is_supp, id_tipe_harga_jual)
select 0, 'CASH', '-', null, '', '', '', '', true, '', '0000', true, true, 1
UNION ALL            
select 1 tipe_relasi, nama_supp, alamat_1, id_kota, kontak,
telepon, email, npwp, active, fax, kode_supp, 
false, true, 1
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select nama_supp, alamat_1, case when trim(upper(kota)) in (''SBY'', ''SURABAYA'') then 56 else null end id_kota, kontak,
telepon, email, npwp, active, fax, kode_supp
from r_supplier ')
    AS supp(nama_supp varchar, alamat_1 varchar, id_kota int, kontak varchar, telepon varchar, email varchar, npwp varchar, active boolean, 
    fax varchar, kode_supp varchar);


DROP TRIGGER tg_ar_inv_det_ins_upd ON public.ar_inv_det;
DROP TRIGGER tg_ap_inv_det ON public.ap_inv_det;


INSERT INTO public.ap_inv(
supplier_id, invoice_no, invoice_date, top, 
invoice_amount, item_amount, paid_amount, owing, id_gudang,  
time_ins, user_ins, trx_type) 
select (select id_relasi from m_relasi where kode=gr.kode_supp) as supp_id, no_gr, tanggal, 0 as top, 
item_amount, item_amount, item_amount, 0, 1, now(), 'system', 'PBL' as trx_type
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select h.no_gr, h.tanggal, h.kode_supp, 
sum(case when is_disc_rp then (qty*unit_price - coalesce(disc,0))/qty else (unit_price*(1-coalesce(disc,0)/100)) end * coalesce(d.qty,0)) as item_amount
from r_gr h 
join r_gr_detail d on d.no_gr=h.no_gr
where to_char(tanggal, ''yyyy'')>=''2019''
group by h.no_gr, h.tanggal, h.kode_supp
order by h.tanggal, h.no_gr')
AS gr(no_gr varchar, tanggal date, kode_supp varchar, item_amount double precision);

INSERT INTO public.ap_inv_det(
ap_id, id_barang, qty, unit_price, disc, unit_cost, seq)

select (select id from ap_inv  where invoice_no  =grd.no_gr) as ap_id, 
(select id from m_item where plu=grd.kode_item) as id_barang, grd.qty, grd.unit_price, grd.disc, grd.unit_price, urut
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select d.no_gr, d.kode_item, d.qty, d.unit_price, d.disc, d.urut 
from r_gr_detail d 
inner join r_gr h on h.no_gr=d.no_gr
where to_char(tanggal, ''yyyy'')>=''2019''
order by h.tanggal, h.no_gr')
AS grd(no_gr varchar, kode_item varchar, qty numeric, unit_price double precision, disc double precision, urut int);

INSERT INTO public.ar_inv(
invoice_no, id_customer, invoice_date, jenis_bayar, 
top, invoice_amount, item_amount, paid_amount, owing, id_gudang, 
trx_type, time_ins, user_ins, shift)
select sales_no, 1, coalesce(sales_date, substring(sales_no, 3, 6)::date), 'T',0, total, total, total, 0 , 1, 
'PJL', now(), 'system', 'P'
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select h.sales_no, sales_date, sum(coalesce(d.qty,0)*coalesce(d.unit_price,0)) as total 
from r_sales h
inner join r_sales_detail d on d.sales_no=h.sales_no
where to_char(sales_date, ''yyyy'')>=''2019''
group by h.sales_date, h.sales_no')
AS grd(sales_no varchar, sales_date date, total double precision);



INSERT INTO public.ar_inv_det(
ar_id, id_barang, qty, unit_price, disc, unit_cost, biaya_item, seq)

select (select id from ar_inv  where invoice_no  =det.sales_no) as ar_id, 
(select id from m_item where plu=det.kode_item) as id_barang, det.qty, det.unit_price, '0' disc, det.hpp, det.biaya_item, ROW_NUMBER () OVER (ORDER BY det.sales_no)
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select d.sales_no, d.kode_item, d.qty, d.unit_price, d.unit_jual, d.biaya_item, d.hpp
from r_sales h 
inner join r_sales_detail d on d.sales_no=h.sales_no 
where to_char(sales_date, ''yyyy'')>=''2019''
order by h.sales_date, h.sales_no')
AS det(sales_no varchar, kode_item varchar, qty numeric, unit_price double precision, unit_jual varchar, biaya_item double precision, 
hpp double precision);

/*
INSERT INTO public.item_history(
id_barang, itemno, txdate, txtype, description, 
quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
glperiod, glyear, warehouseid)

select (select id from m_item where plu=item.kode_item) id_barang, ROW_NUMBER () OVER (ORDER BY item.kode_item) as itemno, '2019-12-31' tanggal, 
'SAW' txtype, 'Saldo akhir per 31/12/2019' as description,
item.saldo, item.harga_sat, item.hpp, item.hpp, item.hpp, 0, '12' gl_period, '2019' glyear, 1 warehouseid
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select x.kode_item, ks.tanggal, ks.saldo, ks.harga_sat, ks.hpp
from(
	select kode_item, max(serial_no) id from r_kartu_stok
	where tanggal<=''2019-12-31''
	group by kode_item
)x 
join r_kartu_stok ks on ks.serial_no=x.id
order by x.kode_item')
    AS item(kode_item varchar, tanggal date, saldo double precision, harga_sat double precision, hpp double precision);
*/
--06.47 minutes

INSERT INTO public.item_history(
            id_barang, txdate, txtype, description, 
            quantity, sellingprice, item_cost, costavr, prevcost,  
            glperiod, glyear, warehouseid,
            no_bukti)
select (select id from m_item where plu=ks.kode_item) id_barang, ks.tanggal, 
ks.tipe, ks.keterangan, ks.quantity, ks.harga_sat, ks.hpp, ks.hpp, ks.hpp, ks.gl_period::smallint, ks.glyear::integer, 1 warehouseid, source_no
FROM dblink('dbname=dutajaya port=5432 user=test password=test',
'select kode_item, ks.tanggal, ks.tipe, case when ks.tipe=''SA'' then ''CUT-OFF PER 31/12/2018'' else ks.keterangan end keterangan, 
sum(coalesce(masuk,0)-coalesce(keluar,0)) as quantity, harga_sat, hpp, 
to_char(tanggal, ''MM'') glperiod, to_char(tanggal, ''YYYY'') glyear, source_no 
from r_kartu_stok ks
group by ks.kode_item, ks.tanggal, ks.tipe, case when ks.tipe=''SA'' then ''CUT-OFF PER 31/12/2018'' else ks.keterangan end, 
ks.harga_sat, ks.hpp, to_char(tanggal, ''MM''), to_char(tanggal, ''YYYY''), source_no ')
AS ks(kode_item varchar, tanggal date, tipe varchar, keterangan varchar, quantity numeric, harga_sat double precision, 
hpp double precision, gl_period text, glyear text, source_no varchar);
   
CREATE TRIGGER tg_ap_inv_det
  AFTER INSERT OR UPDATE
  ON public.ap_inv_det
  FOR EACH ROW
  EXECUTE PROCEDURE public.fn_tg_ap_inv_det();
  
CREATE TRIGGER tg_ar_inv_det_ins_upd
  AFTER INSERT OR UPDATE
  ON public.ar_inv_det
  FOR EACH ROW
  EXECUTE PROCEDURE public.fn_tg_ar_inv_det();
  