﻿
CREATE OR REPLACE FUNCTION public.fn_get_harga_item_pelanggan(
    v_item integer,
    v_pelanggan integer,
    v_gudang integer)
  RETURNS SETOF record AS
$BODY$
declare
	rcd record;
	v_stok double precision;
begin	
select into v_stok sum(coalesce(quantity,0)) from item_history  where id_barang=v_item and warehouseid=v_gudang;

for rcd in
	select i.id, coalesce(i.plu,'') plu, coalesce(i.barcode,'') barcode, i.nama_barang, coalesce(i.satuan,'') satuan,  
	coalesce(i.hpp,0) hpp, coalesce(
	(select harga_jual from m_item_harga_jual h inner join m_relasi r on r.id_tipe_harga_jual=h.id_tipe_harga_jual 
	where id_item=v_item and r.id_relasi=v_pelanggan), i.harga_jual) harga_jual, 0::double precision disc, 0::double precision ppn, 
	coalesce(i.biaya_item,0) biaya_item, coalesce(v_stok,0)
	from m_item i
	where i.id=v_item
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_get_harga_item_pelanggan(2890, 1, 1) as (id integer, plu varchar, barcode varchar, nama_barang varchar, satuan varchar, hpp double precision, 
harga_jual double precision, disc double precision, ppn double precision, biaya_item double precision, stok double precision)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

alter table ar_inv_det  add column biaya_item double precision default 0;