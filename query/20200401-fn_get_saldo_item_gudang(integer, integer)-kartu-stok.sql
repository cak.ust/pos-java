﻿-- Function: public.fn_get_saldo_item_gudang(integer, integer)

-- DROP FUNCTION public.fn_get_saldo_item_gudang(integer, integer);

CREATE OR REPLACE FUNCTION public.fn_get_saldo_item_gudang(
    integer,
    integer)
  RETURNS double precision AS
$BODY$
declare
	v_item	alias for $1;
	v_gudang alias for $2;
	v_stock double precision;
begin
select into v_stock sum(coalesce(quantity,0))	
from item_history where id_barang=v_item and warehouseid=v_gudang;

return coalesce(v_stock,0);

--select fn_get_saldo_item_gudang(3843, 1)
--SELECT 
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
CREATE OR REPLACE FUNCTION public.fn_rpt_kartu_stok_formula(
    character varying,
    character varying,
    character varying,
    character varying)
  RETURNS SETOF record AS
$BODY$
declare
	v_item	 	alias for $1;
	v_gudang	alias for $2;
	v_tanggal1	alias for $3;
	v_tanggal2	alias for $4;
	ritem	record;
	rcd		record;
	v_saldo_total	double precision;
	v_saldo_gudang	double precision;
	v_nilai_total	double precision;
	v_nilai_gudang	double precision;
	v_litem	int;
	v_lgudang int;
	
begin
for ritem in
	select i.id id_barang, coalesce(i.plu,'') plu, coalesce(i.nama_barang, '') nama_barang, 
	case when v_gudang='' then 0 else g.id end id_gudang,
	case when v_gudang='' then 'Semua Gudang' else coalesce(g.nama_gudang,'') end::varchar nama_gudang, null::date tanggal, 'SAW'::varchar txtype, '-'::varchar no_bukti, 
	'Saldo Awal'::varchar keterangan, 
	coalesce((select coalesce(h.sellingprice,0) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end order by itemhistid desc limit 1),0) sellingprice, 
	coalesce((select coalesce(h.item_cost,0) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end order by itemhistid desc limit 1),0) item_cost, 
	coalesce((select sum(coalesce(h.quantity,0)) from item_history h where id_barang=i.id and h.txdate<v_tanggal1::date and case when v_gudang='' then true else h.warehouseid=v_gudang::int end),0) quantity, 0::bigint  itemhistid
	from m_item i, 
	(select id, nama_gudang from m_gudang where v_gudang<>'' and id = v_gudang::int UNION select 0::int id, ''::varchar nama_gudang from m_gudang where v_gudang='')g 
	WHERE coalesce(v_item,'')='' or i.id =v_item::int
	order by coalesce(i.nama_barang, ''), case when v_gudang='' then '' else coalesce(g.nama_gudang,'') end::varchar
	
	
LOOP
	if (ritem.id_gudang <> v_lgudang AND ritem.id_barang<>v_litem) then
		v_nilai_total = 0;
	end if;
	
	v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
	v_nilai_total=coalesce(v_nilai_total,0)+v_nilai_gudang;
	
	return QUERY select ritem.id_barang, ritem.plu, ritem.nama_barang, ritem.id_gudang, ritem.nama_gudang, --5
			ritem.tanggal, ritem.txtype, ritem.no_bukti, ritem.keterangan keterangan, ritem.sellingprice, ritem.item_cost, ritem.quantity, --12
			ritem.itemhistid itemhistid, 
			case when ritem.quantity>0 then ritem.quantity else 0 end masuk, 
			case when ritem.quantity<0 then abs(ritem.quantity) else 0 end keluar, 
			ritem.quantity, ritem.quantity, v_nilai_gudang, v_nilai_total;
	for rcd in
		select id_barang, id_gudang, tanggal, txtype, no_bukti, (coalesce(x.keterangan,'') || case when v_gudang='' then ' ['||coalesce(g.nama_gudang,'')||']' else '' end)::varchar keterangan, sum(sellingprice) sellingprice, 
		sum(item_cost) item_cost, sum(quantity) quantity, max(itemhistid) itemhistid
		from(
			select h.id_barang, h.warehouseid id_gudang, 
			h.txdate tanggal, h.txtype, coalesce(h.no_bukti,'') no_bukti, 
			h.description keterangan, coalesce(h.sellingprice,0) sellingprice, coalesce(h.item_cost,0) item_cost, 
			coalesce(h.quantity,0) quantity, h.itemhistid
			from item_history h 
			where h.id_barang=ritem.id_barang
			AND h.txdate>=v_tanggal1::date and h.txdate<=v_tanggal2::date 
			AND (v_gudang='' OR h.warehouseid=ritem.id_gudang)
			order by h.txdate, h.itemhistid
		)x
		left join m_gudang g on g.id = x.id_gudang
		group by id_barang, id_gudang, tanggal, txtype, no_bukti, (coalesce(x.keterangan,'') || case when v_gudang='' then ' ['||coalesce(g.nama_gudang,'')||']' else '' end)::varchar
		order by tanggal, itemhistid	
		
	LOOP
		raise notice 'v_litem: %, itemskg: %, lgudang: %, gdgskg: %', v_litem, rcd.id_barang, v_lgudang, rcd.id_gudang;
		
		if(coalesce(v_litem,0)<>rcd.id_barang) then
			v_saldo_total=coalesce(ritem.quantity,0);
			v_saldo_gudang=coalesce(ritem.quantity,0);
			v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
			v_nilai_total=v_nilai_gudang;
			v_litem = rcd.id_barang;
		end if;
		if(coalesce(v_lgudang,0)<>rcd.id_gudang) then
			v_saldo_gudang=coalesce(ritem.quantity,0);
			v_nilai_gudang=coalesce(ritem.quantity,0)*coalesce(ritem.item_cost,0);
			v_lgudang=rcd.id_gudang;
		end if;
		v_saldo_total=v_saldo_total+coalesce(rcd.quantity,0);
		v_saldo_gudang=v_saldo_gudang+coalesce(rcd.quantity,0);

		v_nilai_total = v_nilai_total+coalesce(rcd.quantity,0)*coalesce(rcd.item_cost,0);
		v_nilai_gudang = v_nilai_gudang+coalesce(rcd.quantity,0)*coalesce(rcd.item_cost,0);

		return QUERY select rcd.id_barang, ritem.plu, ritem.nama_barang, ritem.id_gudang, ritem.nama_gudang, 
		rcd.tanggal, rcd.txtype, rcd.no_bukti, rcd.keterangan, rcd.sellingprice, rcd.item_cost, rcd.quantity, 
		rcd.itemhistid, 
		case when rcd.quantity>0 then rcd.quantity else 0 end masuk, 
		case when rcd.quantity<0 then abs(rcd.quantity) else 0 end keluar, 
		v_saldo_gudang, v_saldo_total, v_nilai_gudang, v_nilai_total;

		v_litem = rcd.id_barang;
		v_lgudang=rcd.id_gudang;
		
	END LOOP;	
END LOOP;		
/*

select id_barang, count(*) from ar_inv_det group by id_barang order by count(*) desc
select id_barang, count(*) from item_history   group by id_barang order by count(*) desc

select * from fn_rpt_kartu_stok_formula('945', '', '2020-01-01', '2020-12-31') as (id_barang integer, plu varchar, nama_barang varchar, id_gudang integer,
nama_gudang varchar, tanggal date, trx_type varchar, no_bukti varchar, keterangan varchar, selling_price double precision, item_cost double precision,
quantity double precision, id bigint, masuk double precision, keluar double precision, saldo_gudang double precision, 
saldo_total double precision, nilai_gudang double precision, nilai_total double precision)

select id_barang, count(distiNct warehouseid), COUNT(*) from item_history
group by id_barang
having count(distiNct warehouseid) > 1
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

  