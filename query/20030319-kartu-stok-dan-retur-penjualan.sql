﻿CREATE OR REPLACE FUNCTION public.fn_tg_ar_inv_det()
  RETURNS trigger AS
$BODY$
declare
	r_arinv		record;
	r_stok		record;
	v_priority 	int;
	v_qty_jual	double precision;
	v_qty_sisa	double precision;
	v_qty_pakai	double precision;
	v_tot_qty_so	double precision;
	v_tot_qty_jual	double precision;
	
begin
v_qty_jual:=0;
v_qty_sisa:=0;

if(TG_OP='INSERT' or TG_OP='UPDATE') then
	select into r_arinv i.*, t.keterangan, m.nama_relasi as nama_customer --t.kode=i.trx_type
	from ar_inv i
	left join m_trx_type t on t.kode=i.trx_type
	left join m_relasi m on m.id_relasi=i.id_customer
	where id=NEW.ar_id;

	if (coalesce(r_arinv.no_so,'')='') then
		select into v_tot_qty_jual sum(qty) from ar_inv_det d
		inner join ar_inv h on h.id=ar_id and h.no_so=r_arinv.no_so;

		select into v_tot_qty_so sum(qty) from so_Det where no_so=r_arinv.no_so;

		if(v_tot_qty_jual>=v_tot_qty_so) then
			update so set closed=true where no_so=r_arinv.no_so;
		end if;	
	end if;
	if(exists(select * from m_item_price_by_customer where id_barang = NEW.id_barang and  id_customer=r_arinv.id_customer)) then
		UPDATE m_item_price_by_customer
		   SET price=NEW.unit_price, disc=NEW.disc, ppn=NEw.ppn, last_trx_id=NEW.ar_id
		WHERE id_barang = NEW.id_barang and  id_customer=r_arinv.id_customer;
	else
		INSERT INTO m_item_price_by_customer(
		id_barang, id_customer, price, disc, ppn, last_trx_id)
		VALUES (NEW.id_barang, r_arinv.id_customer, NEw.unit_price, NEW.disc, NEW.ppn, NEw.ar_id);
	end if;

	if r_arinv.id_sales is not null then
		if(exists(select * from m_item_price_by_salesman where id_barang = NEW.id_barang and  id_sales=r_arinv.id_sales)) then
			UPDATE m_item_price_by_salesman
			   SET price=NEW.unit_price, disc=NEW.disc, ppn=NEw.ppn, last_trx_id=NEW.ar_id
			WHERE id_barang = NEW.id_barang and  id_sales=r_arinv.id_sales;
		else
			INSERT INTO m_item_price_by_salesman(
			id_barang, id_sales, price, disc, ppn, last_trx_id)
			VALUES (NEW.id_barang, r_arinv.id_sales, NEw.unit_price, NEW.disc, NEW.ppn, NEw.ar_id);
		end if;
	end if;
	if(TG_OP='INSERT') then
		if r_arinv.trx_type='PJL' THEN
			v_qty_jual=coalesce(NEW.qty,0);
			v_qty_sisa=coalesce(NEW.qty,0);
			raise notice 'Qty: %', coalesce(NEW.qty,0);
			/*
			for r_stok in
				select * from item_history where id_barang=NEW.id_barang and qtycontrol>0 and warehouseid=r_arinv.id_gudang order by txdate, itemhistid
			LOOP
				v_qty_pakai:=case when r_stok.qtycontrol>=v_qty_sisa then v_qty_sisa else r_stok.qtycontrol end;
				raise notice 'Masih ada %, Pakai %', r_stok.qtycontrol, v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    -v_qty_pakai, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), r_stok.item_cost, 0, 0, v_qty_pakai,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.itemhistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;

				update item_history set qtycontrol=r_stok.qtycontrol-v_qty_pakai
				where itemhistid=r_stok.itemhistid;

			
				v_qty_sisa=v_qty_sisa-v_qty_pakai;

				if(v_qty_sisa<=0) then
					exit;
				end if;
			END LOOP;	
			*/
			if(v_qty_sisa<>0) then
				raise notice 'Masih sisa: %', v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    -v_qty_sisa, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    coalesce(i.hpp,0) hpp, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, null, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;
			end if;
		END IF;
		IF r_arinv.trx_type='RJL' THEN
			v_qty_jual=abs(coalesce(NEW.qty,0));
			v_qty_sisa=abs(coalesce(NEW.qty,0));
			raise notice 'Qty Retur: %', coalesce(NEW.qty,0);
			/*
			for r_stok in
				select * from item_history where id_barang=NEW.id_barang and invoiceid=r_arinv.retur_dari 
				and prevqty>0
				order by itemhistid desc
			LOOP
				
				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id, retur_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' - '||i.nama_barang , 
					    case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else -r_stok.quantity end, 
					    (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    r_stok.item_cost, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.fifohistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id, r_stok.itemhistid
				from m_Item i where i.id=NeW.id_barang;

				update item_history set qtycontrol=qtycontrol+case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else abs(r_stok.quantity) end
				where itemhistid=r_stok.fifohistid;

				update item_history set prevqty=prevqty-case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else -r_stok.quantity end
				where itemhistid=r_stok.itemhistid;
				
				v_qty_sisa=v_qty_sisa+r_stok.quantity;

				if(v_qty_sisa<=0) then
					exit;
				end if;
			END LOOP;
			*/
			if(v_qty_sisa<>0) then
				raise notice 'Masih sisa: %', v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    v_qty_sisa, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    coalesce(i.hpp,0) hpp, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.itemhistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;
			end if;
		END IF;
		return NEW;
	end if;		
	if(TG_OP='UPDATE') then
		UPDATE item_history 
			set 	id_barang=new.id_barang, 
				txdate=r_arinv.invoice_date, 
				txtype=r_arinv.trx_type, 
				invoiceid=r_arinv.id, 
				description=r_arinv.keterangan||' '||i.nama_barang, 
				quantity=NEW.qty, 
				sellingprice=(coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
				item_cost=coalesce(i.hpp,0), 
				costavr=0, 
				prevcost=0, 
				prevqty=0, 
				glperiod=to_char(r_arinv.invoice_date, 'MM')::smallint, 
				glyear=to_char(r_arinv.invoice_date, 'YYYY')::int, 
				warehouseid=r_arinv.id_gudang, 
				fifohistid=null, 
				fifostart=New.qty, 
				qtycontrol=New.qty, 
				no_bukti=r_arinv.invoice_no
		from m_Item i where i.id=NeW.id_barang 
		and invoiceid=r_arinv.id and id_barang=new.id_barang and txtype=r_arinv.trx_type;

		return NEW;
	end if;

	RETURN NULL;
end if;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


  CREATE OR REPLACE FUNCTION public.fn_ar_retur_lookup(integer)
  RETURNS SETOF record AS
$BODY$
declare
	v_arid	alias for $1;
	rcd	record;
begin
for rcd in
	select coalesce(i.plu,'') as plu, coalesce(i.nama_barang,'') as nama_barang, sum(qty) as qty_sisa, coalesce(i.satuan,'') as satuan,
	d.unit_price-case when isnumeric(d.disc) then d.disc::double precision else 0 end, coalesce(d.biaya,0) as biaya, coalesce(d.unit_cost,0) as unit_cost, coalesce(d.biaya_item,0) as biaya_item, d.id_barang 
	from ar_inv_det  d
	inner join ar_inv h on h.id=d.ar_id
	inner join m_item i on i.id=d.id_barang
	where (h.id=v_arid and h.trx_type='PJL') or (h.trx_type='RJL' and h.retur_dari=v_arid and coalesce(h.retur_dari,0)<>0)
	group by d.id_barang, coalesce(i.plu, '') , coalesce(i.nama_barang,''), d.unit_price, d.seq, coalesce(i.satuan,''), 
	coalesce(d.biaya,0), coalesce(d.unit_cost,0), case when isnumeric(d.disc) then d.disc::double precision else 0 end, coalesce(d.biaya_item,0)
	having sum(qty)>0
	order by d.seq
	
LOOP
	return next rcd;
END LOOP;

/*
select * from fn_ar_retur_lookup(416) as (kode varchar, nama_barang varchar, qty double precision, satuan varchar, 
harga double precision, biaya double precision, hpp double precision, biaya_item double precision, id_barang integer)

select * from ar_inv  order by id desc
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
