﻿CREATE OR REPLACE FUNCTION public.isnumeric(text)
  RETURNS boolean AS
$BODY$
DECLARE x NUMERIC;
BEGIN
    x = $1::NUMERIC;
    RETURN TRUE;
EXCEPTION WHEN others THEN
    RETURN FALSE;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;

CREATE OR REPLACE FUNCTION public.fn_get_kode_item()
  RETURNS character varying AS
$BODY$
begin
--select fn_get_kode_item()
return trim(to_char((select max(plu)::int from m_item where coalesce(plu,'')<>'' and isnumeric(plu)=true )+1, '00000'))  ;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

--Hapus penjualan dan retur
DROP TRIGGER tg_ar_inv_det_delete ON public.ar_inv_det;
delete from ar_inv_det;
alter SEQUENCE ar_inv_det_det_id_seq restart with 1;
delete from ar_inv;
alter SEQUENCE ar_inv_id_seq  restart with 1;
delete from ar_inv_komisi;


delete from so_det;
delete from so;  

delete from ar_receipt_detail;
alter SEQUENCE ar_receipt_detail_serial_no_seq restart with 1;
delete from ar_receipt;
delete from ar_sj; 
  
  
CREATE TRIGGER tg_ar_inv_det_delete
  AFTER DELETE
  ON public.ar_inv_det
  FOR EACH ROW
  EXECUTE PROCEDURE public.fn_tg_ar_inv_det_delete();

--Hapus data pembelian barang
DROP TRIGGER tg_ap_inv_det_delete ON public.ap_inv_det;

delete from po_det;
delete from po;  


delete from ap_payment_detail;
alter SEQUENCE ap_payment_detail_serial_no_seq restart with 1;
delete from ap_payment;

delete from ap_hutang_lain_angsuran;
delete from ap_hutang_lain_alat_bayar ; 
delete from ap_hutang_lain;

delete from ap_bayar_hutang_detail;  
delete from ap_bayar_hutang_alat_bayar;
delete from ap_bayar_hutang;   


delete from ap_inv_det;
alter SEQUENCE ap_inv_det_det_id_seq  restart with 1;
delete from ap_inv;
alter SEQUENCE ap_inv_id_seq  restart with 1;

CREATE TRIGGER tg_ap_inv_det_delete
  AFTER DELETE
  ON public.ap_inv_det
  FOR EACH ROW
  EXECUTE PROCEDURE public.fn_tg_ap_inv_det_delete();

--DROP TRIGGER tg_penerimaan_stok_det_delete ON public.mutasi_stok_det;

delete from mutasi_stok_det;
delete from mutasi_stok;

delete from opname_detail;
alter SEQUENCE opname_detail_seq_seq restart with 1;
delete from opname;
alter SEQUENCE opname_id_seq  restart with 1;
 
delete from transfer_detail ;
alter SEQUENCE transfer_detail_serial_no_seq restart with 1;
delete from transfer;
alter SEQUENCE transfer_id_seq restart with 1;

DROP TRIGGER tg_item_history_del ON public.item_history;
   
delete from item_history;
alter SEQUENCE item_history_itemhistid_seq RESTART with 1;

CREATE TRIGGER tg_item_history_del
  AFTER DELETE
  ON public.item_history
  FOR EACH ROW
  EXECUTE PROCEDURE public.fn_tg_item_history_del();

/*
delete from audit_log_detail;
alter SEQUENCE audit_log_detail_id_seq  restart with 1;

delete from audit_log;
alter SEQUENCE audit_log_id_seq  restart with 1;
*/
delete from m_item_supplier;
delete from m_item_price_by_customer;  
delete from m_item_harga_jual ;

delete from m_item;
alter SEQUENCE item_id_seq  restart with 1; 

delete from m_item_kategori ;
alter SEQUENCE pos_item_kategori_id_seq  restart with 1;
--select * from m_item_kategori

delete from m_satuan;
alter sequence m_satuan_id_seq restart with 1;
--select * from m_satuan

delete from m_relasi;
alter SEQUENCE m_relasi_id_relasi_seq  restart with 1;

--alter table m_item add column biaya_item double precision default 0;
alter table m_user alter column pwd type varchar(70);
--alter table m_setting  add column tgl_system_default boolean default true;
--alter table m_setting  add column status_shift boolean default true;
--alter table m_setting_penjualan  add column tampilkan_hpp boolean default true;
--alter table m_setting_penjualan  add column tutup_dialog_bayar_print boolean default true;

delete from m_gudang;
alter SEQUENCE m_gudang_id_seq restart with 1;
insert into  m_gudang(nama_gudang, keterangan) VALUES('TOKO', 'Gudang Toko');

/*
delete from m_user;
insert into m_user(user_id, user_name, complete_name, pwd, profile) VALUES
('001', 'admin', 'Administrator', '$2a$10$SvsdrX/V3XBDM/TR5stM9Op5QuCz1zlb8dTwS541HCPBOD8ZbFfve', 0);
*/


