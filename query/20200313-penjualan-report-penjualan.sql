﻿-- Function: public.fn_rpt_penjualan_detail_item_cost(character varying, character varying, bigint, bigint, bigint, bigint)

-- DROP FUNCTION public.fn_rpt_penjualan_detail_item_cost(character varying, character varying, bigint, bigint, bigint, bigint);

CREATE OR REPLACE FUNCTION public.fn_rpt_penjualan_detail_item_cost(
    character varying,
    character varying,
    bigint,
    bigint,
    bigint,
    bigint)
  RETURNS SETOF record AS
$BODY$
declare
	v_tanggal1	alias for $1;
	v_Tanggal2	alias for $2;
	v_cust		alias for $3;
	v_sales		alias for $4;
	v_expedisi	alias for $5;
	v_gudang	alias for $6;
	rcd 		record;
begin
for rcd in
	select i.id, i.invoice_no as no_faktur, (i.invoice_date::date) as tgl_faktur, coalesce(i.no_so, '') as no_so, coalesce(g.nama_gudang,'') as nama_gudang, 
	i.id_customer, coalesce(c.nama_relasi,'') as nama_customer, coalesce(c.alamat,'') as alamat_cust, coalesce(k.nama_kota,'') as kota_customer, 
	case when coalesce(c.telp,'')<>'' then c.telp else coalesce(c.hp,'') end as telp_hp, i.id_expedisi, coalesce(ex.nama_expedisi,'') as nama_expedisi,
	(i.invoice_date+coalesce(i.top,0)) as jt_tempo, coalesce(i.description,'') as ket_faktur, coalesce(t.keterangan,'') as trx_type, coalesce(i.ket_pembayaran,'') as ket_pembayaran,
	case when i.jenis_bayar='K' then 'Kredit' else 'Tunai' end as status_bayar, i.id_sales, coalesce(s.nama_sales,'') as nama_sales, 
	coalesce(i.ar_disc,'') as ar_disc, 
	--coalesce(x.disc_rp, 0) as ar_disc_rp, coalesce(x.nett,0) as nett, 
	0::double precision ar_disc_rp, coalesce(i.invoice_amount,0)::double precision nett,
	coalesce(m_item.plu,'') as plu, coalesce(m_item.nama_barang,'') as nama_barang, 
	coalesce(d.qty,0) as quantity, --1*ks.quantity, 
	coalesce(m_item.satuan,'') as satuan, d.unit_price, case when isnumeric(d.disc) then d.disc else '0' end disc, d.ppn, coalesce(d.biaya,0) as biaya,
	--fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) as sub_total, 
	(d.qty*(coalesce(d.unit_price,0)-case when isnumeric(d.disc) then d.disc::numeric else 0 end))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) as sub_total,
	coalesce(d.keterangan,'') as keterangan, 
	(coalesce(d.unit_cost,0)+case when isnumeric(d.disc) then d.disc else '0' end::numeric) item_cost,
	coalesce(d.biaya_item,0) as biaya_item
	from ar_inv  i
	inner join ar_inv_det d on d.ar_id=i.id
	inner join m_item on m_item.id=d.id_barang
	--inner join item_history ks on ks.invoiceid=d.ar_id and ks.txtype=i.trx_type and ks.id_barang=d.id_barang --ks.det_id=d.det_id 
	left join m_relasi c on c.id_relasi=i.id_customer
	left join m_gudang g on g.id=i.id_gudang
	left join m_salesman s on s.id_sales=i.id_sales
	left join m_expedisi ex on ex.id=i.id_expedisi
	left join m_user u on u.user_name=i.user_ins
	left join m_kota k on k.id=c.id_kota
	left join m_trx_type t on t.kode=i.trx_type
	/*left join(
		select h.id, 
		--fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)* coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,''))+coalesce(h.biaya_lain,0) as nett, 
		--sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100))-
		--fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,'')) as disc_rp
		sum((d.qty*(coalesce(d.unit_price,0)-case when isnumeric(d.disc) then d.disc::numeric else 0 end))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) as nett,
		case when isnumeric(d.disc) then d.disc::numeric else 0 end disc_rp
		from ar_inv h
		inner join ar_inv_det d on d.ar_id=h.id 
		where to_Char(h.invoice_date, 'yyyy-MM-dd')>=v_tanggal1
		and to_Char(h.invoice_date, 'yyyy-MM-dd')<=v_tanggal2
		and case when v_cust is not null or v_cust>0 then h.id_customer=v_cust else 1=1 end
		and case when v_sales is not null or v_sales>0 then h.id_sales=v_sales else 1=1 end
		and case when v_expedisi is not null or v_expedisi>0 then h.id_expedisi=v_expedisi else 1=1 end
		and case when v_gudang is not null or v_gudang>0 then h.id_gudang=v_gudang else 1=1 end
		group by h.id, coalesce(h.ar_disc,'')

		
	)x on x.id=i.id
	*/
	where i.invoice_date::date>=v_tanggal1::date
	and i.invoice_date::date<=v_tanggal2::date
	--and case when v_cust is not null or v_cust>0 then i.id_customer=v_cust else 1=1 end
	--and case when v_sales is not null or v_sales>0 then i.id_sales=v_sales else 1=1 end
	--and case when v_expedisi is not null or v_expedisi>0 then i.id_expedisi=v_expedisi else 1=1 end
	--and case when v_gudang is not null or v_gudang>0 then i.id_gudang=v_gudang else 1=1 end
	order by i.id, d.det_id
LOOP
	return next rcd;
END LOOP;	
/*
select * from fn_rpt_penjualan_detail_item_cost('2020-03-01', '2020-03-15', null, null, null, null) as (id_faktur bigint, no_faktur varchar, tgl_faktur date, no_so varchar, 
nama_gudang varchar, id_customer bigint, nama_custoer varchar, alamat_customer varchar, kota_customer varchar, telp_hp varchar, id_expedisi int, 
nama_expedisi varchar, jt_tempo date, ket_faktur text, trx_type varchar, ket_pembayaran text, status_bayar text, id_sales bigint, nama_sales varchar, ar_disc varchar, 
ar_disc_rp double precision, nett double precision, plu varchar, nama_barang varchar, qty double precision, satuan varchar, unit_price double precision, 
disc varchar, ppn double precision, biaya double precision, sub_total double precision, keterangan varchar, item_cost double precision, biaya_item double precision)
select * from ar_inv_det  order by ar_id  desc limit 1
*/
return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.fn_rpt_penjualan_fast_slow(
    character varying,
    character varying,
    character varying,
    character varying,
    integer,
    integer)
  RETURNS SETOF record AS
$BODY$
declare
	v_tanggal1	alias for $1;
	v_tanggal2	alias for $2;
	v_order_by	alias for $3;
	v_asc_desc	alias for $4;
	v_limit		alias for $5;
	v_item_kat	alias for $6;
	rcd		record;
	v_sql		text;
begin
v_sql='select d.id_barang, coalesce(i.plu,'''') as kode, coalesce(i.nama_barang,'''') as nama_barang, sum(d.qty) as qty, 
	coalesce(i.satuan,'''') as satuan, sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) as harga,
	sum(d.qty*coalesce(d.unit_cost,0)) as item_cost,
	sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) -
	sum(d.qty*coalesce(d.unit_cost,0)) as profit, sum(d.qty*coalesce(d.biaya_item,0)) as biaya_item
	from ar_inv h
	inner join ar_inv_det d on d.ar_id=h.id
	inner join m_item i on i.id=d.id_barang
	/*
	left join(
		select det_id, item_cost 
		from item_history  h
		inner join m_item i on i.id=h.id_barang
		where txtype in(''PJL'', ''RJL'')
		and to_char(txdate, ''yyyy-MM-dd'')>='||''''||v_tanggal1||''''||'
		and to_char(txdate, ''yyyy-MM-dd'')<='||''''||v_tanggal2||''''||' '
		||case when coalesce(v_item_kat, 0)=0 then '' else 'and i.id_kategori='||v_item_kat||' ' end 
	||') ks on ks.det_id=d.det_id
	*/
	where to_char(h.invoice_date, ''yyyy-MM-dd'')>='||''''||v_tanggal1||''''||'
	and to_char(h.invoice_date, ''yyyy-MM-dd'')<='||''''||v_tanggal2||''''||' '
	||case when coalesce(v_item_kat, 0)=0 then '' else 'and i.id_kategori='||v_item_kat||' ' end 
	||'group by d.id_barang, coalesce(i.nama_barang,''''), coalesce(i.satuan,''''), coalesce(i.plu,'''')
	order by '||case	when v_order_by='Harga' then 'sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) '
			when v_order_by='Profit' then 'sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) -
				sum(d.qty*coalesce(d.unit_cost,0)) '
			when v_order_by='% Profit' then 'sum(d.qty*coalesce(d.unit_cost,0))/ 
				sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) '
			else 'sum(d.qty) ' end
	||case when v_asc_desc='ASC' then ' asc ' else ' desc '  end ||
	'limit '||v_limit;
	raise notice 'Sql: %', v_sql;
for rcd in execute
	v_sql
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_rpt_penjualan_fast_slow('2020-03-01', '2020-03-31', 'Profit', 'DESC', 100, null) as (id_barang integer, kode varchar, nama_barang varchar,
qty double precision, satuan varchar, harga double precision, item_cost double precision, profit double precision, biaya_item double precision)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.fn_rpt_penjualan_detail_shift(
    v_tgl1 character varying,
    v_tgl2 character varying,
    v_shift character varying,
    v_user character varying,
    v_pelanggan integer)
  RETURNS SETOF record AS
$BODY$
declare
	rcd record;
begin
for rcd in	

	select i.id, i.invoice_no as no_faktur, (i.invoice_date::date) as tgl_faktur, i.shift, coalesce(g.nama_gudang,'') as nama_gudang, 
	i.id_customer, coalesce(c.nama_relasi,'') as nama_customer, coalesce(c.alamat,'') as alamat_cust, coalesce(k.nama_kota,'') as kota_customer, 
	case when coalesce(c.telp,'')<>'' then c.telp else coalesce(c.hp,'') end as telp_hp, 
	(i.invoice_date+coalesce(i.top,0)) as jt_tempo, coalesce(i.description,'') as ket_faktur, coalesce(t.keterangan,'') as trx_type, coalesce(i.ket_pembayaran,'') as ket_pembayaran,
	case when i.jenis_bayar='K' then 'Kredit' else 'Tunai' end as status_bayar, 
	coalesce(i.paid_amount,0) bayar, coalesce(m_item.plu,'') as plu, coalesce(m_item.nama_barang,'') as nama_barang, 
	d.qty, coalesce(m_item.satuan,'') as satuan, d.unit_price, case when isnumeric(d.disc) then d.disc::double precision else 0 end disc, d.ppn, coalesce(d.biaya,0) as biaya,
	((d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0))::double precision as sub_total, 
	coalesce(d.unit_cost,0) as unit_cost, coalesce(d.biaya_item,0) as biaya_item, coalesce(d.keterangan,'') as keterangan, 
	coalesce(u.complete_name,'') as dibuat_oleh
	from ar_inv  i
	inner join ar_inv_det d on d.ar_id=i.id
	inner join m_item on m_item.id=d.id_barang
	left join m_relasi c on c.id_relasi=i.id_customer
	left join m_gudang g on g.id=i.id_gudang
	left join m_user u on u.user_name=i.user_ins
	left join m_kota k on k.id=c.id_kota
	left join m_trx_type t on t.kode=i.trx_type
	where i.invoice_date>=v_tgl1::Date
	and i.invoice_date<=v_tgl2::Date
	and case when v_shift='' then true else i.shift=v_shift end
	and case when v_user='' then true else i.user_ins=v_user end
	and case when v_pelanggan is null then true else i.id_customer=v_pelanggan end
	order by i.invoice_date, d.det_id
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_rpt_penjualan_detail_shift('2020-01-20', '2020-08-20', '', '', null) as (id bigint, no_faktur varchar, tgl_faktur date, shift varchar, 
nama_gudang varchar, id_customer bigint, nama_customer varchar, alamat_cust varchar, kota_customer varchar, telp_hp varchar, jt_tempo date, 
ket_faktur text, trx_type varchar, ket_pembayaran text, status_bayar text, bayar double precision, plu varchar, nama_barang varchar, qty double precision, 
satuan varchar, unit_price double precision, disc double precision, ppn double precision, biaya double precision, sub_total double precision, 
unit_cost double precision, biaya_item double precision, keterangan varchar, dibuat_oleh varchar)

select id, no_faktur, tgl_faktur, shift, nama_customer, trx_type, status_bayar,
sum(sub_total) total, sum(qty*disc) disc, sum(sub_total-(qty*disc)) nett, 
case when bayar-sum(sub_total-(qty*disc))>=0 then sum(sub_total-(qty*disc)) else bayar end bayar 
from fn_rpt_penjualan_detail_shift('2017-08-20', '2017-08-20', '', '', null) as (id bigint, no_faktur varchar, tgl_faktur date, shift varchar, 
nama_gudang varchar, id_customer bigint, nama_customer varchar, alamat_cust varchar, kota_customer varchar, telp_hp varchar, jt_tempo date, 
ket_faktur text, trx_type varchar, ket_pembayaran text, status_bayar text, bayar double precision, plu varchar, nama_barang varchar, qty double precision, 
satuan varchar, unit_price double precision, disc double precision, ppn double precision, biaya double precision, sub_total double precision, 
keterangan varchar, dibuat_oleh varchar)
group by id, no_faktur, tgl_faktur, shift, nama_customer, trx_type, bayar, status_bayar
order by id desc

*/

end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION public.fn_rpt_closing_shift(
    v_tanggal character varying,
    v_shift character varying)
  RETURNS SETOF record AS
$BODY$
declare
	rcd record;
begin
for rcd in	
	select urut, tipe, sum(total)
	from(
		select 1 urut, 'PENJUALAN'tipe, 
		coalesce(sum(d.qty*d.unit_price),0) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
		and trx_type='PJL'

		UNION ALL

		select 1 urut, 'PENJUALAN KREDIT'tipe, 
		-coalesce(sum(d.qty*d.unit_price),0) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
		and jenis_bayar='K'
		and trx_type='PJL'
		
		union all
		
		select 2 urut, 'POTONGAN PENJUALAN' tipe, 
		coalesce(-sum(case when isnumeric(d.disc) then d.qty*d.disc::double precision else 0 end),0) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end and coalesce(d.disc,'')<>''
		and trx_type='PJL'
		and isnumeric(d.disc) and d.disc::numeric > 0
		
		union all

		select 1 urut, 'RETUR PENJUALAN'tipe, 
		sum(d.qty*d.unit_price-case when isnumeric(d.disc) then d.qty*d.disc::double precision else 0 end) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and h.trx_type='RJL'
		and case when v_shift='' then true else h.shift=v_shift end
		group by case when trx_type='PJL' then 1 else 3 end, case when trx_type='PJL' then 'PENJUALAN' when trx_type='RJL' then 'RETUR PENJUALAN' else '' end

		union all

		select  case when h.masuk>0 then 9 else 10 end, 'PETTY CASH'||case when h.masuk>0 then ' MASUK' else ' KELUAR' end ||' ('||h.keterangan||')' tipe, 
		sum(coalesce(h.masuk,0)-coalesce(h.keluar,0)) total
		from petty_cash h 
		where h.tanggal=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
		group by case when h.masuk>0 then 9 else 10 end, 'PETTY CASH'||case when h.masuk>0 then ' MASUK' else ' KELUAR' end ||' ('||h.keterangan||')'
		
		union all

		select 3 urut, 'PENERIMAAN PIUTANG PELANGGAN', coalesce(sum(coalesce(d.bayar,0)-coalesce(d.discount,0)),0) 
		from ar_receipt h
		inner join ar_receipt_detail d on h.ar_no=d.ar_no  
		where h.tanggal=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
				
	)x
	group by urut, tipe
	order by urut 
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_rpt_closing_shift('2020-03-14', '') as (urut integer, tipe text, total double precision)
select * from ar_inv  order by invoice_date   desc
*/	
end
$BODY$
  LANGUAGE plpgsql VOLATILE;  

 select h.invoice_date, 
sum(case when h.jenis_bayar<>'K' and upper(h.shift)='P' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_tunai_p, 
sum(case when h.jenis_bayar='K' and upper(h.shift)='P' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_kredit_p,
sum(case when h.jenis_bayar<>'K' and upper(h.shift)='S' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_tunai_s, 
sum(case when h.jenis_bayar='K' and upper(h.shift)='S' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_kredit_s,
sum((d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0))::double precision as total
from ar_inv h
join ar_inv_det d on d.ar_id=h.id
where h.invoice_date between '2020-01-01' and '2020-01-02'
and (1=1 or h.shift='P')
and (1=1 or h.user_ins='P')
group by h.invoice_date
order by h.invoice_date;


 select h.invoice_date, 
sum(case when h.jenis_bayar<>'K' and upper(h.shift)='P' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_tunai_p, 
sum(case when h.jenis_bayar='K' and upper(h.shift)='P' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_kredit_p,
sum(case when h.jenis_bayar<>'K' and upper(h.shift)='S' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_tunai_s, 
sum(case when h.jenis_bayar='K' and upper(h.shift)='S' then (d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) else 0 end)::double precision as tot_kredit_s,
sum((d.qty*(coalesce(d.unit_price,0)))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0))::double precision as total
from ar_inv h
join ar_inv_det d on d.ar_id=h.id
--where h.invoice_date between $P{tanggal1} and $P{tanggal2}
--and (coalesce($P{shift},'')''='' or h.shift=$P{shift})
--and (coalesce($P{kasir},'')''=''  or h.user_ins=${kasir})
group by h.invoice_date
order by h.invoice_date;


