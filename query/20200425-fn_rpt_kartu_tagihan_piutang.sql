﻿alter table acc_coa add column singkatan varchar(10);

update m_relasi  set alamat ='' where alamat is null;

CREATE OR REPLACE FUNCTION public.fn_rpt_kartu_tagihan_piutang_landscape(
    character varying,
    character varying,
    integer)
  RETURNS SETOF record AS
$BODY$
declare
	v_tanggal1	alias for $1;
	v_tanggal2	alias for $2;
	v_id_cust	alias for $3;
	rcd		record;
begin
for rcd in 
	select i.id, i.invoice_no, coalesce(r.nama_relasi,'') as customer, i.invoice_date, i.id_sales, coalesce(s.nama_sales,'') as nama_sales, 
	i.item_amount, i.item_amount-i.invoice_amount as disc_penjualan, 
	coalesce(ret.ret_amount,0) as ret_amount, i.invoice_amount-coalesce(ret.ret_amount,0)-i.paid_amount as sisa, 
	case when bayar.alat_bayar<>'BG' then bayar.bayar else 0 end as tunai, 
	case when bayar.alat_bayar='BG' then bayar.bayar else 0 end as giro, 
	bayar.tanggal as tgl_bayar,
	coalesce(bayar.akun,'') as akun_kas_bank,
	coalesce(bayar.check_no,'') as no_cek_giro,  bayar.tgl_cek, bayar.ket_pembayaran
	from ar_inv i
	inner join m_relasi r on r.id_relasi=i.id_customer
	left join m_salesman s on s.id_sales=i.id_sales
	left join 
	(	
		select ret.retur_dari, sum(ret.invoice_amount) as ret_amount
		from ar_inv ret
		inner join ar_inv i on i.id=ret.retur_dari
		where ret.trx_type='RJL' 
		and i.jenis_bayar='K'
		and case when v_id_cust is null then 1=1 else i.id_customer=v_id_cust end
		and to_char(i.invoice_date, 'yyyy-MM-dd')>=v_tanggal1
		and to_char(i.invoice_date, 'yyyy-MM-dd')<=v_Tanggal2
		
		group by ret.retur_dari
	)ret on ret.retur_dari=i.id 
	left join(
		select d.id_penjualan, d.bayar, h.tanggal, coalesce(h.alat_bayar,'') as alat_bayar, coalesce(coa.singkatan, coalesce(coa.acc_name,'')) as akun, 
		coalesce(h.memo,'') as ket_pembayaran, h.check_no, h.tgl_cek
		from ar_receipt h
		inner join ar_receipt_detail d on h.ar_no=d.ar_no
		inner join ar_inv inv on inv.id=d.id_penjualan
		left join acc_coa coa on coa.acc_no=h.acc_no
		where not h.void_check and case when v_id_cust is null then 1=1 else inv.id_customer=v_id_cust end
		and to_char(inv.invoice_date, 'yyyy-MM-dd')>=v_tanggal1
		and to_char(inv.invoice_date, 'yyyy-MM-dd')<=v_Tanggal2
		and inv.jenis_bayar='K'
		
	) as bayar on bayar.id_penjualan= i.id
	where (i.trx_type in('PJL', 'SAR') or (i.trx_type='RJL' and i.jenis_bayar<>'T'))
	and case when v_id_cust is null then 1=1 else i.id_customer=v_id_cust end
	and to_char(i.invoice_date, 'yyyy-MM-dd')>=v_tanggal1
	and to_char(i.invoice_date, 'yyyy-MM-dd')<=v_Tanggal2
	and i.jenis_bayar='K'
	order by coalesce(r.nama_relasi,'')
LOOP
	return next rcd;
END LOOP;
end
/*
select * from fn_rpt_kartu_tagihan_piutang_landscape('2014-04-01', '2014-04-30', null) as (id bigint, invoice_no varchar, customer varchar, 
invoice_date date, id_sales bigint, nama_sales varchar, item_amount double precision, disc_penjualan double precision, ret_penjualan double precision, sisa double precision,
tunai double precision, giro double precision, tgl_bayar date, bank varchar, no_giro varchar, tgl_cek date, ket_pembayaran text)
*/
$BODY$
  LANGUAGE plpgsql VOLATILE;

  