/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos;

import com.ustasoft.component.DBConnection;
import com.ustasoft.component.OSValidator;
import com.ustasoft.component.rhs.ini;
import java.awt.Color;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import static javax.swing.SwingUtilities.updateComponentTreeUI;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.BorderUIResource;

/**
 *
 * @author cak-ust
 */
public class SIA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {

        try {
            BorderUIResource borderUIResource = new BorderUIResource(BorderFactory.createLineBorder(Color.yellow, 2));
            if (OSValidator.isWindows()) {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            } else {
//                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
//                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//                UIManager.setLookAndFeel("javax.swing.plaf.synth");
//                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
//                UIManager.put("text", Color.red);
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//                SwingUtilities.updateComponentTreeUI(f1);
//                updateComponentTreeUI(f1);
//                for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//                    System.out.println("info.getName(): "+info.getName());
//                    if ("Nimbus".equals(info.getName())) {
//                        UIManager.setLookAndFeel(info.getClassName());
//                        break;
//                    }
//                }
            }
            UIManager.put("Table.focusCellHighlightBorder", borderUIResource);
            LoginUser f1 = new LoginUser();
            //f1.setConn(dataSource.getConnection());
            f1.setConn(new DBConnection(get1(), get2(), f1).getCon());
            f1.setVisible(true);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(LoginUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(LoginUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(LoginUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static String get1() {
        return ini.u;
    }

    private static String get2() {
        return ini.p;
    }
}
