/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos.ar;

import com.ustasoft.component.DlgLookup;
import com.ustasoft.component.GeneralFunction;
import com.ustasoft.pos.dao.jdbc.ExpedisiDao;
import com.ustasoft.pos.dao.jdbc.GudangDao;
import com.ustasoft.pos.dao.jdbc.ItemKategoriDao;
import com.ustasoft.pos.dao.jdbc.RelasiDao;
import com.ustasoft.pos.dao.jdbc.SalesDao;
import com.ustasoft.pos.domain.Expedisi;
import com.ustasoft.pos.domain.Gudang;
import com.ustasoft.pos.domain.ItemKategori;
import com.ustasoft.pos.domain.Relasi;
import com.ustasoft.pos.domain.Sales;
import com.ustasoft.pos.service.ReportService;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import pos.MainForm;

/**
 *
 * @author cak-ust
 */
public class FrmRptPenjualan extends javax.swing.JInternalFrame {
    private GeneralFunction fn= GeneralFunction.getInstance();
    ReportService service;
    private Connection conn;
    List lstCustomer=new ArrayList();
    List lstSalesman=new ArrayList();
    List lstKategori=new ArrayList();
    List lstGudang=new ArrayList();
    List lstExpedisi=new ArrayList();
    private Integer idBarang;
    /**
     * Creates new form FrmRptPenjualan
     */
    public FrmRptPenjualan() {
        initComponents();
        AutoCompleteDecorator.decorate(cmbGudang);
        AutoCompleteDecorator.decorate(cmbCustomer);
//        AutoCompleteDecorator.decorate(cmbExpedisi);
//        AutoCompleteDecorator.decorate(cmbSalesman);
        MyKeyListener kListener=new MyKeyListener();
        fn.addKeyListenerInContainer(jPanel1, kListener, null);
        fn.addKeyListenerInContainer(jPanel2, kListener, null);
    }
    
    public void setConn(Connection con){
        this.conn=con;
    }

    private void initForm(){
        try {
            service=new ReportService(conn, this);
            
            Timestamp skg=fn.getTimeServer();
            jDateChooserAwal.setDate(skg); jDateChooserAwal.setFormats(MainForm.sDateFormat);
            jDateChooserAkhir.setDate(skg);jDateChooserAkhir.setFormats(MainForm.sDateFormat);
            jList1.setSelectedIndex(0);
            
            cmbGudang.removeAllItems();
            lstGudang.add("");
            cmbGudang.addItem("");
            GudangDao dao=new GudangDao(conn);
            for(Gudang gudang: dao.cariSemuaData()){
                lstGudang.add(gudang.getId());
                cmbGudang.addItem(gudang.getNama_gudang());
            }
            
            cmbCustomer.removeAllItems();
            lstCustomer.add("");
            cmbCustomer.addItem("");
            RelasiDao rel=new RelasiDao(conn);
            for(Relasi relasi: rel.cariSemuaData(0)){
                lstCustomer.add(relasi.getIdRelasi());
                cmbCustomer.addItem(relasi.getNamaRelasi());
            }
            
//            cmbSalesman.removeAllItems();
//            lstSalesman.add("");
//            cmbSalesman.addItem("");
//            SalesDao sales=new SalesDao(conn);
//            for(Sales s:sales.cariSemuaData()){
//                lstSalesman.add(s.getId_sales());
//                cmbSalesman.addItem(s.getNama_sales());
//            }
            
            cmbKategori.removeAllItems();
            lstKategori.add("");
            cmbKategori.addItem("");
            ItemKategoriDao kat=new ItemKategoriDao(conn);
            for(ItemKategori s:kat.cariSemuaKategori()){
                lstKategori.add(s.getId());
                cmbKategori.addItem(s.getNamaKategori());
            }
            
//            cmbExpedisi.removeAllItems();
//            lstExpedisi.add("");
//            cmbExpedisi.addItem("");
//            ExpedisiDao expedisi=new ExpedisiDao(conn);
//            for(Expedisi e:expedisi.cariSemuaData()){
//                lstExpedisi.add(e.getId());
//                cmbExpedisi.addItem(e.getNama_expedisi());
//            }
            cmbKasir.removeAllItems();
            cmbKasir.addItem("Semua");
            ResultSet rs=MainForm.conn.createStatement().executeQuery("select user_name from m_user order by 1");
            while(rs.next()){
                cmbKasir.addItem(rs.getString(1));
            }
            rs.close();
            rs=null;
        } catch (Exception ex) {
            Logger.getLogger(FrmRptPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private class MyKeyListener extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent evt) {
            if(evt.getKeyCode()==KeyEvent.VK_DELETE && (
                    evt.getSource()==cmbCustomer ||
//                    evt.getSource()==cmbExpedisi ||
                    evt.getSource()==cmbGudang
                    ) ){
                ((JComboBox)evt.getSource()).setSelectedIndex(0);
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbGudang = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbCustomer = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jDateChooserAkhir = new org.jdesktop.swingx.JXDatePicker();
        jDateChooserAwal = new org.jdesktop.swingx.JXDatePicker();
        cmbKriteria = new javax.swing.JComboBox();
        txtLimit = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cmbKategori = new javax.swing.JComboBox();
        lblItem = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cmbShift = new javax.swing.JComboBox();
        cmbKasir = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtItem = new javax.swing.JTextField();
        btnLookupItem = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setClosable(true);
        setTitle("Laporan Penjualan");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "1.  Penjualan Detail per Tanggal", "2.  Penjualan Rekap per Tanggal", "3.  Rincian Penjualan per Customer", "4.  Rekap Penjualan per Customer", "5.  Detail Penjualan per Gudang", "6.  Rekap Penjualan per Gudang", "7.  Detail Profit Penjualan per Faktur", "8.  Rekap Profit Penjualan per Tanggal", "9.  Detail Penjualan Item", "10.Rekap Penjualan Item", "11.Penjualan Stok Tertinggi", "12.Penjualan Stok Terendah", "13.Rekap Tutup Shift", "14.Rekap TOTAL per Tanggal" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Parameter"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Gudang :");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 80, 20));

        jLabel2.setText("Awal :");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 80, 20));

        cmbGudang.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TUNAI", "KREDIT" }));
        jPanel2.add(cmbGudang, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 200, 130, -1));

        jLabel3.setText("Sampai :");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 45, 80, 20));

        jLabel4.setText("Pelanggan");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 80, 20));

        cmbCustomer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TUNAI", "KREDIT" }));
        jPanel2.add(cmbCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 120, 220, -1));

        jLabel6.setText("Urut berdasar : ");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 100, 20));
        jPanel2.add(jDateChooserAkhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 45, 165, -1));
        jPanel2.add(jDateChooserAwal, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 165, -1));

        cmbKriteria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Qty", "Harga", "Profit", "% Profit" }));
        jPanel2.add(cmbKriteria, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 240, 135, -1));

        txtLimit.setText("50");
        txtLimit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(txtLimit, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 240, 50, 20));

        jLabel8.setText("Baris");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, -1, 20));

        cmbKategori.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Qty", "Harga", "Profit", "% Profit" }));
        jPanel2.add(cmbKategori, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 145, 220, -1));

        lblItem.setText("Item :");
        jPanel2.add(lblItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 175, 80, 20));

        jLabel9.setText("Shift");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 80, 20));

        cmbShift.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SEMUA", "PAGI", "SORE" }));
        jPanel2.add(cmbShift, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 220, -1));

        cmbKasir.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SEMUA", "PAGI", "SORE" }));
        jPanel2.add(cmbKasir, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 95, 220, -1));

        jLabel10.setText("Kasir");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 95, 80, 20));

        jLabel16.setText("Katergori Item : ");
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 145, 100, 20));

        txtItem.setEditable(false);
        txtItem.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtItemActionPerformed(evt);
            }
        });
        jPanel2.add(txtItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 175, 200, 20));

        btnLookupItem.setText("...");
        btnLookupItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLookupItemActionPerformed(evt);
            }
        });
        jPanel2.add(btnLookupItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(291, 170, 30, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 330, 300));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/printer.png"))); // NOI18N
        jButton1.setText("Tampilkan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 320, 120, 30));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        jButton2.setText("Tutup");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 320, 90, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        setBounds(0, 0, 646, 419);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        udfPreview();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        initForm();;
    }//GEN-LAST:event_formInternalFrameOpened

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        int idx=jList1.getSelectedIndex();
        cmbShift.setEnabled(idx==0 || idx==1|| idx==12);
        cmbKasir.setEnabled(idx==0 || idx==1);
        cmbKategori.setEnabled(idx==8|idx==9||idx==10||idx==11);
        cmbGudang.setEnabled(idx==4 || idx==5);
        cmbCustomer.setEnabled(idx==2 || idx==3);
        //Fast-Slow
        boolean fastSlow = idx==10||idx==11;
        jLabel6.setVisible(fastSlow);
        cmbKriteria.setVisible(fastSlow);
        txtLimit.setVisible(fastSlow);
        jLabel8.setVisible(fastSlow);
        boolean showItem = idx==8;
        lblItem.setVisible(showItem); txtItem.setVisible(showItem); btnLookupItem.setVisible(showItem);
    }//GEN-LAST:event_jList1ValueChanged

    private void txtItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtItemActionPerformed

    private void btnLookupItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLookupItemActionPerformed
        udfLookupItem(txtItem.getText());
    }//GEN-LAST:event_btnLookupItemActionPerformed
    SimpleDateFormat fmt=new SimpleDateFormat("yyyy-MM-dd");
    
    private void udfPreview(){
        HashMap param=new HashMap();
        param.put("toko", MainForm.sToko);
        param.put("alamat1", MainForm.sAlamat1);
        param.put("alamat2", MainForm.sAlamat2);
        param.put("telp", MainForm.sTelp1);
        param.put("email", MainForm.sEmail);
        param.put("tanggal1", fmt.format(jDateChooserAwal.getDate()));
        param.put("tanggal2", fmt.format(jDateChooserAkhir.getDate()));
        param.put("shift", cmbShift.getSelectedIndex()==0?"": cmbShift.getSelectedItem().toString().substring(0, 1));
        param.put("kasir", cmbKasir.getSelectedIndex()==0?"": cmbKasir.getSelectedItem().toString());
        param.put("kriteria", cmbKriteria.getSelectedItem().toString());
        param.put("namaGudang", cmbGudang.getSelectedItem().toString());
        param.put("idCustomer", cmbCustomer.getSelectedIndex()==0? null: (Integer)lstCustomer.get(cmbCustomer.getSelectedIndex()));
        param.put("namaCustomer", cmbCustomer.getSelectedIndex()==0? "Semua Pelanggan": cmbCustomer.getSelectedItem().toString());
//        param.put("idSales", cmbSalesman.getSelectedIndex()==0? null: (Integer)lstSalesman.get(cmbSalesman.getSelectedIndex()));
        param.put("idSales", null);
        param.put("idGudang", cmbGudang.getSelectedIndex()==0? null: (Integer)lstGudang.get(cmbGudang.getSelectedIndex()));
//        param.put("idExpedisi", cmbExpedisi.getSelectedIndex()==0? null: (Integer)lstExpedisi.get(cmbExpedisi.getSelectedIndex()));
        param.put("idExpedisi", null);
        param.put("idKategori", cmbKategori.getSelectedIndex()==0? null: (Integer)lstKategori.get(cmbKategori.getSelectedIndex()));
        param.put("idItem", this.idBarang);
        param.put("kategori", cmbKategori.getSelectedIndex()<=0? "": cmbKategori.getSelectedItem());
        param.put("limit", fn.udfGetInt(txtLimit.getText()));

        switch(jList1.getSelectedIndex()){
            case 0:{
                service.udfPreview(param, "PenjualanDetailShift", null);       break;
            }
            case 1:{
                service.udfPreview(param, "PenjualanRekap", null);        break;
            }
            case 2:{
                service.udfPreview(param, "PenjualanDetailCust", null);   break;
            }
            case 3:{
                service.udfPreview(param, "PenjualanRekapCust", null);    break;
            }
            case 4:{
                service.udfPreview(param, "PenjualanDetailGudang", null);    break;
            }
            case 5:{
                service.udfPreview(param, "PenjualanRekapGudang", null);    break;
            }
            case 6:{
                service.udfPreview(param, "PenjualanDetailProfit", null);    break;
            }
            case 7:{
                service.udfPreview(param, "PenjualanRekapProfitPerTanggal", null);    break;
            }
            case 8:{
                service.udfPreview(param, "Sales_ByItemDetail", null);    break;
            }
            case 9:{
                service.udfPreview(param, "Sales_ByItem", null);    break;
            }
            case 10:{
                param.put("urut", "DESC");
                service.udfPreview(param, "PenjualanFastSlow", null);    break;
            }
            case 11:{
                param.put("urut", "ASC");
                service.udfPreview(param, "PenjualanFastSlow", null);    break;
            }
            case 12:{
                service.udfPreview(param, "RekapClosing", null);    break;
            }
            case 13:{
                service.udfPreview(param, "PenjualanRekapTanggalShift", null);    break;
            }
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLookupItem;
    private javax.swing.JComboBox cmbCustomer;
    private javax.swing.JComboBox cmbGudang;
    private javax.swing.JComboBox cmbKasir;
    private javax.swing.JComboBox cmbKategori;
    private javax.swing.JComboBox cmbKriteria;
    private javax.swing.JComboBox cmbShift;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private org.jdesktop.swingx.JXDatePicker jDateChooserAkhir;
    private org.jdesktop.swingx.JXDatePicker jDateChooserAwal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblItem;
    private javax.swing.JTextField txtItem;
    private javax.swing.JTextField txtLimit;
    // End of variables declaration//GEN-END:variables

    private void udfLookupItem(String cari) {
        DlgLookup d1 = new DlgLookup(JOptionPane.getFrameForComponent(this), true);
        
        String s = "select *  from ("
                + "select i.id::varchar as id ,coalesce(i.plu,'') as kode, i.nama_barang, coalesce(k.nama_kategori,'') as kategori, "
                + "coalesce(h.harga_jual,0) harga_jual, coalesce(i.stok,0) stok, coalesce(i.satuan,'') as satuan, coalesce(i.biaya_item,0) as biaya_item "
                + "from m_item i "
                + "left join m_item_kategori k on k.id=i.id_kategori "
                + "left join m_item_harga_jual h on h.id_item=i.id and h.id_tipe_harga_jual=1 "
                + "where coalesce(i.active, true)=true "
                +(cmbKategori.getSelectedIndex()>0? "AND i.id_kategori = "+(Integer)lstKategori.get(cmbKategori.getSelectedIndex())+" ": "") 
                + "order by upper(i.nama_barang)  "
                + ")x ";

        System.out.println(s);
        d1.setTitle("Lookup Stok " + (cmbKategori.getSelectedIndex()>0? " ["+cmbKategori.getSelectedItem().toString()+"]" : ""));
        JTable tbl = d1.getTable();
        d1.setSearch(txtItem.getText());
        d1.udfLoad(conn, s, "(x.id||x.kode||x.nama_barang||kategori)", null);
        tbl.getColumn("id").setPreferredWidth(0);
        tbl.getColumn("id").setMinWidth(0);
        tbl.getColumn("id").setMaxWidth(0);
//        d1.setBounds(getScree, 0, 900, 500);
        d1.setSize(900, 500);
        d1.setLocationRelativeTo(null);
        d1.setVisible(true);

        if (d1.getKode().length() > 0) {
            TableColumnModel col = d1.getTable().getColumnModel();
            int iRow = tbl.getSelectedRow();
            this.idBarang = Integer.valueOf(tbl.getValueAt(iRow, col.getColumnIndex("id")).toString());
            txtItem.setText(tbl.getValueAt(iRow, col.getColumnIndex("nama_barang")).toString());
        }
    }
}
