/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos.ar;

import com.ustasoft.component.DlgLookup;
import com.ustasoft.component.GeneralFunction;
import com.ustasoft.component.Konfigurasi;
import com.ustasoft.pos.dao.jdbc.ArInvDao;
import com.ustasoft.pos.dao.jdbc.ArReceiptDao;
import com.ustasoft.pos.dao.jdbc.ItemDao;
import com.ustasoft.pos.dao.jdbc.RelasiDao;
import com.ustasoft.pos.dao.jdbc.SalesDao;
import com.ustasoft.pos.domain.ArInv;
import com.ustasoft.pos.domain.ArInvDet;
import com.ustasoft.pos.domain.ArReceipt;
import com.ustasoft.pos.domain.ArReceiptDet;
import com.ustasoft.pos.domain.Gudang;
import com.ustasoft.pos.domain.Item;
import com.ustasoft.pos.domain.Pembayaran;
import com.ustasoft.pos.domain.Relasi;
import com.ustasoft.pos.domain.Sales;
import com.ustasoft.pos.service.PrintTrxService;
import com.ustasoft.pos.service.ReportService;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;
import net.sf.jasperreports.engine.type.OrientationEnum;
import org.jdesktop.swingx.JXDatePicker;
import pos.MainForm;

/**
 *
 * @author cak-ust
 */
public class FrmPenjualan extends javax.swing.JFrame {
    ReportService reportService = new ReportService(MainForm.conn, this);;
    private Connection conn;
    DlgLookup lookup;
    private Component aThis;
    private final GeneralFunction fn = new GeneralFunction();
    private ItemDao itemDao;
    private final ArInvDao invDao = new ArInvDao();
    private SalesDao salesDao;
    MyKeyListener kListener = new MyKeyListener();
    private List<Gudang> gudang;
    private List<Sales> salesman;
    private Integer idPenjualan = null;
    private Object srcForm;
    private boolean isNew;
    private String userIns;
    private Date timeIns;
    private boolean bisaEditHarga = false;
    private int IDX_KOLOM_HARGA = 5;
    private int IDX_KOLOM_KETERANGAN = -1;
    MyTableCellEditor cEditor = new MyTableCellEditor();
    /**
     * Creates new form PenerimaanBarang
     */
    public FrmPenjualan() {
        initComponents();
        tblDetail.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tblDetail.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
                put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "selectNextColumnCell");
        tblDetail.getColumn("ProductID").setMinWidth(0);
        tblDetail.getColumn("ProductID").setMaxWidth(0);
        tblDetail.getColumn("ProductID").setPreferredWidth(0);
        tblDetail.getColumn("ID").setMinWidth(0);
        tblDetail.getColumn("ID").setMaxWidth(0);
        tblDetail.getColumn("ID").setPreferredWidth(0);
        tblDetail.getColumn("Diskon (Rp)").setPreferredWidth(150);
        tblDetail.getColumn("Hpp").setPreferredWidth(150);
        
        IDX_KOLOM_HARGA = tblDetail.getColumnModel().getColumnIndex("Harga");
        IDX_KOLOM_KETERANGAN = tblDetail.getColumnModel().getColumnIndex("Keterangan");
        jXDatePicker1.setFormats(MainForm.sDateFormat);
        jXDatePicker1.setEditable(!Konfigurasi.TGL_DEFAULT_SYSTEM);
        jXDatePicker1.setDate(GeneralFunction.getInstance().getTimeServer());
//        tblDetail.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
//            @Override
//            public void valueChanged(ListSelectionEvent e) {
//                int row = tblDetail.getSelectedRow();
//                int col = tblDetail.getSelectedColumn();
//                
//                bisaEditHarga = row > 0 && itemDao.bisaEditHarga(
//                    GeneralFunction.udfGetInt(tblDetail.getValueAt(row, tblDetail.getColumnModel().getColumnIndex("ProductID")))
//                );
//                if(IDX_KOLOM_HARGA == col) {
//                    if(bisaEditHarga) {
//                        tblDetail.getColumn("Harga").setCellEditor(cEditor);
//                    }else{
//                        tblDetail.getColumn("Harga").setCellEditor(null);
//                        
//                    }
//                }
//            }
//        });
    }

    public void setConn(Connection con) {
        this.conn = con;
    }

    private void udfInitForm() {
        //txtNoInvoice.setEnabled(false);
        try {
            aThis = this;
            lookup = new DlgLookup(JOptionPane.getFrameForComponent(this), true);
            fn.addKeyListenerInContainer(jPanel5, kListener, txtFocusListener);
            fn.addKeyListenerInContainer(jPanel1, kListener, txtFocusListener);
            fn.addKeyListenerInContainer(jPanel2, kListener, txtFocusListener);
            fn.addKeyListenerInContainer(jPanel3, kListener, txtFocusListener);
            fn.addKeyListenerInContainer(jPanel4, kListener, txtFocusListener);
            fn.addKeyListenerInContainer(panelBarcode, kListener, txtFocusListener);
            jScrollPane1.addKeyListener(kListener);
            tblDetail.addKeyListener(kListener);
            itemDao = new ItemDao(conn);
            RelasiDao relasiDao = new RelasiDao(conn);
            Relasi r = relasiDao.cariByID(1);
            if (r != null) {
                txtCustomer.setText(String.valueOf(r.getIdRelasi()));
                lblCustomer.setText(r.getNamaRelasi());
            }
            r = null;

            invDao.setConn(conn);
            tblDetail.getColumn("No").setPreferredWidth(40);
            tblDetail.getColumn("ProductID").setPreferredWidth(80);
            tblDetail.getColumn("Nama Barang").setPreferredWidth(350);
            tblDetail.getColumn("Keterangan").setPreferredWidth(200);
            
            if(!Konfigurasi.TAMPILKAN_HPP_PENJUALAN) {
                tblDetail.getColumn("Hpp").setPreferredWidth(0);
                tblDetail.getColumn("Hpp").setMinWidth(0);
                tblDetail.getColumn("Hpp").setMaxWidth(0);
            }
            tblDetail.setRowHeight(22);
            jXDatePicker1.setFormats(new String[]{"dd/MM/yyyy"});
            tblDetail.getModel().addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                    double total = 0, totalDiskon = 0, sub = 0;
                    int iRow = tblDetail.getSelectedRow();
                    TableColumnModel col = tblDetail.getColumnModel();
                    if (e.getColumn() == col.getColumnIndex("Qty")
                            || e.getColumn() == col.getColumnIndex("Diskon (Rp)")
                            || e.getColumn() == col.getColumnIndex("Harga")) {
                        double nett = fn.udfGetDouble(tblDetail.getValueAt(iRow, col.getColumnIndex("Harga"))) * fn.udfGetDouble(tblDetail.getValueAt(iRow, col.getColumnIndex("Qty")));
//                                nett=fn.getDiscBertingkat(nett, tblDetail.getValueAt(iRow, col.getColumnIndex("Disc")).toString());
//                                nett=nett*(1+fn.udfGetDouble(tblDetail.getValueAt(iRow, col.getColumnIndex("PPn")))/100);
                        nett -= fn.udfGetDouble(tblDetail.getValueAt(iRow, col.getColumnIndex("Diskon (Rp)"))) * fn.udfGetDouble(tblDetail.getValueAt(iRow, col.getColumnIndex("Qty")));

                        tblDetail.setValueAt(nett, iRow, col.getColumnIndex("Sub Total"));
                    }

                    int colSubTotal = tblDetail.getColumnModel().getColumnIndex("Sub Total");
                    int colDiskon = tblDetail.getColumnModel().getColumnIndex("Diskon (Rp)");
                    int colQty = tblDetail.getColumnModel().getColumnIndex("Qty");
                    int colHarga = tblDetail.getColumnModel().getColumnIndex("Harga");
                    for (int i = 0; i < tblDetail.getRowCount(); i++) {
                        sub = GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, colQty)) * GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, colHarga));
//                        total += GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, colSubTotal));
                        total += sub;
                        totalDiskon += GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, colQty)) * GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, colDiskon));
                    }
                    lblSubTotal.setText(GeneralFunction.intFmt.format(total));
                    lblDiskon.setText(GeneralFunction.intFmt.format(totalDiskon));
                    setGrandTotal();
                }
            });
            
            tblDetail.getColumn("Qty").setCellEditor(cEditor);
            tblDetail.getColumn("Harga").setCellEditor(cEditor);
//            tblDetail.getColumn("Disc").setCellEditor(cEditor);
//            tblDetail.getColumn("PPn").setCellEditor(cEditor);
            tblDetail.getColumn("Diskon (Rp)").setCellEditor(cEditor);
            tblDetail.getColumn("Keterangan").setCellEditor(cEditor);

            if (idPenjualan != null) {
                tampilkanData(idPenjualan);
            }
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    txtBarcode.requestFocusInWindow();
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setGrandTotal() {
        double gTotal = GeneralFunction.udfGetDouble(lblSubTotal.getText());
        double discRp = GeneralFunction.udfGetDouble(lblDiskon.getText());
        gTotal -= discRp;
        lblGrandTotal.setText(fn.intFmt.format(gTotal));
    }

    private void udfLookupItem(String cari) {
//        if(tblPaket.getSelectedRow()<0)
//            return;
//        
        DlgLookup d1 = new DlgLookup(JOptionPane.getFrameForComponent(aThis), true);
//        String sItem="";
//        int colKode=tblDetail.getColumnModel().getColumnIndex("ProductID");
//        for(int i=0; i< tblDetail.getRowCount(); i++){
//            sItem+=(sItem.length()==0? "" : ",") +"'"+tblDetail.getValueAt(i, colKode).toString()+"'";
//        }

        String s = "select *  from ("
                + "select i.id::varchar as id ,coalesce(i.plu,'') as kode, i.nama_barang, coalesce(k.nama_kategori,'') as kategori, "
                + "coalesce(h.harga_jual,0) harga_jual, coalesce(i.stok,0) stok, coalesce(i.satuan,'') as satuan, coalesce(i.biaya_item,0) as biaya_item "
                + "from m_item i "
                + "left join m_item_kategori k on k.id=i.id_kategori "
                + "left join m_item_harga_jual h on h.id_item=i.id and h.id_tipe_harga_jual=1 "
                + "where coalesce(i.active, true)=true "
                //+(sItem.length()>0? "where i.id::varchar not in("+sItem+")":"")+" "
                + "order by upper(i.nama_barang)  "
                + ")x ";

        System.out.println(s);
        d1.setTitle("Lookup Stok");
        JTable tbl = d1.getTable();
        d1.setSearch(txtBarcode.getText());
        d1.udfLoad(conn, s, "(x.id||x.kode||x.nama_barang||kategori)", null);
        tbl.getColumn("id").setPreferredWidth(0);
        tbl.getColumn("id").setMinWidth(0);
        tbl.getColumn("id").setMaxWidth(0);
//        d1.setBounds(getScree, 0, 900, 500);
        d1.setSize(900, 500);
        d1.setLocationRelativeTo(null);
        d1.setVisible(true);

        if (d1.getKode().length() > 0) {
            TableColumnModel col = d1.getTable().getColumnModel();
            int iRow = tbl.getSelectedRow();
            setTableRow(Integer.valueOf(tbl.getValueAt(iRow, col.getColumnIndex("id")).toString()));
        }
    }

    private SimpleDateFormat yymm = new SimpleDateFormat("yyMM");

    private boolean udfCekBeforeSave() {
        btnSave.requestFocusInWindow();
        if (!btnSave.isEnabled()) {
            return false;
        }
        if (txtCustomer.getText().trim().length() == 0) {
            JOptionPane.showMessageDialog(aThis, "Silahkan pilih nama toko terlebih dulu!");
            txtCustomer.requestFocusInWindow();
            return false;
        }
//        if (txtNoInvoice.getText().trim().length() == 0) {
//            JOptionPane.showMessageDialog(aThis, "Silahkan isi Nomor Invoice terlebih dulu!");
//            txtNoInvoice.requestFocusInWindow();
//            return false;
//        }
        if (tblDetail.getRowCount() == 0) {
            JOptionPane.showMessageDialog(aThis, "Item yang diterima masih belum dimasukkan!");
            txtBarcode.requestFocusInWindow();
            return false;
        }
        if(!cmbStatus.getSelectedItem().toString().substring(0, 1).equalsIgnoreCase("T") && txtCustomer.getText().equalsIgnoreCase("1")) {
            JOptionPane.showMessageDialog(this, "Silahkan rubah customer menjadi NON CASH");
            txtCustomer.requestFocusInWindow();
            return false;
        }
//        if(cmbStatus.getSelectedItem().toString().equalsIgnoreCase("TUNAI") && 
//                fn.udfGetDouble(txtBayar.getText())<fn.udfGetDouble(lblGrandTotal.getText()) ){
//            JOptionPane.showMessageDialog(aThis, 
//                "Untuk status TUNAI, Pembayaran harus lebih besar atau sama dengan Grand Total!", "Pembayaran", JOptionPane.YES_NO_OPTION);
//            txtBayar.requestFocusInWindow();
//            return false;
//        }
//        if (txtNoInvoice.getText().trim().length() == 0) {
//            try {
//                txtNoInvoice.setText(invDao.getNoInvoice(cmbStatus.getSelectedItem().toString().substring(0, 1).equalsIgnoreCase("K"), 
//                        GeneralFunction.yyyymmdd_format.format(jXDatePicker1.getDate())));
//            } catch (SQLException ex) {
//                Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
//                return false;
//            }
//        } else if (idPenjualan == null && invDao.isExistsInvoiceNo(txtNoInvoice.getText())) {
//            JOptionPane.showMessageDialog(aThis, "Nomor Invoice sudah dipakai!");
//            txtNoInvoice.requestFocusInWindow();
//            return false;
//        }

        return true;
    }

    private void caraBayar(){
        if (!udfCekBeforeSave()) {
            return;
        }
        DlgPembayaran d = new DlgPembayaran(JOptionPane.getFrameForComponent(aThis), true);
        d.setTotal(fn.udfGetDouble(lblGrandTotal.getText()));
        d.setNoTrx(txtNoInvoice.getText());
        d.setFormPenjualan(this);
        d.setSrcForm(srcForm);
        d.setVisible(true);
    }
    public Integer udfSave(Double bayar) {
        if(bayar < GeneralFunction.udfGetDouble(lblGrandTotal.getText()) && txtCustomer.getText().equalsIgnoreCase("1")) {
            JOptionPane.showMessageDialog(this, "Silahkan rubah customer menjadi NON CASH");
            txtCustomer.requestFocusInWindow();
            return 0;
        }
        double piutang = bayar >= fn.udfGetDouble(lblGrandTotal.getText()) ? 0 : fn.udfGetDouble(lblGrandTotal.getText()) - bayar;
        if (txtNoInvoice.getText().trim().length() == 0) {
            try {
                txtNoInvoice.setText(invDao.getNoInvoice(piutang>0? true: false, 
                        GeneralFunction.yyyymmdd_format.format(jXDatePicker1.getDate())));
            } catch (SQLException ex) {
                Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
                return 0;
            }
        } else if (idPenjualan == null && invDao.isExistsInvoiceNo(txtNoInvoice.getText())) {
            JOptionPane.showMessageDialog(aThis, "Nomor Invoice sudah dipakai!");
            txtNoInvoice.requestFocusInWindow();
            return 0;
        }
        try {
                Timestamp timeServer = fn.getTimeServer();
                ArInv header = new ArInv();
                header.setId(idPenjualan);
                header.setCustomerId(Integer.parseInt(txtCustomer.getText()));
                header.setInvoiceNo(txtNoInvoice.getText());
                header.setDescription(txtKeterangan.getText());
                header.setInvoiceDate(jXDatePicker1.getDate());
                header.setTop(0);
                header.setInvoiceAmount(fn.udfGetDouble(lblGrandTotal.getText()));
                header.setItemAmount(fn.udfGetDouble(lblSubTotal.getText()));
//                header.setJenisBayar(cmbStatus.getSelectedItem().toString().substring(0, 1));
                header.setPaidAmount(bayar);
                header.setJenisBayar(piutang>0? "K": "T");
                header.setOwing(piutang);
                header.setIdGudang(MainForm.idGudang);
                header.setArDisc("0");
                header.setBiayaLain(new Double(0));
                header.setTimeIns(idPenjualan == null ? new java.sql.Timestamp(fn.now()) : timeIns);
                header.setUserIns(idPenjualan == null ? MainForm.sUserName : (userIns == null ? MainForm.sUserName : userIns));
                header.setTimeUpd(new Date(timeServer.getTime()));
                header.setUserUpd(MainForm.sUserName);
                header.setIdSales(null);
                header.setIdExpedisi(null);
                header.setKetPembayaran("");
                header.setDisiapkanOleh("");
                header.setDiperiksaOleh("");
                header.setDiterimaOleh("");
                header.setTrxType("PJL");
                header.setShift(MainForm.shift);
                conn.setAutoCommit(false);
                invDao.simpanHeader(header);

                List<ArInvDet> detail = new ArrayList<ArInvDet>();
                TableColumnModel col = tblDetail.getColumnModel();

                for (int i = 0; i < tblDetail.getRowCount(); i++) {
                    ArInvDet det = new ArInvDet();
                    det.setArId(header.getId());
                    det.setItemId(Integer.parseInt(tblDetail.getValueAt(i, col.getColumnIndex("ProductID")).toString()));
                    det.setQty(GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, col.getColumnIndex("Qty"))));
                    det.setUnitPrice(new Double(GeneralFunction.udfGetInt(tblDetail.getValueAt(i, col.getColumnIndex("Harga")))));
//                det.setDisc(tblDetail.getValueAt(i, col.getColumnIndex("Disc")).toString());
//                det.setPpn(GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, col.getColumnIndex("PPn"))));
                    det.setDisc(tblDetail.getValueAt(i, col.getColumnIndex("Diskon (Rp)")) == null ? "0" : tblDetail.getValueAt(i, col.getColumnIndex("Diskon (Rp)")).toString());
                    det.setPpn(new Double(0));
                    det.setUnitCost(GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, col.getColumnIndex("Hpp"))));
                    det.setLastCost(GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, col.getColumnIndex("Hpp"))));
                    det.setBiaya(new Double(0));
                    det.setBiayaItem(GeneralFunction.udfGetDouble(tblDetail.getValueAt(i, col.getColumnIndex("Biaya Item"))));
                    det.setKeterangan(tblDetail.getValueAt(i, col.getColumnIndex("Keterangan")).toString());
                    det.setDetId(tblDetail.getValueAt(i, col.getColumnIndex("ID")) == null ? null : GeneralFunction.udfGetInt(tblDetail.getValueAt(i, col.getColumnIndex("ID"))));
                    detail.add(det);
                }

                invDao.simpanArInvDetail(detail, idPenjualan == null);
                idPenjualan = header.getId();

                if(piutang > 0 && bayar >0) {
                    List<ArReceiptDet> details = new ArrayList<>();
                    details.add(new ArReceiptDet("", idPenjualan, bayar, new Double(0)));
                    ArReceipt r = new ArReceipt("", 
                            jXDatePicker1.getDate(), 
                            GeneralFunction.udfGetInt(txtCustomer.getText()), "CS", 1, "DP", "", "", bayar, jXDatePicker1.getDate(), details);
                    String noPembayaran = new ArReceiptDao().simpan(r);
                    System.out.println("Simpan pembayaran dengan nomor: " + noPembayaran);
                }
                conn.setAutoCommit(true);
                return idPenjualan;
//                if (srcForm != null) {
//                if(srcForm instanceof FrmPembelianHis){
//                    if(!isNew)
//                        {((FrmPembelianHis)srcForm).udfRefreshDataPerBaris(header.getId());}
//                    else
//                        {((FrmPembelianHis)srcForm).udfLoadData(header.getId());}
//                }
//                }
//                JOptionPane.showMessageDialog(this, "Data penjualan tersimpan!");
                
        } catch (SQLException se) {
            try {
                conn.rollback();
                conn.setAutoCommit(true);
                System.err.println(se.getMessage() + "lanjut:" + se.getNextException());
                if (se.getMessage().indexOf("Key (invoice_no)=(" + txtNoInvoice.getText() + ") already exists") > 0) {
                    JOptionPane.showMessageDialog(this, "Nomor invoice '" + txtNoInvoice.getText() + "' telah terpakai!\n"
                            + "Silahkan masukkan nomor lain!");
                    txtNoInvoice.requestFocus();
                    return 0 ;
                }
                JOptionPane.showMessageDialog(this, se.getMessage() + se.getNextException());
            } catch (SQLException ex) {
                Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        cmbStatus = new javax.swing.JComboBox();
        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtNoInvoice = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblCustomer = new javax.swing.JLabel();
        txtCustomer = new javax.swing.JTextField();
        lblAlamat = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtKeterangan = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetail = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnClose = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnClear1 = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblGrandTotal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblSubTotal = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblDiskon = new javax.swing.JLabel();
        panelBarcode = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtBarcode = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setTitle("Penjualan Barang");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel22.setText("Status :");
        jPanel4.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 90, 20));

        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TUNAI", "KREDIT" }));
        cmbStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbStatusItemStateChanged(evt);
            }
        });
        jPanel4.add(cmbStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 60, 170, -1));

        jXDatePicker1.setEditable(false);
        jPanel4.add(jXDatePicker1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 35, 170, -1));

        jLabel23.setText("Tanggal Faktur :");
        jPanel4.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 105, 20));

        jLabel24.setText("No. Faktur :");
        jPanel4.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 20));

        txtNoInvoice.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNoInvoice.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNoInvoice.setEnabled(false);
        txtNoInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNoInvoiceActionPerformed(evt);
            }
        });
        jPanel4.add(txtNoInvoice, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 170, 20));

        jLabel2.setText("Keterangan :");

        lblCustomer.setText("UMUM");
        lblCustomer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        txtCustomer.setText("1");
        txtCustomer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtCustomer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCustomerKeyReleased(evt);
            }
        });

        lblAlamat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel3.setText("Nama Toko :");

        jLabel9.setText("Alamat :");

        txtKeterangan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtKeterangan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtKeteranganKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(txtCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(lblCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtKeterangan, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)
                        .addComponent(lblAlamat, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(3, 3, 3)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtKeterangan, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tblDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Kode", "Nama Barang", "Qty", "Satuan", "Harga", "Diskon (Rp)", "Sub Total", "Hpp", "Biaya Item", "Keterangan", "ProductID", "ID"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, true, true, false, false, false, true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDetail.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(tblDetail);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        btnClose.setText("Tutup");
        btnClose.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        jPanel3.add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, 90, 30));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/disk.png"))); // NOI18N
        btnSave.setText("Bayar (End)");
        btnSave.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jPanel3.add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 10, 140, 30));

        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/page.png"))); // NOI18N
        btnClear.setText("Bersihkan (F4)");
        btnClear.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        jPanel3.add(btnClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 160, 30));

        jLabel4.setBackground(new java.awt.Color(204, 255, 255));
        jLabel4.setForeground(new java.awt.Color(0, 0, 153));
        jLabel4.setText("<html>\n &nbsp <b>Del &nbsp &nbsp &nbsp : </b> &nbsp  Menghapus item pembelian  <br>\n &nbsp <b>Insert : </b> &nbsp Menambah ItemTransaski<br>\n</html>"); // NOI18N
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel4.setOpaque(true);
        jLabel4.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 280, -1));

        btnClear1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/page.png"))); // NOI18N
        btnClear1.setText("Buka Baru");
        btnClear1.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear1ActionPerformed(evt);
            }
        });
        jPanel3.add(btnClear1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 10, 160, 30));

        jLabel19.setText("Sales");

        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        jLabel1.setText("PENJUALAN");

        lblGrandTotal.setFont(new java.awt.Font("Ubuntu", 1, 70)); // NOI18N
        lblGrandTotal.setForeground(new java.awt.Color(26, 60, 217));
        lblGrandTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblGrandTotal.setText("0");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblSubTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblSubTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubTotal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(lblSubTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 5, 140, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Total Diskon");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 100, 20));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Sub Total :");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 5, 100, 20));

        lblDiskon.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblDiskon.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiskon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(lblDiskon, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 140, 20));

        panelBarcode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelBarcode.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Kode Barang/ Barcode");
        panelBarcode.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 150, 20));

        txtBarcode.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtBarcode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtBarcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBarcodeActionPerformed(evt);
            }
        });
        panelBarcode.add(txtBarcode, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, 190, 25));

        jLabel5.setForeground(new java.awt.Color(50, 89, 212));
        jLabel5.setText("<html>\n<b>F12 :</b> Fokus Barcode<br>\n<b>F11 :</b> Fokus ke Qty Tabel<br>\n<b>Ins :</b> Lookup Item<br>\n</html>");
        panelBarcode.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, 190, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblGrandTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(13, 13, 13))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(590, 590, 590)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelBarcode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblGrandTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(216, 216, 216)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelBarcode, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );

        setBounds(0, 0, 901, 641);
    }// </editor-fold>//GEN-END:initComponents

    private void txtCustomerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCustomerKeyReleased
        String sQry = "select id_relasi::varchar as id_customer, coalesce(nama_relasi,'') as nama_customer, "
                + "coalesce(alamat,'')||' - '||coalesce(kota.nama_kota,'') as alamat "
                + "from m_relasi s "
                + "left join m_kota kota on kota.id=s.id_kota "
                + "where is_cust=true and id_relasi::varchar||coalesce(nama_relasi,'')||"
                + "coalesce(alamat,'')||' - '||coalesce(kota.nama_kota,'') "
                + "ilike '%" + txtCustomer.getText() + "%' order by 2";
        fn.lookup(evt, new Object[]{lblCustomer, lblAlamat}, sQry, txtCustomer.getWidth() + lblCustomer.getWidth(), 300);
    }//GEN-LAST:event_txtCustomerKeyReleased

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        caraBayar();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
        
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        udfClear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtNoInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNoInvoiceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNoInvoiceActionPerformed


    private void cmbStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbStatusItemStateChanged
//        try {
//            txtTOP.setText(cmbStatus.getSelectedIndex() == 0 || txtCustomer.getText().equalsIgnoreCase("") ? "0"
//                    : new RelasiDao(conn).cariByID(Integer.parseInt(txtCustomer.getText())).getTop().toString()
//            );
//            setDueDate();
//        } catch (Exception ex) {
//            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }//GEN-LAST:event_cmbStatusItemStateChanged

    private void txtBarcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBarcodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBarcodeActionPerformed

    private void txtKeteranganKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtKeteranganKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtKeteranganKeyReleased

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        udfInitForm();
    }//GEN-LAST:event_formWindowOpened

    private void btnClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear1ActionPerformed
        FrmPenjualan p = new FrmPenjualan();
        p.setConn(conn);
        p.setSrcForm(this);
        p.setVisible(true);
    }//GEN-LAST:event_btnClear1ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        fn.setVisibleList(false);
    }//GEN-LAST:event_formWindowClosed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClear1;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox cmbStatus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    private javax.swing.JLabel lblAlamat;
    private javax.swing.JLabel lblCustomer;
    private javax.swing.JLabel lblDiskon;
    private javax.swing.JLabel lblGrandTotal;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JPanel panelBarcode;
    private javax.swing.JTable tblDetail;
    private javax.swing.JTextField txtBarcode;
    private javax.swing.JTextField txtCustomer;
    private javax.swing.JTextField txtKeterangan;
    private javax.swing.JTextField txtNoInvoice;
    // End of variables declaration//GEN-END:variables

    public void tampilkanData(Integer id) {
        try {
            String sQry = "select h.id, h.invoice_date as tanggal, h.id_customer, coalesce(c.nama_relasi,'') as nama_customer, "
                    + "coalesce(c.alamat,'')||' - '||coalesce(k.nama_kota,'') as alamat, "
                    + "coalesce(h.invoice_no,'') as invoice_no, to_Char(h.invoice_date+coalesce(h.top,0), 'dd/MM/yyyy') as jt_tempo, "
                    + "coalesce(h.top,0) as top, coalesce(h.ar_disc,'') as ar_disc , coalesce(h.biaya_lain,0) as biaya_lain, coalesce(h.paid_amount,0) as paid_amount, "
                    + "coalesce(g.nama_gudang,'') as nama_gudang, coalesce(h.description,'') as description, "
                    + "coalesce(h.disiapkan_oleh,'') as disiapkan_oleh, coalesce(h.no_so,'') as no_so, "
                    + "coalesce(h.diperiksa_oleh,'') as diperiksa_oleh, "
                    + "coalesce(h.diterima_oleh,'') as diterima_oleh, "
                    + "coalesce(u.complete_name,'') as dibuat_oleh, "
                    + "coalesce(s.nama_sales,'') as nama_sales,"
                    + "coalesce(h.id_expedisi::varchar,'') as id_expedisi, "
                    + "coalesce(e.nama_expedisi,'') as nama_expedisi, "
                    + "case when h.jenis_bayar='K' then 'KREDIT' else 'TUNAI' end as jenis_bayar,"
                    + "coalesce(ket_pembayaran,'') as ket_pembayaran, "
                    + "coalesce(h.user_ins,'') as user_ins, h.time_ins "
                    + "from ar_inv h "
                    + "left join m_relasi c on c.id_relasi=h.id_customer "
                    + "left join m_kota k on k.id=c.id_kota "
                    + "left join m_gudang g on g.id=h.id_gudang "
                    + "left join m_salesman s on s.id_sales=h.id_sales "
                    + "left join m_Expedisi e on h.id_expedisi = e.id "
                    + "left join m_user u on u.user_name=h.user_ins "
                    + "where h.id=" + id + " ";
            ResultSet rs = conn.createStatement().executeQuery(sQry);
            if (rs.next()) {
                idPenjualan = rs.getInt("id");
                jXDatePicker1.setDate(rs.getDate("tanggal"));
                txtCustomer.setText(rs.getString("id_customer"));
                txtNoInvoice.setText(rs.getString("invoice_no"));
                lblCustomer.setText(rs.getString("nama_customer"));
                lblAlamat.setText(rs.getString("alamat"));
                cmbStatus.setSelectedItem(rs.getString("jenis_bayar"));
                txtKeterangan.setText(rs.getString("description"));

                //txtBiayaLain.setText(fn.intFmt.format(rs.getInt("biaya_lain")));
                userIns = rs.getString("user_ins");
                System.out.println("UserIns: " + userIns);
                timeIns = rs.getTimestamp("time_ins");
                rs.close();

                sQry = "select d.ar_id, d.id_barang, coalesce(i.plu,'') as plu, i.nama_barang, d.qty, d.unit_price, d.disc, d.ppn, coalesce(i.satuan,'') as satuan,"
                        + "(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), "
                        + "coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100))+coalesce(d.biaya,0) as sub_total, coalesce(d.last_Cost,0) as last_cost, "
                        + "coalesce(d.biaya,0) as biaya, coalesce(d.keterangan,'') as keterangan, d.det_id "
                        + "from ar_inv_det d  "
                        + "left join m_item i on i.id=d.id_barang "
                        + "where d.ar_id=" + id + " "
                        + "order by d.seq";
                ((DefaultTableModel) tblDetail.getModel()).setNumRows(0);
                rs = conn.createStatement().executeQuery(sQry);
                while (rs.next()) {
                    ((DefaultTableModel) tblDetail.getModel()).addRow(new Object[]{
                        tblDetail.getRowCount() + 1,
                        rs.getString("plu"),
                        rs.getString("nama_barang"),
                        rs.getDouble("qty"),
                        rs.getString("satuan"),
                        rs.getDouble("unit_price"),
                        //                        rs.getString("disc"),
                        //                        rs.getDouble("ppn"),
                        rs.getDouble("biaya"),
                        rs.getDouble("sub_Total"),
                        rs.getDouble("last_cost"),
                        rs.getString("keterangan"),
                        rs.getInt("id_barang"),
                        rs.getInt("det_id")
                    });
                }
                rs.close();
                if (tblDetail.getRowCount() > 0) {
                    tblDetail.setRowSelectionInterval(0, 0);
                    tblDetail.setModel(fn.autoResizeColWidth(tblDetail, ((DefaultTableModel) tblDetail.getModel())).getModel());
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setIdPenjualan(Integer id) {
        idPenjualan = id;
    }

    public void setSrcForm(Object aThis) {
        this.srcForm = aThis;
    }

    void setIsNew(boolean b) {
        this.isNew = b;
    }

    public void udfClear() {
        idPenjualan = null;
        btnClear.requestFocusInWindow();
        txtNoInvoice.setText("");
        txtBarcode.setText("");
        txtCustomer.setText("1");
        lblCustomer.setText("CASH");
        lblAlamat.setText("");
        lblGrandTotal.setText("0");
        lblSubTotal.setText("0");
        ((DefaultTableModel) tblDetail.getModel()).setNumRows(0);
        txtBarcode.requestFocusInWindow();
    }

    PrintTrxService printTrxService= new PrintTrxService();
    private void udfPrintPenjualan() {
        printTrxService.cetakFakturPenjualan(idPenjualan, aThis);
    }

//    private void bayar() {
//        FrmArReceipt f1 = new FrmArReceipt();
//        MainForm.jDesktopImage2.add(f1);
//        f1.setBounds(0, 0, f1.getWidth(), f1.getHeight());
//        f1.setConn(conn);
//        f1.setId(idPenjualan);
//        f1.setVisible(true);
//    }

    private void setTableRow(Integer id) {
        try {
            double nett = 0;
            double harga, ppn;
            double disc;
            //            double saldo = itemDao.getSaldo(Integer.valueOf(tbl.getValueAt(iRow, col.getColumnIndex("id")).toString()),
//                    MainForm.idGudang);
//            if (saldo <= 0) {
//                JOptionPane.showMessageDialog(aThis, "Stok tidak mencukupi!");
//                return;
//            }
            String query = "select * from fn_get_harga_item_pelanggan(" + id + ", " + fn.udfGetInt(txtCustomer.getText()) + ", " + MainForm.idGudang + ") "
                    + "as (id integer, plu varchar, barcode varchar, nama_barang varchar, satuan varchar, hpp double precision, \n"
                    + "harga_jual double precision, disc double precision, ppn double precision, biaya_item double precision, stok double precision)";
            ResultSet rs = MainForm.conn.createStatement().executeQuery(query);
            if (rs.next()) {
                harga = rs.getDouble("harga_jual");
                if (harga == 0) {
                    JOptionPane.showMessageDialog(aThis, "Harga jual belum disetting");
                    return;
                }
                disc = rs.getDouble("disc");
                ppn = rs.getDouble("ppn");
//            nett = fn.getDiscBertingkat(harga, disc);
                nett = harga - disc;
                nett = nett * (1 + ppn / 100);

                ((DefaultTableModel) tblDetail.getModel()).addRow(new Object[]{
                    tblDetail.getRowCount() + 1,
                    rs.getString("plu"),
                    rs.getString("nama_barang"),
                    1,
                    rs.getString("satuan"),
                    harga,
                    0,
                    nett,
                    rs.getDouble("hpp"),
                    rs.getDouble("biaya_item"),
                    "",
                    id,
                    null
                });

                tblDetail.setRowSelectionInterval(tblDetail.getRowCount() - 1, tblDetail.getRowCount() - 1);
//                tblDetail.requestFocusInWindow();
                tblDetail.changeSelection(tblDetail.getRowCount() - 1, tblDetail.getColumnModel().getColumnIndex("Qty"), false, false);
                txtBarcode.requestFocusInWindow();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class MyKeyListener extends KeyAdapter {

        @Override
        public void keyTyped(java.awt.event.KeyEvent evt) {

//            if(getTableSource()==null)
//                return;
            if (evt.getSource() instanceof JTextField
                    && ((JTextField) evt.getSource()).getName() != null
                    && ((JTextField) evt.getSource()).getName().equalsIgnoreCase("textEditor")
                    && tblDetail.getSelectedColumn() != IDX_KOLOM_KETERANGAN) {
                fn.keyTyped(evt);

            }

        }

        public void keyPressed(KeyEvent evt) {
            Component ct = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            int keyKode = evt.getKeyCode();
            switch (keyKode) {
                case KeyEvent.VK_ENTER: {
                    if (!(ct instanceof JTable)) {
                        if (!fn.isListVisible()) {
                            if (evt.getSource().equals(txtBarcode) && txtBarcode.getText().length() > 0) {
                                //cari barang dengan kode barcode 
                                Integer id = itemDao.cariIDByBarcode(txtBarcode.getText());
                                if (id == null) {

                                    List<Item> items = itemDao.filter(txtBarcode.getText());
                                    if (items.size() == 0) {
                                        JOptionPane.showMessageDialog(aThis, "Item tidak ditemukan!");
                                        txtBarcode.setText("");
                                    } else if (items.size() == 1) {
                                        int colId = tblDetail.getColumnModel().getColumnIndex("ProductID");
                                        id = items.get(0).getId();
                                        for (int i = 0; i < tblDetail.getRowCount(); i++) {
                                            if (id == fn.udfGetInt(tblDetail.getValueAt(i, colId))) {
                                                int colQty = tblDetail.getColumnModel().getColumnIndex("Qty");
                                                tblDetail.setValueAt(fn.udfGetInt(tblDetail.getValueAt(i, colQty)) + 1, i, colQty);
                                                txtBarcode.setText("");
                                                txtBarcode.requestFocusInWindow();
                                                return;
                                            }
                                        }
                                        setTableRow(items.get(0).getId());
                                        txtBarcode.setText("");
                                        txtBarcode.requestFocusInWindow();
                                        return;
                                    } else {
                                        udfLookupItem(txtBarcode.getText());
                                        txtBarcode.setText("");
                                        txtBarcode.requestFocusInWindow();
                                        return;
                                    }

                                } else {

                                    setTableRow(id);
                                    txtBarcode.setText("");
                                    txtBarcode.requestFocusInWindow();
                                    return;
                                }
                            }
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (!(ct.getClass().getSimpleName().equalsIgnoreCase("JTABLE"))) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                        break;
                    }
                }

                case KeyEvent.VK_UP: {
                    if (!(evt.getSource() instanceof JTable)) {
                        Component c = findPrevFocus();
                        c.requestFocus();
                    }
                    break;
                }
                case KeyEvent.VK_F4: {
                    udfClear();
                    break;
                }
                case KeyEvent.VK_END: {
                    caraBayar();

                    break;
                }
                case KeyEvent.VK_F11: {
                    if (aThis.isShowing()) {
                        tblDetail.requestFocusInWindow();
                        tblDetail.changeSelection(tblDetail.getRowCount() - 1, tblDetail.getColumnModel().getColumnIndex("Qty"), false, false);
                    }

                    break;
                }
                case KeyEvent.VK_F12: {
                    if (aThis.isShowing()) {
                        txtBarcode.requestFocusInWindow();
                    }

                    break;
                }
                case KeyEvent.VK_INSERT: {
                    udfLookupItem("");

                    break;
                }
//                case KeyEvent.VK_ESCAPE:{
//                    dispose();
//                    break;
//                }

                case KeyEvent.VK_DELETE: {
                    if (evt.getSource().equals(tblDetail) && tblDetail.getSelectedRow() >= 0) {
                        int iRow = tblDetail.getSelectedRow();
                        if (iRow < 0) {
                            return;
                        }

                        ((DefaultTableModel) tblDetail.getModel()).removeRow(iRow);
                        if (tblDetail.getRowCount() < 0) {
                            return;
                        }
                        if (tblDetail.getRowCount() > iRow) {
                            tblDetail.setRowSelectionInterval(iRow, iRow);
                        } else {
                            tblDetail.setRowSelectionInterval(0, 0);
                        }
                    }
                    break;

                }

            }
        }

//        @Override
//        public void keyReleased(KeyEvent evt){
//            if(evt.getSource().equals(txtDisc)||evt.getSource().equals(txtQty)||evt.getSource().equals(txtUnitPrice))
//                GeneralFunction.keyTyped(evt);
//        }
        public Component findNextFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component nextFocus = policy.getComponentAfter(root, c);
                if (nextFocus == null) {
                    nextFocus = policy.getDefaultComponent(root);
                }
                return nextFocus;
            }
            return null;
        }

        public Component findPrevFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component prevFocus = policy.getComponentBefore(root, c);
                if (prevFocus == null) {
                    prevFocus = policy.getDefaultComponent(root);
                }
                return prevFocus;
            }
            return null;
        }
    }

    private FocusListener txtFocusListener = new FocusListener() {
        public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.YELLOW);
                if ((e.getSource() instanceof JTextField && ((JTextField) e.getSource()).getName() != null && ((JTextField) e.getSource()).getName().equalsIgnoreCase("textEditor"))) {
                    ((JTextField) e.getSource()).setSelectionStart(0);
                    ((JTextField) e.getSource()).setSelectionEnd(((JTextField) e.getSource()).getText().length());

                }
            } else if (e.getSource() instanceof JXDatePicker) {
                ((JXDatePicker) e.getSource()).setBackground(Color.YELLOW);
            }
        }

        public void focusLost(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.WHITE);
            }
        }
    };

    public class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

        private Toolkit toolkit;
        JTextComponent text = new JTextField() {

            @Override
            public void setFont(Font f) {
                super.setFont(new java.awt.Font("Tahoma", Font.BOLD, 11)); // NOI18N
            }

            protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {
                if (hasFocus()) {
                    return super.processKeyBinding(ks, e, condition, pressed);
                } else {
                    this.requestFocus();

                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            processKeyBinding(ks, e, condition, pressed);
                        }
                    });
                    return true;
                }
            }
        };
        int col, row;
//        private NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        //private NumberFormat nf = new ;

        public Component getTableCellEditorComponent(JTable table, Object value,
                boolean isSelected, int rowIndex, int vColIndex) {
            
            col = vColIndex;
            row = rowIndex;
//            if(IDX_KOLOM_HARGA==col && !bisaEditHarga) {
//                return null;
//            }
            text.setBackground(new Color(0, 255, 204));
            text.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
            text.addFocusListener(txtFocusListener);
            text.addKeyListener(kListener);
            text.setFont(table.getFont());
            text.setName("textEditor");
            text.removeKeyListener(kListener);
            text.setMargin(new Insets(2, 2, 2, 2));
//           AbstractDocument doc = (AbstractDocument)text.getDocument();
//           doc.setDocumentFilter(null);
//           doc.setDocumentFilter(new FixedSizeFilter(iText));

            text.removeKeyListener(kListener);
            text.addKeyListener(kListener);

            if (isSelected) {
            }

            if (value instanceof Double || value instanceof Float || value instanceof Integer) {
//                try {
                //Daouble dVal=Double.parseDouble(value.toString().replace(",",""));
                double dVal = fn.udfGetFloat(value.toString());
//                text.setText(nf.format(dVal));
                text.setText(String.valueOf(dVal).replace(",00", ""));
//                } catch (java.text.ParseException ex) {
//                    //Logger.getLogger(DlgLookupBarang.class.getName()).log(Level.SEVERE, null, ex);
//                }
            } else {
                text.setText(value == null ? "" : value.toString());
            }
            return text;
        }

        public Object getCellEditorValue() {
            Object retVal = 0;
            try {
                if (col == tblDetail.getColumnModel().getColumnIndex("Harga")
                        || col == tblDetail.getColumnModel().getColumnIndex("Diskon (Rp)")
                        || col == tblDetail.getColumnModel().getColumnIndex("Qty")) {
//                    if (col == tblDetail.getColumnModel().getColumnIndex("Qty")) {
//                        Double saldo = itemDao.getSaldo(
//                                fn.udfGetInt(tblDetail.getValueAt(row, tblDetail.getColumnModel().getColumnIndex("ProductID"))),
//                                MainForm.idGudang);
//                        Double qty = fn.udfGetDouble(((JTextField) text).getText());
//                        if (saldo < qty) {
//                            String msg = "Saldo tidak mencukupi!\n"
//                                    + "Saldo saat ini adalah " + fn.intFmt.format(saldo);
//
//                            JOptionPane.showMessageDialog(aThis, msg);
//                            qty = fn.udfGetDouble(tblDetail.getValueAt(row, tblDetail.getColumnModel().getColumnIndex("Qty")));
//                            return saldo > 0 ? qty : 0;
//                        }
//                    }

                    retVal = fn.udfGetDouble(((JTextField) text).getText());
                } else {
                    retVal = ((JTextField) text).getText();
                }
                return retVal;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                toolkit.beep();
                retVal = 0;
            }
            return retVal;
        }

    }

    private Date getDueDate(Date d, int i) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add(Calendar.DAY_OF_MONTH, i);

        return c.getTime();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmPenjualan().setVisible(true);
            }
        });
    }

}
