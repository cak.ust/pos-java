/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * FrmUserManagement.java
 *
 * Created on Oct 4, 2010, 11:33:23 AM
 */

package pos;

import com.ustasoft.component.FrmSettingMenu;
import com.ustasoft.component.GeneralFunction;
import com.ustasoft.config.DlgMenuGroup;
import com.ustasoft.config.KonfigurasiSistemDao;
import com.ustasoft.config.SettingMenu;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author ustadho
 */
public class FrmUserManagement extends javax.swing.JInternalFrame {
    private Connection conn;
    GeneralFunction fn;
    private String oldUsername="";
    KonfigurasiSistemDao konfigurasiSistemDao = new KonfigurasiSistemDao();
    List<SettingMenu> oldList = new ArrayList<>();
    private MainForm mainForm;
    BCryptPasswordEncoder paswordEncoder = new BCryptPasswordEncoder();
    /** Creates new form FrmUserManagement */
    public FrmUserManagement() {
        initComponents();

        tblUser.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                btnDelete.setEnabled(tblUser.getSelectedRow()>=0);
                btnUpdate.setEnabled(tblUser.getSelectedRow()>=0);

                if(conn==null || tblUser.getSelectedRow()<0) return;
                TableColumnModel col=tblUser.getColumnModel();

                try{
                    String s="select u.user_id, u.user_name, coalesce(u.complete_name,'') as complete_name, " +
                            "photo, ttd_electronic, coalesce(profile, 2) as profile " +
                            "from m_user u " +
                            "where user_name ='"+tblUser.getValueAt(tblUser.getSelectedRow(), col.getColumnIndex("Username")).toString()+"' ";

                    ResultSet rs=conn.createStatement().executeQuery(s);
                    if(rs.next()){
                        oldUsername = rs.getString("user_name");
                        txtUserID.setEnabled(false);
                        txtUserID.setText(rs.getString("user_id"));
                        txtUsername.setText(rs.getString("user_name"));
                        oldUsername = rs.getString("user_name");
                        txtCompleteName.setText(rs.getString("complete_name"));
                        cmbUserProfile.setSelectedIndex(rs.getInt("profile"));
                        byte[] imgBytes = rs.getBytes("photo");
                        byte[] imgSign = rs.getBytes("ttd_electronic");

                        if(imgBytes!=null){
                            javax.swing.ImageIcon myIcon = new javax.swing.ImageIcon(imgBytes);
                            javax.swing.ImageIcon bigImage = new javax.swing.ImageIcon(myIcon.getImage().getScaledInstance
                                               (lblPhoto.getWidth(), lblPhoto.getHeight(), Image.SCALE_REPLICATE));

                            lblPhoto.setIcon(bigImage);
                            imgBytes=null;
                        }else{
                            lblPhoto.setIcon(null);
                        }
                        if(imgSign!=null){
                            javax.swing.ImageIcon myIcon = new javax.swing.ImageIcon(imgSign);
                            javax.swing.ImageIcon bigImage = new javax.swing.ImageIcon(myIcon.getImage().getScaledInstance
                                               (lblSign.getWidth(), lblSign.getHeight(), Image.SCALE_REPLICATE));

                            lblSign.setIcon(bigImage);
                            imgSign=null;
                        }else{
                            lblSign.setIcon(null);
                        }
                        
                        udfLoadMenuUserAuth();
                    }
                    rs.close();
                }catch(SQLException se){
                    JOptionPane.showMessageDialog(FrmUserManagement.this, se.getMessage());
                }

            }
        });
        tblMenu.getModel().addTableModelListener(new MyTableModelListener(tblMenu));
    }

    public void setConn(Connection con){
        this.conn=con;
        fn=GeneralFunction.getInstance();
    }

    private void udfDelete(){
        int iRow=tblUser.getSelectedRow();
        if(iRow<0) return;
        if(JOptionPane.showConfirmDialog(this, "Anda yakin untuk menghapus user ini?", "Delete user", JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
            try{
                conn.setAutoCommit(false);
                conn.createStatement().executeUpdate("delete from m_user  " +
                            "where user_name ='"+tblUser.getValueAt(tblUser.getSelectedRow(), tblUser.getColumnModel().getColumnIndex("Username")).toString()+"'");
                conn.setAutoCommit(true);
                ((DefaultTableModel)tblUser.getModel()).removeRow(iRow);
            }catch(SQLException se){
                try {
                    conn.setAutoCommit(true);
                    conn.rollback();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(this, se.getMessage());
                }
                JOptionPane.showMessageDialog(this, se.getMessage());
            }
        }
    }

    private void udfFilter(String sKode){
        ((DefaultTableModel)tblUser.getModel()).setNumRows(0);
        int iRow=0;
        try{
            ResultSet rs=conn.createStatement().executeQuery("select user_id, user_name, coalesce(complete_name,'') as complete_name "
                    + " from m_user " +
                    "where user_id||user_name||coalesce(complete_name,'') ilike '%"+txtSearch.getText()+"%'" +
                    "order by user_name ");
            while(rs.next()){
                ((DefaultTableModel)tblUser.getModel()).addRow(new Object[]{
                    rs.getString("user_id"),
                    rs.getString("user_name"),
                    rs.getString("complete_name"),
                });
                if(sKode.equalsIgnoreCase(rs.getString("user_id")))
                   iRow=tblUser.getRowCount()-1; 
            }
            if(tblUser.getRowCount()>0)
                tblUser.setRowSelectionInterval(iRow, iRow);
            
            rs.close();
        }catch(SQLException se){
            JOptionPane.showMessageDialog(this, se.getMessage());
        }
    }
    
    private void udfInitSettingMenu() {
        try {

            ResultSet rs = MainForm.conn.createStatement().executeQuery("select group_id from m_menu_group order by 1");
            cmbGroup1.removeAllItems();
            cmbGroup1.addItem("< Semua >");
            while (rs.next()) {
                cmbGroup1.addItem(rs.getString(1));
            }
            rs.close();
        } catch (SQLException se) {
            JOptionPane.showMessageDialog(this, se.getMessage());
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTitledPanel1 = new org.jdesktop.swingx.JXTitledPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUser = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnDelete1 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelUserDetail = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        txtUserID = new javax.swing.JTextField();
        lblPhoto = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtPasswordAgain = new javax.swing.JPasswordField();
        txtPassword = new javax.swing.JPasswordField();
        lblSign = new javax.swing.JLabel();
        cmbUserProfile = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        txtCompleteName = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblMenu = new javax.swing.JTable();
        jLabel24 = new javax.swing.JLabel();
        cmbGroup1 = new javax.swing.JComboBox();
        jXHyperlink1 = new org.jdesktop.swingx.JXHyperlink();
        jSeparator2 = new javax.swing.JSeparator();
        chkAll1 = new javax.swing.JCheckBox();

        setClosable(true);
        setIconifiable(true);
        setTitle("Pengguna");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jXTitledPanel1.setTitle("Daftar Pengguna");
        jXTitledPanel1.getContentContainer().setLayout(new javax.swing.BoxLayout(jXTitledPanel1.getContentContainer(), javax.swing.BoxLayout.LINE_AXIS));

        tblUser.setAutoCreateRowSorter(true);
        tblUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "User ID", "Username", "Complete Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblUser.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tblUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblUserKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblUser);

        jXTitledPanel1.getContentContainer().add(jScrollPane1);

        getContentPane().add(jXTitledPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 240, 370));

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/delete.png"))); // NOI18N
        btnDelete.setText("Hapus");
        btnDelete.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        getContentPane().add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 430, 100, 30));

        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        btnClose.setText("Tutup");
        btnClose.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        getContentPane().add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 430, 90, 30));

        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/disk.png"))); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        getContentPane().add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 430, 100, 30));

        jLabel3.setText("Cari :");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, 60, 20));

        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });
        getContentPane().add(txtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 390, 160, -1));

        btnDelete1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/add.png"))); // NOI18N
        btnDelete1.setText("Baru");
        btnDelete1.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelete1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnDelete1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 430, 100, 30));

        panelUserDetail.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelUserDetail.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Nama Pengguna :");
        panelUserDetail.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 55, 110, 20));

        jLabel2.setText("ID Pengguna:");
        panelUserDetail.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 110, 20));

        txtUsername.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUserDetail.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 55, 190, 22));

        txtUserID.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUserDetail.add(txtUserID, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 90, 22));

        lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPhoto.setText("Foto"); // NOI18N
        lblPhoto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblPhoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoMouseClicked(evt);
            }
        });
        panelUserDetail.add(lblPhoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 180, 130, 180));

        jLabel7.setText("Password (ulang) :");
        panelUserDetail.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 155, 120, 20));

        jLabel10.setText("Profile :");
        panelUserDetail.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 105, 90, 20));

        jLabel11.setText("Password :");
        panelUserDetail.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 120, 20));

        txtPasswordAgain.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUserDetail.add(txtPasswordAgain, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 155, 190, 22));

        txtPassword.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUserDetail.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 190, 22));

        lblSign.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSign.setText("Tanda Tangan"); // NOI18N
        lblSign.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblSign.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSignMouseClicked(evt);
            }
        });
        panelUserDetail.add(lblSign, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 190, 80));

        cmbUserProfile.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrator", "Supervisor", "User" }));
        panelUserDetail.add(cmbUserProfile, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 105, 130, -1));

        jLabel4.setText("Nama Lengkap :");
        panelUserDetail.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 110, 20));

        txtCompleteName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelUserDetail.add(txtCompleteName, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 190, 22));

        jTabbedPane1.addTab("User Detail", panelUserDetail);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblMenu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Menu", "All", "Read", "Insert", "Update", "Delete", "Print", "Correction", "ID"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Boolean.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMenu.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tblMenu.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(tblMenu);

        jPanel1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 690, 300));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Daftar Menu");
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 210, 20));

        cmbGroup1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbGroup1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGroup1ItemStateChanged(evt);
            }
        });
        cmbGroup1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGroup1ActionPerformed(evt);
            }
        });
        jPanel1.add(cmbGroup1, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 5, 230, -1));

        jXHyperlink1.setText("Kelompok Menu");
        jXHyperlink1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXHyperlink1ActionPerformed(evt);
            }
        });
        jPanel1.add(jXHyperlink1, new org.netbeans.lib.awtextra.AbsoluteConstraints(186, 10, 110, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 350, 240, 10));

        chkAll1.setText("Check All");
        chkAll1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAll1ActionPerformed(evt);
            }
        });
        jPanel1.add(chkAll1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 340, 120, -1));

        jTabbedPane1.addTab("Otorisasi Menu", jPanel1);

        getContentPane().add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 10, 700, 400));

        setBounds(0, 0, 979, 497);
    }// </editor-fold>//GEN-END:initComponents

    private void lblPhotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoMouseClicked
        if(evt.getButton()==MouseEvent.BUTTON1){
            PilihFoto("foto");
        }
}//GEN-LAST:event_lblPhotoMouseClicked

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        udfFilter("");
        udfInitSettingMenu();
    }//GEN-LAST:event_formInternalFrameOpened

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        udfSave();
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void lblSignMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSignMouseClicked
        if(evt.getButton()==MouseEvent.BUTTON1){
            PilihFoto("sign");
        }
    }//GEN-LAST:event_lblSignMouseClicked

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        udfFilter("");
    }//GEN-LAST:event_txtSearchKeyReleased

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        udfDelete();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void tblUserKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblUserKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_DELETE)
            udfDelete();
        
    }//GEN-LAST:event_tblUserKeyPressed

    private void btnDelete1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelete1ActionPerformed
        udfNew();
        txtUsername.requestFocus();
    }//GEN-LAST:event_btnDelete1ActionPerformed

    private void cmbGroup1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGroup1ItemStateChanged
        if(cmbGroup1.getSelectedIndex()>=0)
        udfLoadMenuUserAuth();
    }//GEN-LAST:event_cmbGroup1ItemStateChanged

    private void cmbGroup1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGroup1ActionPerformed

    }//GEN-LAST:event_cmbGroup1ActionPerformed

    private void jXHyperlink1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXHyperlink1ActionPerformed
        DlgMenuGroup d1 =new DlgMenuGroup(JOptionPane.getFrameForComponent(this), true);
        d1.setLocationRelativeTo(this);
        d1.setVisible(true);
    }//GEN-LAST:event_jXHyperlink1ActionPerformed

    private void chkAll1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAll1ActionPerformed
        for(int i=0; i<tblMenu.getRowCount(); i++){
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 1);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 2);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 3);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 4);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 5);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 6);
            ((DefaultTableModel)tblMenu.getModel()).setValueAt(chkAll1.isSelected(), i, 7);
        }
    }//GEN-LAST:event_chkAll1ActionPerformed

    private void udfLoadMenuUserAuth() {
        chkAll1.setSelected(false);
        ((DefaultTableModel) tblMenu.getModel()).setNumRows(0);
        oldList = konfigurasiSistemDao.getSettingMenu(
                (cmbGroup1.getSelectedIndex()==0?  "" : cmbGroup1.getSelectedItem().toString()),
                ((DefaultTableModel)tblUser.getModel()).getValueAt(tblUser.getSelectedRow(), tblUser.getColumnModel().getColumnIndex("Username")).toString());

        oldList.forEach((sm) -> {
            ((DefaultTableModel) tblMenu.getModel()).addRow(new Object[]{
                sm.getMenuDescription(),
                sm.isCanRead() && sm.isCanInsert() && sm.isCanUpdate() && sm.isCanDelete() && sm.isCanPrint(),
                sm.isCanRead(),
                sm.isCanInsert(),
                sm.isCanUpdate(),
                sm.isCanDelete(),
                sm.isCanPrint(),
                sm.isCanCorrection(),
                sm.getMenuId()
            });
        });
            if (tblMenu.getRowCount() > 0) {
                tblMenu.setRowSelectionInterval(0, 0);
            }

            tblMenu.setModel((DefaultTableModel) fn.autoResizeColWidth(tblMenu, (DefaultTableModel) tblMenu.getModel()).getModel());
    }
    
    private void PilihFoto(String sFrom) {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        FileInputStream in = null;
        try {
            m_chooser.setDialogTitle(sFrom.equalsIgnoreCase("foto")? "Pilih foto": "Tanda tangan");
            m_chooser.setFileFilter(fFilter);
            if (m_chooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
                return;
            }
            File fChoosen = m_chooser.getSelectedFile();
            in = new FileInputStream(fChoosen);
            javax.swing.ImageIcon myIcon = new javax.swing.ImageIcon(m_chooser.getSelectedFile().toString());
            javax.swing.ImageIcon bigImage ;

            if(sFrom.equalsIgnoreCase("foto")){
                bigImage = new javax.swing.ImageIcon(myIcon.getImage().getScaledInstance
                               (lblPhoto.getWidth(), lblPhoto.getHeight(), Image.SCALE_REPLICATE));
                sFotoFile=m_chooser.getSelectedFile().toString();
                fisFoto = in;
                lblPhoto.setIcon(bigImage);
            }else if(sFrom.equalsIgnoreCase("sign")){
                bigImage = new javax.swing.ImageIcon(myIcon.getImage().getScaledInstance
                               (lblSign.getWidth(), lblSign.getHeight(), Image.SCALE_REPLICATE));

                sSignFile=m_chooser.getSelectedFile().toString();
                fisSginature = in;
                lblSign.setIcon(bigImage);
            }
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            in.close();

        } catch (IOException ex) {
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(this, ex.getMessage());
        } finally {
            try {
                this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                if (in != null) in.close();
            } catch (IOException ex) {
                Logger.getLogger(FrmUserManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private FileFilter fFilter=new FileFilter() {

        @Override
        public boolean accept(File f) {
            return f.getName ().toLowerCase ().endsWith (".jpg")
          || f.isDirectory ();
        }

        @Override
        public String getDescription() {
            return "Image files (*.jpg)";
        }
    };


    private void udfNew(){
        txtUserID.setText("");
        txtUsername.setText("");
        txtCompleteName.setText("");
        cmbUserProfile.setSelectedIndex(2);
        txtPassword.setText("");
        txtPasswordAgain.setText("");
        btnUpdate.setText("Simpan");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDelete1;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JCheckBox chkAll;
    private javax.swing.JCheckBox chkAll1;
    private javax.swing.JComboBox cmbGroup;
    private javax.swing.JComboBox cmbGroup1;
    private javax.swing.JComboBox cmbUserProfile;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink1;
    private org.jdesktop.swingx.JXTitledPanel jXTitledPanel1;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JLabel lblSign;
    private javax.swing.JPanel panelAuthMenu;
    private javax.swing.JPanel panelUserDetail;
    private javax.swing.JTable tblMenu;
    private javax.swing.JTable tblUser;
    private javax.swing.JTextField txtCompleteName;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JPasswordField txtPasswordAgain;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtUserID;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables

    private JFileChooser m_chooser = new JFileChooser();
    private String sFotoFile="", sSignFile="";
    FileInputStream fisFoto, fisSginature;

    private void udfSave()  {
        boolean isNew=false;
        {
            FileInputStream fis = null;
            try {
                conn.setAutoCommit(false);
                String pass = "";
                char[] chrPass = txtPassword.getPassword();
                for (int i1 = 0; i1 < chrPass.length; i1++) {
                    pass = pass + chrPass[i1];
                    chrPass[i1] = '0';
                }
                ResultSet rs = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE).executeQuery("select * from m_user where user_id='" + txtUserID.getText() + "'");
                if (!rs.next()) {
                    isNew = true;
                    rs.moveToInsertRow();
                    if(txtUserID.getText().equalsIgnoreCase("")){
                        ResultSet rs1=conn.createStatement().executeQuery("select fn_get_user_id()");
                        if(rs1.next())
                            txtUserID.setText(rs1.getString(1));
                        rs1.close();
                    }
                }
                rs.updateString("user_id", txtUserID.getText());
                rs.updateString("user_name", txtUsername.getText());
                rs.updateString("complete_name", txtCompleteName.getText());
                rs.updateString("pwd", paswordEncoder.encode(pass));
                rs.updateInt("profile", cmbUserProfile.getSelectedIndex());
                
                if (sFotoFile.length() > 0) {
                    File file;
                    file = new File(sFotoFile);
                    fis = new FileInputStream(file);
                    rs.updateBinaryStream("photo", fis, (int) file.length());
                }
                if (sSignFile.length() > 0) {
                    File file;
                    file = new File(sSignFile);
                    fis = new FileInputStream(file);
                    rs.updateBinaryStream("ttd_electronic", fis, (int) file.length());
                }
                if (isNew) {
                    rs.insertRow();
                } else {
                    rs.updateRow();
                    
                }
                
//                if(pass.length() >0)
//                    i = conn.createStatement().executeUpdate("update m_user set pwd=md5('" + pass + "') " + "where user_id='" + txtUserID.getText() + "'; ");

                String sIns ="";
                for (int a=0; a<tblMenu.getRowCount(); a++){
                    sIns+=  "Delete from m_menu_authorization where menu_id="+tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("ID")).toString()+" " +
                            "and user_name='"+oldUsername+"' ; " +
                            "Insert into m_menu_authorization(menu_id, user_name, can_insert, can_update," +
                            "can_delete, can_read, can_print, can_correction) select " +
                            "'"+tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("ID")).toString()+"'," +
                            "'"+txtUsername.getText()+"', " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Insert")) +", " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Update")) +", " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Delete")) +", " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Read")) +", " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Print")) +", " +
                            (Boolean)tblMenu.getValueAt(a, tblMenu.getColumnModel().getColumnIndex("Correction")) +" ;";
                }
                System.out.println(sIns);

                int iR=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE).executeUpdate(sIns);
                conn.setAutoCommit(true);

                if (iR>0) JOptionPane.showMessageDialog(this, "Update Successful!");

                mainForm.udfSetUserMenu();

                conn.setAutoCommit(true);
                JOptionPane.showMessageDialog(this, "Insert/ Update user sukses!");
                udfFilter(txtUserID.getText());
                if(oldUsername.equalsIgnoreCase(MainForm.sUserName)){
                    MainForm.sUserName = txtUsername.getText();
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FrmUserManagement.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException se) {
                try {
                    conn.rollback();
                    conn.setAutoCommit(true);
                    JOptionPane.showMessageDialog(this, se.getMessage());
                } catch (SQLException ex) {
                    Logger.getLogger(FrmUserManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    if(fis!=null)   fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(FrmUserManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    void setMainForm(MainForm aThis) {
        this.mainForm = aThis;
    }
    
    public class MyTableModelListener implements TableModelListener {
        JTable table;

        // It is necessary to keep the table since it is not possible
        // to determine the table from the event's source
        MyTableModelListener(JTable table) {
            this.table = table;
        }

        public void tableChanged(TableModelEvent e) {
            int firstRow = e.getFirstRow();
            int lastRow = e.getLastRow();
            int mColIndex = e.getColumn();

//            if(e.getType()==TableModelEvent.UPDATE || e.getType()==TableModelEvent.INSERT ) {
//                if (firstRow == TableModelEvent.HEADER_ROW) {
//                    if (mColIndex == TableModelEvent.ALL_COLUMNS) {
//                        // A column was added
//                    } else {
//                    }
//                } else {
//                    // The rows in the range [firstRow, lastRow] changed
//                }
//            }

            if(e.getType()==TableModelEvent.UPDATE){
                table.getModel().removeTableModelListener(this);

                int iRow =e.getLastRow();//  jTable1.getSelectedRow();
                if(iRow<0) return;
                if(table.getSelectedColumn()==1){
                        for (int i=2; i<table.getColumnCount()-1; i++){
                            ((DefaultTableModel)table.getModel()).setValueAt(
                                    (Boolean)((DefaultTableModel)table.getModel()).getValueAt(iRow, table.getSelectedColumn()),
                                    iRow, i);
                        }
                    //}
                }else{
                    if(iRow>=0 && table.getSelectedColumn()>1 && ((DefaultTableModel)table.getModel()).getValueAt(iRow, table.getSelectedColumn())!=null &&
                            (Boolean)((DefaultTableModel)table.getModel()).getValueAt(iRow, table.getSelectedColumn())==false){
                        ((DefaultTableModel)table.getModel()).setValueAt(false, iRow, 1);
                    }
                }

                table.getModel().addTableModelListener(this);

            }
        }
    }
}
