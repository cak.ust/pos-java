/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos.inventory;

import com.ustasoft.component.GeneralFunction;
import com.ustasoft.component.Konfigurasi;
import com.ustasoft.pos.dao.jdbc.GudangDao;
import com.ustasoft.pos.dao.jdbc.RelasiDao;
import com.ustasoft.pos.domain.Gudang;
import com.ustasoft.pos.domain.Relasi;
import com.ustasoft.pos.service.ReportService;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pos.MainForm;

/**
 *
 * @author cak-ust
 */
public class FrmRptInventory extends javax.swing.JInternalFrame {
    private Connection conn;
    ReportService service;
    List<Gudang> gudang=new ArrayList<Gudang>();
    List<Relasi> suppliers=new ArrayList<Relasi>();
    private GeneralFunction fn= GeneralFunction.getInstance();
    final static int RPT_ITEM_SUPPLIER = 10;
    final static int RPT_STOK_KOSONG = 11;
    /**
     * Creates new form FrmReportInventory
     */
    public FrmRptInventory() {
        initComponents();
        jDateAwal.setFormats(Konfigurasi.FORMAT_TANGGAL);
        jDateAkhir.setFormats(Konfigurasi.FORMAT_TANGGAL);
    }

    public void setConn(Connection con){
        this.conn=con;
        service=new ReportService(conn, this);
    }
    
    private void initForm(){
        try {
            gudang=new GudangDao(conn).cariSemuaData();
            cmbGudang.removeAllItems();
            cmbGudang.addItem("Semua Gudang");
            for(int i=0; i<gudang.size(); i++){
                cmbGudang.addItem(gudang.get(i).getNama_gudang());
            }
            suppliers=new RelasiDao(conn).cariSemuaSupplier();
            cmbSupplier.removeAllItems();
            for(int i=0; i<suppliers.size(); i++){
                cmbSupplier.addItem(suppliers.get(i).getNamaRelasi());
            }
            Timestamp skg=fn.getTimeServer();
            jDateAwal.setDate(skg);
            jDateAkhir.setDate(skg);
            jList1.setSelectedIndex(0);
            
        } catch (Exception ex) {
            Logger.getLogger(FrmRptInventory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    SimpleDateFormat fmt=new SimpleDateFormat("yyyy-MM-dd");
    
    private void udfPreview(){
        int idx=jList1.getSelectedIndex();
//        if(idx==2||idx==3||idx==4){
//            if(txtItem1.getText().length()==0){
//                JOptionPane.showMessageDialog(this, "Silahkan pilih item ke 2 terlebih dulu!");
//                txtItem1.requestFocusInWindow();
//                return;
//            }
//        }
        
        HashMap param=new HashMap();
        param.put("toko", MainForm.sToko);
        param.put("alamat1", MainForm.sAlamat1);
        param.put("alamat2", MainForm.sAlamat2);
        param.put("telp", MainForm.sTelp1);
        param.put("email", MainForm.sEmail);
        param.put("tanggal1", fmt.format(jDateAwal.getDate()));
        param.put("tanggal", fmt.format(jDateAwal.getDate()));
        param.put("tanggal2", fmt.format(jDateAkhir.getDate()));
        param.put("idBarang", lblIDItem.getText());
        param.put("kodeBarang1", lblIDItem.getText());
        param.put("kodeBarang2", lblIDItem.getText());
        param.put("IS_IGNORE_PAGINATION", chkIgnorePagination.isSelected());
//        param.put("kodeBarang2", txtItemPLU.getText());
        param.put("idGudang", cmbGudang.getSelectedIndex()==0? "": gudang.get(cmbGudang.getSelectedIndex()-1).getId().toString());
        

        switch(jList1.getSelectedIndex()){
            case 0:{
                service.udfPreview(param, "KartuStokFormula", null);
                break;
            }
            case 1:{
                service.udfPreview(param, "KartuStokPriceFormula", null);
                break;
            }
            case 2:{
                param.put("tanggal", fmt.format(jDateAkhir.getDate()));
                service.udfPreview(param, "StokSaldo", null);
                break;
            }
            case 3:{
                service.udfPreview(param, "StokMutasi", null);
                break;
            }
            case 4:{
                service.udfPreview(param, "StokMutasiPerGudang", null);
                break;
            }
            case 5:{
                param.put("inOut", "IN");
                service.udfPreview(param, "StokMasukKeluar", null);
                break;
            }
            case 6:{
                param.put("inOut", "OUT");
                service.udfPreview(param, "StokMasukKeluar", null);
                break;
            }
            case 7:{
                service.udfPreview(param, "StokTransfer", null);
                break;
            }
            case 8:{
                service.udfPreview(param, "StockMonthly", null);
                break;
            }
            case 9:{
                String query="select i.id, i.plu kode, i.nama_barang, coalesce(k.nama_kategori,'') kategori, i.stok, coalesce(s.harga,0) harga, coalesce(s.disc,'') disc \n" +
                            "from m_item i \n" +
                            "left join m_item_supplier s on s.id_barang=i.id\n" +
                            "left join m_item_kategori k on k.id=i.id_kategori\n" +
                            "where id_supp_default ="+suppliers.get(cmbSupplier.getSelectedIndex()).getIdRelasi()+"\n" +
                            "order by i.nama_barang";
                service.udfExportToExcel(query, "ItemSupplier-"+cmbSupplier.getSelectedItem().toString(), this);
                break;
            }
            case 10:{
                String query="select i.id, i.plu kode, i.nama_barang, coalesce(k.nama_kategori,'') kategori, i.stok, coalesce(s.harga,0) harga, coalesce(s.disc,'') disc \n" +
                            "from m_item i \n" +
                            "left join m_item_supplier s on s.id_barang=i.id\n" +
                            "left join m_item_kategori k on k.id=i.id_kategori\n" +
                            "where id_supp_default ="+suppliers.get(cmbSupplier.getSelectedIndex()).getIdRelasi()+"\n" +
                            "order by i.nama_barang";
                service.udfExportToExcel(query, "ItemSupplier-"+cmbSupplier.getSelectedItem().toString(), this);
                break;
            }
            case RPT_STOK_KOSONG: {
                service.udfPreview(param, "StokKosong", null);
                break;
            }
            default:{
                break;
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbGudang = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txtItemPLU = new javax.swing.JTextField();
        lblIDItem = new javax.swing.JLabel();
        lblNamaBarang = new javax.swing.JLabel();
        cmbSupplier = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jDateAkhir = new org.jdesktop.swingx.JXDatePicker();
        jDateAwal = new org.jdesktop.swingx.JXDatePicker();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        chkIgnorePagination = new javax.swing.JCheckBox();

        setClosable(true);
        setTitle("Laporan Persediaan");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "1.   Kartu Stok", "2.   Kartu Stok - Dengan Harga Transaksi", "3.   Daftar Saldo Stok", "4.   Mutasi Stok", "5.   Mutasi Stok Per Gudang", "6.   Penerimaan Stok", "7.   Pengeluaran Stok", "8.   Pindah Gudang", "9.   Stok Bulanan", "10. Item Point", "11. Item Supplier", "12. Stok Kosong" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 269, 260));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Parameter"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Gudang :");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 70, 20));

        jLabel2.setText("Awal :");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 70, 20));

        cmbGudang.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TUNAI", "KREDIT" }));
        jPanel2.add(cmbGudang, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 130, -1));

        jLabel3.setText("Sampai :");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 45, 70, 20));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Item"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtItemPLU.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtItemPLU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtItemPLUKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtItemPLUKeyReleased(evt);
            }
        });
        jPanel3.add(txtItemPLU, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 50, 20));

        lblIDItem.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.add(lblIDItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 20, 40, 20));

        lblNamaBarang.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.add(lblNamaBarang, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 20, 260, 20));

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 370, 50));

        cmbSupplier.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TUNAI", "KREDIT" }));
        jPanel2.add(cmbSupplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 95, 280, -1));

        jLabel4.setText("Supplier : ");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 95, 70, 20));
        jPanel2.add(jDateAkhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 45, 170, -1));
        jPanel2.add(jDateAwal, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 170, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 390, 220));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/printer.png"))); // NOI18N
        jButton1.setText("Tampilkan");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 235, 120, 30));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        jButton2.setText("Tutup");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 235, 90, 30));

        chkIgnorePagination.setText("Abaikan halaman");
        jPanel1.add(chkIgnorePagination, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 160, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(285, 11, 410, 270));

        setBounds(0, 0, 704, 312);
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        initForm();
    }//GEN-LAST:event_formInternalFrameOpened

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        udfPreview();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtItemPLUKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtItemPLUKeyReleased
        fn.lookup(evt, new Object[]{lblNamaBarang, lblIDItem}, "select coalesce(plu,'') as plu, "
                + "coalesce(nama_barang,'') as nama_barang, id::varchar as id "
                + "from m_item i "
                + "where id::varchar||coalesce(plu,'')||coalesce(nama_barang,'') ilike '%"+txtItemPLU.getText()+"%' "
                + "order by coalesce(plu,'')", 
                txtItemPLU.getWidth()+lblIDItem.getWidth()+lblNamaBarang.getWidth()+18, 150);
    }//GEN-LAST:event_txtItemPLUKeyReleased

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        int idx=jList1.getSelectedIndex();
        jDateAkhir.setEnabled(idx!=RPT_STOK_KOSONG);
        cmbGudang.setEnabled(idx!=RPT_STOK_KOSONG && (idx==0||idx==1||idx==2||idx==3||idx==4||idx==5||idx==6));
        jPanel3.setVisible(idx!=RPT_STOK_KOSONG && (idx==0||idx==1||idx==3||idx==4));
//        txtItem1.setEnabled(idx==0||idx==1||idx==2||idx==3);
        jDateAwal.setEnabled(idx!=RPT_STOK_KOSONG && !(idx==2));
        jLabel4.setVisible(idx!=RPT_STOK_KOSONG &&  idx==RPT_ITEM_SUPPLIER); // Supplier
        cmbSupplier.setVisible(idx!=RPT_STOK_KOSONG && idx==RPT_ITEM_SUPPLIER);
    }//GEN-LAST:event_jList1ValueChanged

    private void txtItemPLUKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtItemPLUKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtItemPLUKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkIgnorePagination;
    private javax.swing.JComboBox cmbGudang;
    private javax.swing.JComboBox cmbSupplier;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private org.jdesktop.swingx.JXDatePicker jDateAkhir;
    private org.jdesktop.swingx.JXDatePicker jDateAwal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIDItem;
    private javax.swing.JLabel lblNamaBarang;
    private javax.swing.JTextField txtItemPLU;
    // End of variables declaration//GEN-END:variables
}
