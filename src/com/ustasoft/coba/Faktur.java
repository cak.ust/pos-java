/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.coba;

/**
 *
 * @author cak-ust
 */
import java.util.ArrayList;
import java.util.List;
 
public class Faktur {
 
    private String nomorFaktur;
    private List<ItemFaktur> listItemFaktur = new ArrayList<ItemFaktur>();
 
    public Faktur(String nomorFaktur) {
        this.nomorFaktur = nomorFaktur;
    }
 
    public String getNomorFaktur() {
        return nomorFaktur;
    }
 
    public List<ItemFaktur> getListItemFaktur() {
        return listItemFaktur;
    }
 
    public void tambahItemFaktur(ItemFaktur itemFaktur) {
        this.listItemFaktur.add(itemFaktur);
    }
}
