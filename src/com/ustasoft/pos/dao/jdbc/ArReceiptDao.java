/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.dao.jdbc;

import com.ustasoft.component.GeneralFunction;
import com.ustasoft.pos.domain.ArReceipt;
import com.ustasoft.pos.domain.ArReceiptDet;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pos.MainForm;

/**
 *
 * @author ustadho
 */
public class ArReceiptDao {
    public String simpan(ArReceipt r) {
        try {
            ResultSet rs = MainForm.conn.createStatement().executeQuery(
                    "select fn_get_ar_no('" + GeneralFunction.yyyymmdd_format.format(r.getTanggal()) + "')");
            if (rs.next()) {
                r.setNomor(rs.getString(1));
            } else {
                rs.close();
                return "Error fn_get_ar_no('" + GeneralFunction.yyyymmdd_format.format(r.getTanggal()) + "')";
            }
            MainForm.conn.setAutoCommit(false);
            String sQry = "INSERT INTO ar_receipt(ar_no, tanggal, id_customer, alat_bayar, rate, "
                    + "memo, acc_no, check_no, date_ins, "
                    + "user_ins, check_amount, tgl_cek, shift)    VALUES ("
                    + "?, ?, ?, ?, 1, " //4
                    + "?, ?, ?, now()," //7
                    + "?, ?, ?, ?);\n"; //11
            PreparedStatement ps=MainForm.conn.prepareStatement(sQry);
            ps.setString(1, r.getNomor());
            ps.setDate(2, new java.sql.Date(r.getTanggal().getTime()));
            ps.setInt(3, r.getIdCustomer());
            ps.setString(4, r.getKodeAlatBayar());
            ps.setString(5, r.getMemo());
            ps.setString(6, r.getAccNo());
            ps.setString(7, r.getNoBukti());
            ps.setString(8, MainForm.sUserName);
            ps.setDouble(9, r.getTotal());
            ps.setDate(10, r.getTglCek()==null? null: new java.sql.Date(r.getTglCek().getTime()));
            ps.setString(11, MainForm.shift);
            ps.executeUpdate();
            
            sQry="";
            for (ArReceiptDet d: r.getDetails()) {
                if (d.getBayar() > 0) {
                    sQry += "INSERT INTO ar_receipt_detail(ar_no, id_penjualan, "
                            + "bayar, discount, "
                            + "date_ins) VALUES ("
                            + "'" + r.getNomor() + "', "
                            + d.getIdPenjualan() + ", "
                            + d.getBayar() + ", " + d.getDiscount() + ", "
                            + "now());\n";
                }
            }
            MainForm.conn.createStatement().executeUpdate(sQry);
            MainForm.conn.setAutoCommit(true);
        } catch (SQLException ex) {
            Logger.getLogger(ArReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return r.getNomor();
    }

}
