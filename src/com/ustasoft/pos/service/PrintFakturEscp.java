/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

import com.ustasoft.coba.Faktur;
import com.ustasoft.coba.FormCetak;
import com.ustasoft.coba.ItemFaktur;
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import pos.MainForm;
import simple.escp.Template;
import simple.escp.json.JsonTemplate;
import simple.escp.swing.PrintPreviewPane;

/**
 *
 * @author cak-ust
 */
public class PrintFakturEscp extends JFrame{
    public PrintFakturEscp() throws HeadlessException {
         super("Latihan");
 
        FakturHeader faktur = new FakturHeader();
        faktur.setNamaToko(MainForm.sToko);
        faktur.setAlamatToko1(MainForm.sAlamat1);
        faktur.setAlamatToko2(MainForm.sAlamat2);
        faktur.setTelpToko(MainForm.sTelp1);
        faktur.setTglFaktur(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
        faktur.setJamFaktur(new SimpleDateFormat("HH:mm:ss").format(new Date()));
        
        faktur.tambahItem(new FakturDetail("Plantronics Backbeat Go 2 With Charging Case", "10", "PCS", "10000", "100", "9000"));
        faktur.tambahItem(new FakturDetail("CORT Gitar Akustik AD810 - Natural Satin", "10", "PCS", "10000", "100", "9000"));
        faktur.tambahItem(new FakturDetail("SAMSON Monitor Speaker System MediaOne 3A", "10", "PCS", "10000", "100", "9000"));
 
        Template template  = null;
        try {
            template = new JsonTemplate(PrintFakturEscp.class.getResourceAsStream("template-faktur.json"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        PrintPreviewPane preview = new PrintPreviewPane(template, null, faktur);
        setLayout(new BorderLayout());
        add(preview, BorderLayout.CENTER);  
 
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    public static void main(String[] args) {
        new PrintFakturEscp();
    }
}
