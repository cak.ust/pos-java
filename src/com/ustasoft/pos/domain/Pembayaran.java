/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.domain;

import java.awt.Component;

/**
 *
 * @author ustadho
 */
public class Pembayaran {
    private Integer id;
    private String invoiceNo;
    private Double totalItem;
    private Double totalDiskon;
    private Double totalBayar;
    private Component srcComponent;

    public Pembayaran(Integer id, String invoiceNo, Double totalItem, Double totalDiskon, Double totalBayar, Component srcComponent) {
        this.id = id;
        this.invoiceNo = invoiceNo;
        this.totalItem = totalItem;
        this.totalDiskon = totalDiskon;
        this.totalBayar = totalBayar;
        this.srcComponent = srcComponent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Double getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(Double totalItem) {
        this.totalItem = totalItem;
    }

    public Double getTotalDiskon() {
        return totalDiskon;
    }

    public void setTotalDiskon(Double totalDiskon) {
        this.totalDiskon = totalDiskon;
    }

    public Double getTotalBayar() {
        return totalBayar;
    }

    public void setTotalBayar(Double totalBayar) {
        this.totalBayar = totalBayar;
    }

    public Component getSrcComponent() {
        return srcComponent;
    }

    public void setSrcComponent(Component srcComponent) {
        this.srcComponent = srcComponent;
    }

    
}
