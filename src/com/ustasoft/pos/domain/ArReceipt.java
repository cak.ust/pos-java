/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ustadho
 */
public class ArReceipt {
    private String nomor;
    private Date tanggal;
    private int idCustomer;
    private String kodeAlatBayar;
    private int rate;
    private String memo;
    private String accNo;
    private String noBukti;
    private Double total;
    private Date tglCek;
    private List<ArReceiptDet> details = new ArrayList<>();

    public ArReceipt(String nomor, Date tanggal, int idCustomer, String kodeAlatBayar, int rate, String memo, String accNo, String noBukti, Double total, Date tglCek, 
            List<ArReceiptDet> det) {
        this.nomor = nomor;
        this.tanggal = tanggal;
        this.idCustomer = idCustomer;
        this.kodeAlatBayar = kodeAlatBayar;
        this.rate = rate;
        this.memo = memo;
        this.accNo = accNo;
        this.noBukti = noBukti;
        this.total = total;
        this.tglCek = tglCek;
        this.details = det;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getKodeAlatBayar() {
        return kodeAlatBayar;
    }

    public void setKodeAlatBayar(String kodeAlatBayar) {
        this.kodeAlatBayar = kodeAlatBayar;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getNoBukti() {
        return noBukti;
    }

    public void setNoBukti(String noBukti) {
        this.noBukti = noBukti;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getTglCek() {
        return tglCek;
    }

    public void setTglCek(Date tglCek) {
        this.tglCek = tglCek;
    }

    public List<ArReceiptDet> getDetails() {
        return details;
    }

    public void setDetails(List<ArReceiptDet> details) {
        this.details = details;
    }
    
}
