/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.domain;

/**
 *
 * @author ustadho
 */
public class ArReceiptDet {
    private String nomor;
    private int idPenjualan;
    private Double bayar;
    private Double discount;

    public ArReceiptDet(String nomor, int idPenjualan, Double bayar, Double discount) {
        this.nomor = nomor;
        this.idPenjualan = idPenjualan;
        this.bayar = bayar;
        this.discount = discount;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public int getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(int idPenjualan) {
        this.idPenjualan = idPenjualan;
    }

    public Double getBayar() {
        return bayar;
    }

    public void setBayar(Double bayar) {
        this.bayar = bayar;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
    
}
