/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.component;

import com.ustasoft.pos.dao.jdbc.GudangDao;
import com.ustasoft.pos.domain.Gudang;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author cak-ust
 */
public class FrmSettingKoneksi extends javax.swing.JInternalFrame {

    private Connection conn;
    ResultSet rs = null;
    boolean isNew = false;
    private GeneralFunction fn= GeneralFunction.getInstance();
    Properties prop = new Properties();
    GudangDao gudangDao;

    /**
     * Creates new form FrmInfoPerusahaan
     */
    public FrmSettingKoneksi() {
        initComponents();
        MyKeyListener kListener = new MyKeyListener();
        fn.addKeyListenerInContainer(jPanel1, kListener, txtFocusListener);
        btnSave.addKeyListener(kListener);
        btnKeluar.addKeyListener(kListener);
    }

    public void setConn(Connection c) {
        this.conn = c;
        gudangDao = new GudangDao(conn);
    }

    private void udfInitForm() {
        InputStream input = null;

        try {
            input = new FileInputStream("setting.properties");

            // load a properties file
            prop.load(input);
            chkPreview.setSelected(prop.getProperty("tampikan_sebelum_cetak") != null && prop.getProperty("tampikan_sebelum_cetak").equalsIgnoreCase("1"));
            chkDotMatrixKecil.setSelected(prop.getProperty("jenis_printer_kecil") != null && prop.getProperty("jenis_printer_kecil").equalsIgnoreCase("1"));
            chkDotMatrixBesar.setSelected(prop.getProperty("jenis_printer_besar") != null && prop.getProperty("jenis_printer_besar").equalsIgnoreCase("1"));
            chkPreviewBarcode.setSelected(prop.getProperty("printer_barcode_preview") != null && prop.getProperty("printer_barcode_preview").equalsIgnoreCase("1"));
            txtPrinterKecil.setText(prop.getProperty("printer_kecil"));
            txtPrinterBesar.setText(prop.getProperty("printer_besar"));
            txtPrinterBarcode.setText(prop.getProperty("printer_barcode"));
            txtGudang.setText(prop.getProperty("gudang"));
            Gudang g = gudangDao.cariByID(fn.udfGetInt(txtGudang.getText()));
            lblGudang.setText(g.getNama_gudang());
            txtServer.setText(prop.getProperty("server"));
            txtPort.setText(prop.getProperty("port"));
            txtDatabase.setText(prop.getProperty("database"));
            txtMarginKiri.setText(prop.getProperty("margin_kiri"));
            txtMarginBawah.setText(prop.getProperty("margin_bawah"));
            Konfigurasi.TAMPILKAN_FAKTUR_SEBELUM_DICETAK = chkPreview.isSelected();
            Konfigurasi.PRINTER_BARCODE = txtPrinterBarcode.getText();
            Konfigurasi.PRINTER_BARCODE_PREVIEW = chkPreviewBarcode.isSelected();
        } catch (Exception ex) {
            Logger.getLogger(FrmSettingKoneksi.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void udfSave() {
        OutputStream output = null;

        try {

            output = new FileOutputStream("setting.properties");

            // set the properties value
            prop.setProperty("database", txtDatabase.getText());
            prop.setProperty("server", txtServer.getText());
            prop.setProperty("port", txtPort.getText());
            prop.setProperty("gudang", txtGudang.getText());
            prop.setProperty("tampikan_sebelum_cetak", chkPreview.isSelected()? "1": "0");
            prop.setProperty("jenis_printer_kecil", chkDotMatrixKecil.isSelected()? "1": "0");
            prop.setProperty("jenis_printer_besar", chkDotMatrixBesar.isSelected()? "1": "0");
            prop.setProperty("printer_kecil", txtPrinterKecil.getText());
            prop.setProperty("printer_besar", txtPrinterBesar.getText());
            prop.setProperty("printer_barcode", txtPrinterBarcode.getText());
            prop.setProperty("printer_barcode_preview", chkPreviewBarcode.isSelected()? "1": "0");
            prop.setProperty("margin_kiri", txtMarginKiri.getText());
            prop.setProperty("margin_bawah", txtMarginBawah.getText());

            // save properties to project root folder
            prop.store(output, null);
            Konfigurasi.PRINTER_KECIL = txtPrinterKecil.getText();
            Konfigurasi.PRINTER_BESAR = txtPrinterBesar.getText();
            Konfigurasi.PRINTER_BARCODE = txtPrinterBarcode.getText();
            Konfigurasi.PRINTER_BARCODE_PREVIEW = chkPreviewBarcode.isSelected();
            
            JOptionPane.showMessageDialog(this, "Simpan konfigurasi sukses");
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtPrinterBesar = new javax.swing.JTextField();
        lblGudang = new javax.swing.JLabel();
        txtGudang = new javax.swing.JTextField();
        chkPreview = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();
        txtServer = new javax.swing.JTextField();
        txtPrinterKecil = new javax.swing.JTextField();
        txtPort = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtDatabase = new javax.swing.JTextField();
        chkDotMatrixBesar = new javax.swing.JCheckBox();
        chkDotMatrixKecil = new javax.swing.JCheckBox();
        jLabel12 = new javax.swing.JLabel();
        txtMarginBawah = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtMarginKiri = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtPrinterBarcode = new javax.swing.JTextField();
        chkPreviewBarcode = new javax.swing.JCheckBox();
        btnSave = new javax.swing.JButton();
        btnKeluar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Setting Koneksi & Printer");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel2.setText("Printer Besar");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 60, 130, 20);

        jLabel3.setText("Printer Kecil");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 35, 130, 20);

        jLabel4.setText("Server");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 140, 130, 20);

        txtPrinterBesar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtPrinterBesar);
        txtPrinterBesar.setBounds(140, 60, 240, 22);

        lblGudang.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(lblGudang);
        lblGudang.setBounds(200, 110, 260, 22);

        txtGudang.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtGudang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtGudangKeyReleased(evt);
            }
        });
        jPanel1.add(txtGudang);
        txtGudang.setBounds(140, 110, 60, 22);

        chkPreview.setText("Tampilkan nota sebelum dicetak");
        jPanel1.add(chkPreview);
        chkPreview.setBounds(140, 10, 360, 18);

        jLabel9.setText("Default Gudang");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 110, 130, 20);

        txtServer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtServer);
        txtServer.setBounds(140, 140, 130, 22);

        txtPrinterKecil.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtPrinterKecil);
        txtPrinterKecil.setBounds(140, 35, 240, 22);

        txtPort.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtPort);
        txtPort.setBounds(320, 140, 60, 22);

        jLabel10.setText("Port");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(280, 140, 40, 20);

        jLabel11.setText("Database");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(10, 165, 130, 20);

        txtDatabase.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtDatabase);
        txtDatabase.setBounds(140, 165, 320, 22);

        chkDotMatrixBesar.setText("Dot Matrix");
        jPanel1.add(chkDotMatrixBesar);
        chkDotMatrixBesar.setBounds(380, 60, 98, 18);

        chkDotMatrixKecil.setText("Dot Matrix");
        jPanel1.add(chkDotMatrixKecil);
        chkDotMatrixKecil.setBounds(380, 30, 85, 18);

        jLabel12.setText("Bawah");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(260, 190, 60, 20);

        txtMarginBawah.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtMarginBawah);
        txtMarginBawah.setBounds(320, 190, 50, 22);

        jLabel13.setText("Margin Dot Matrix      (Kiri)");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(10, 190, 180, 20);

        txtMarginKiri.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtMarginKiri);
        txtMarginKiri.setBounds(190, 190, 50, 22);

        jLabel5.setText("Printer Barcode");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 85, 130, 20);

        txtPrinterBarcode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtPrinterBarcode);
        txtPrinterBarcode.setBounds(140, 85, 240, 22);

        chkPreviewBarcode.setText("Preview");
        jPanel1.add(chkPreviewBarcode);
        chkPreviewBarcode.setBounds(380, 85, 98, 18);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 70, 490, 240);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/printer.png"))); // NOI18N
        btnSave.setText("Simpan");
        btnSave.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        getContentPane().add(btnSave);
        btnSave.setBounds(290, 310, 100, 32);

        btnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        btnKeluar.setText("Keluar");
        btnKeluar.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKeluarActionPerformed(evt);
            }
        });
        getContentPane().add(btnKeluar);
        btnKeluar.setBounds(390, 310, 110, 32);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Setting Koneksi & Printer");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(10, 20, 390, 29);

        setBounds(0, 0, 520, 378);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        udfSave();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKeluarActionPerformed
        try {
            if (rs != null) {
                rs.close();
            }
            this.dispose();
        } catch (SQLException ex) {
            Logger.getLogger(FrmSettingKoneksi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnKeluarActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        udfInitForm();
    }//GEN-LAST:event_formInternalFrameOpened

    private void txtGudangKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGudangKeyReleased
        fn.lookup(evt, new Object[]{lblGudang}, "select id, coalesce(nama_gudang,'') as \"Nama Gudang\" "
                + "from m_gudang where "
                + "id||coalesce(nama_gudang,'') ilike '%" + txtGudang.getText() + "%' "
                + "order by nama_gudang", txtGudang.getWidth() + lblGudang.getWidth() + 20, 150);
    }//GEN-LAST:event_txtGudangKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnKeluar;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkDotMatrixBesar;
    private javax.swing.JCheckBox chkDotMatrixKecil;
    private javax.swing.JCheckBox chkPreview;
    private javax.swing.JCheckBox chkPreviewBarcode;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblGudang;
    private javax.swing.JTextField txtDatabase;
    private javax.swing.JTextField txtGudang;
    private javax.swing.JTextField txtMarginBawah;
    private javax.swing.JTextField txtMarginKiri;
    private javax.swing.JTextField txtPort;
    private javax.swing.JTextField txtPrinterBarcode;
    private javax.swing.JTextField txtPrinterBesar;
    private javax.swing.JTextField txtPrinterKecil;
    private javax.swing.JTextField txtServer;
    // End of variables declaration//GEN-END:variables

    public class MyKeyListener extends KeyAdapter {

        @Override
        public void keyTyped(java.awt.event.KeyEvent evt) {

//            if(getTableSource()==null)
//                return;
            if (evt.getSource() instanceof JTextField
                    && ((JTextField) evt.getSource()).getName() != null
                    && ((JTextField) evt.getSource()).getName().equalsIgnoreCase("textEditor")) {
                fn.keyTyped(evt);

            }

        }

        public void keyPressed(KeyEvent evt) {
            Component ct = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            int keyKode = evt.getKeyCode();
            switch (keyKode) {
                case KeyEvent.VK_ENTER: {
                    if (!(ct instanceof JTable)) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (!(ct.getClass().getSimpleName().equalsIgnoreCase("JTABLE"))) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                        break;
                    }
                }

                case KeyEvent.VK_UP: {
                    if (!(evt.getSource() instanceof JTable)) {
                        Component c = findPrevFocus();
                        c.requestFocus();
                    }
                    break;
                }
                case KeyEvent.VK_ESCAPE: {
                    dispose();
                    break;
                }

            }
        }

//        @Override
//        public void keyReleased(KeyEvent evt){
//            if(evt.getSource().equals(txtDisc)||evt.getSource().equals(txtQty)||evt.getSource().equals(txtUnitPrice))
//                GeneralFunction.keyTyped(evt);
//        }
        public Component findNextFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component nextFocus = policy.getComponentAfter(root, c);
                if (nextFocus == null) {
                    nextFocus = policy.getDefaultComponent(root);
                }
                return nextFocus;
            }
            return null;
        }

        public Component findPrevFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component prevFocus = policy.getComponentBefore(root, c);
                if (prevFocus == null) {
                    prevFocus = policy.getDefaultComponent(root);
                }
                return prevFocus;
            }
            return null;
        }
    }

    private FocusListener txtFocusListener = new FocusListener() {
        public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.YELLOW);
                if ((e.getSource() instanceof JTextField && ((JTextField) e.getSource()).getName() != null && ((JTextField) e.getSource()).getName().equalsIgnoreCase("textEditor"))) {
                    ((JTextField) e.getSource()).setSelectionStart(0);
                    ((JTextField) e.getSource()).setSelectionEnd(((JTextField) e.getSource()).getText().length());

                }
            }
        }

        public void focusLost(FocusEvent e) {
            if (e.getSource().getClass().getSimpleName().equalsIgnoreCase("JTextField")
                    || e.getSource().getClass().getSimpleName().equalsIgnoreCase("JFormattedTextField")) {
                ((JTextField) e.getSource()).setBackground(Color.WHITE);
            }
        }
    };
}
