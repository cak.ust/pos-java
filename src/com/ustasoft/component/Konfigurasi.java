/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.component;

/**
 *
 * @author ustadho
 */
public class Konfigurasi {
    public static Boolean TAMPILKAN_FAKTUR_SEBELUM_DICETAK = Boolean.TRUE;
    public static boolean TGL_DEFAULT_SYSTEM = Boolean.TRUE;
    public static boolean STATUS_SHIFT = true;
    public static boolean TAMPILKAN_HPP_PENJUALAN = Boolean.TRUE;
    public static boolean TUTUP_DIALOG_PEMBAYARAN = Boolean.FALSE;
    public static int JUMLAH_CETAK_FAKTUR_PENJUALAN = 1;
    public static String FORMAT_TANGGAL = "dd-MM-yyyy";
    public static String GUDANG_DEFAULT = "1";
    public static String PRINTER_KECIL ="";
    public static String PRINTER_BESAR = "";
    public static String PRINTER_BARCODE = "";
    public static Boolean PRINTER_BARCODE_PREVIEW = false;
}
