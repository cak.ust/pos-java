/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.config;

import com.ustasoft.component.Konfigurasi;
import com.ustasoft.pos.domain.Gudang;

/**
 *
 * @author ustadho
 */
public class SettingKoneksi {
    private final boolean tampilkanSebelumDicetak;
    private final boolean printerKecilDotMatrix;
    private final boolean printerBesarDotMatrix;
    private final boolean printerBarcodePreview;
    private final String printerKecil;
    private final String printerBesar;
    private final String printerBarcode;
    private final String idGudangDefault;
    private final String server;
    private final String port;
    private final String database;
    private final int marginKiri;
    private final int marginBawah;

    public SettingKoneksi(boolean tampilkanSebelumDicetak, boolean printerKecilDotMatrix, boolean printerBesarDotMatrix, boolean printerBarcodePreview, String printerKecil, String printerBesar, String printerBarcode, String idGudangDefault, String server, String port, String database, int marginKiri, int marginBawah) {
        this.tampilkanSebelumDicetak = tampilkanSebelumDicetak;
        this.printerKecilDotMatrix = printerKecilDotMatrix;
        this.printerBesarDotMatrix = printerBesarDotMatrix;
        this.printerBarcodePreview = printerBarcodePreview;
        this.printerKecil = printerKecil;
        this.printerBesar = printerBesar;
        this.printerBarcode = printerBarcode;
        this.idGudangDefault = idGudangDefault;
        this.server = server;
        this.port = port;
        this.database = database;
        this.marginKiri = marginKiri;
        this.marginBawah = marginBawah;
    }

    
    public boolean isTampilkanSebelumDicetak() {
        return tampilkanSebelumDicetak;
    }

    public boolean isPrinterKecilDotMatrix() {
        return printerKecilDotMatrix;
    }

    public boolean isPrinterBesarDotMatrix() {
        return printerBesarDotMatrix;
    }

    public String getPrinterKecil() {
        return printerKecil;
    }

    public String getPrinterBesar() {
        return printerBesar;
    }

    public boolean isPrinterBarcodePreview() {
        return printerBarcodePreview;
    }

    public String getPrinterBarcode() {
        return printerBarcode;
    }

    public String getIdGudangDefault() {
        return idGudangDefault;
    }

    public String getServer() {
        return server;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public int getMarginKiri() {
        return marginKiri;
    }

    public int getMarginBawah() {
        return marginBawah;
    }

    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SettingKoneksi ip = (SettingKoneksi) o;
        return  tampilkanSebelumDicetak == ip.isTampilkanSebelumDicetak() && 
            printerKecilDotMatrix==ip.isPrinterKecilDotMatrix()&& 
            printerBesarDotMatrix==ip.isPrinterBesarDotMatrix()&& 
            printerBarcodePreview == ip.isPrinterBarcodePreview()&& 
            printerKecil.equals(ip.getPrinterKecil()) && 
            printerBesar.equals(ip.getPrinterBesar()) && 
            printerBarcode.equals(ip.getPrinterBarcode()) && 
            idGudangDefault.equalsIgnoreCase(ip.getIdGudangDefault()) &&
            server.equals(ip.getServer()) && 
            database.equals(ip.getDatabase()) && 
            marginKiri == ip.getMarginKiri() && 
            marginBawah == ip.getMarginBawah()&& 
            port.equals(ip.getPort());
        
    }
    
}
