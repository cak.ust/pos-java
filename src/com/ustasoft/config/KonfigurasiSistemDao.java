/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.config;

import com.ustasoft.component.FrmSettingKoneksi;
import com.ustasoft.component.GeneralFunction;
import com.ustasoft.component.Konfigurasi;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import pos.MainForm;

/**
 *
 * @author ustadho
 */
public class KonfigurasiSistemDao {

    public InformasiPerusahaan getInformasiPerusahaan() {
        InformasiPerusahaan result = null;
        try (ResultSet rs = MainForm.conn.createStatement()
                .executeQuery("select * from m_setting ")) {
            if (rs.next()) {
                result = new InformasiPerusahaan(
                        rs.getString("nama_toko") == null ? "" : rs.getString("nama_toko"),
                        rs.getString("alamat1") == null ? "" : rs.getString("alamat1"),
                        rs.getString("alamat2") == null ? "" : rs.getString("alamat2"),
                        rs.getInt("id_kota"),
                        rs.getString("kota") == null ? "" : rs.getString("kota"),
                        rs.getString("telp1") == null ? "" : rs.getString("telp1"),
                        rs.getString("telp2") == null ? "" : rs.getString("telp2"),
                        rs.getString("email") == null ? "" : rs.getString("email"),
                        rs.getString("akun_laba_ditahan") == null ? "" : rs.getString("akun_laba_ditahan"),
                        rs.getBoolean("tgl_system_default"),
                        rs.getBoolean("status_shift")
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public void simpanInformasiPerusahaan(InformasiPerusahaan oldData, InformasiPerusahaan newData) {
        if(oldData.equals(newData)){
            System.out.println("Data informasi perusahaan sama dan tidak ada yang diupdate");
            return;
        }
        try {
            
            ResultSet rsInfoPerusahaan = MainForm.conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("select * from m_setting ");
            
            boolean isNew = !rsInfoPerusahaan.next();
            rsInfoPerusahaan.updateString("nama_toko", newData.getNamaToko());
            rsInfoPerusahaan.updateString("alamat1", newData.getAlamat1());
            rsInfoPerusahaan.updateString("alamat2", newData.getAlamat2());
            rsInfoPerusahaan.updateInt("id_kota", newData.getIdKota());
            rsInfoPerusahaan.updateString("kota", newData.getKota());
            rsInfoPerusahaan.updateString("telp1", newData.getTelp1());
            rsInfoPerusahaan.updateString("telp2", newData.getTelp2());
            rsInfoPerusahaan.updateString("email", newData.getEmail());
            rsInfoPerusahaan.updateString("akun_laba_ditahan", newData.getAkunLabaDitahan());
            rsInfoPerusahaan.updateBoolean("tgl_system_default", newData.isTglSystemDefault());
            rsInfoPerusahaan.updateBoolean("status_shift", newData.isStatusShift());
            Konfigurasi.TGL_DEFAULT_SYSTEM = newData.isTglSystemDefault();
            if(isNew)
                rsInfoPerusahaan.insertRow();
            else
                rsInfoPerusahaan.updateRow();
            
            rsInfoPerusahaan.close();
            rsInfoPerusahaan = null;
        } catch (SQLException ex) {
            Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public SettingPenjualan getSettingPenjualan(){
        SettingPenjualan result = null;
        try (ResultSet rs = MainForm.conn.createStatement()
                .executeQuery("select * from m_setting_penjualan ")) {
            if (rs.next()) {
                result = new SettingPenjualan(
                    rs.getString("ket_pembayaran")==null? "": rs.getString("ket_pembayaran"),
                    rs.getString("foot_note_faktur")==null? "": rs.getString("foot_note_faktur"),
                    rs.getString("disiapkan_oleh")==null? "": rs.getString("disiapkan_oleh"),
                    rs.getString("dibuat_oleh")==null? "": rs.getString("dibuat_oleh"),
                    rs.getString("diperiksa_oleh")==null? "": rs.getString("diperiksa_oleh"),
                    rs.getBoolean("tampilkan_hpp"),
                    rs.getBoolean("tutup_dialog_bayar_print"),
                    rs.getInt("jumlah_cetak_faktur")
                );
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
    public void simpanSettingPenjualan(SettingPenjualan oldData, SettingPenjualan newData) {
        if(oldData.equals(newData)){
            System.out.println("Data setting penjualan sama dan tidak ada yang diupdate");
            return;
        }
        try {
            
            try (ResultSet rs = MainForm.conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("select * from m_setting_penjualan ")) {
                boolean isNew = !rs.next();
                rs.updateString("ket_pembayaran", newData.getKetPembayaran());
                rs.updateString("dibuat_oleh", newData.getDibuatOleh());
                rs.updateString("diperiksa_oleh", newData.getDiperiksaOleh());
                rs.updateString("disiapkan_oleh", newData.getDisiapkanOleh());
                rs.updateString("foot_note_faktur", newData.getCatatanKaki());
                rs.updateBoolean("tampilkan_hpp", newData.isTampilkanHpp());
                rs.updateBoolean("tutup_dialog_bayar_print", newData.isTutupDialogBayarPrint());
                rs.updateInt("jumlah_cetak_faktur", newData.getJumlahCetakFaktur());
                if(isNew)
                    rs.insertRow();
                else
                    rs.updateRow();
            }
        } catch (SQLException ex) {
            Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Properties prop = new Properties();
    public SettingKoneksi getSettingKoneksi(){
        SettingKoneksi result = null;
        InputStream input = null;

        try {
            input = new FileInputStream("setting.properties");

            // load a properties file
            prop.load(input);
            if (input != null) {
                result = new SettingKoneksi(
                        prop.getProperty("tampikan_sebelum_cetak") != null && prop.getProperty("tampikan_sebelum_cetak").equalsIgnoreCase("1"), 
                        prop.getProperty("jenis_printer_kecil") != null && prop.getProperty("jenis_printer_kecil").equalsIgnoreCase("1"),
                        prop.getProperty("jenis_printer_besar") != null && prop.getProperty("jenis_printer_besar").equalsIgnoreCase("1"), 
                        prop.getProperty("printer_barcode_preview") != null && prop.getProperty("printer_barcode_preview").equalsIgnoreCase("1"), 
                        prop.getProperty("printer_kecil"), 
                        prop.getProperty("printer_besar"), 
                        prop.getProperty("printer_barcode"), 
                        prop.getProperty("gudang"), 
                        prop.getProperty("server"), 
                        prop.getProperty("port"), 
                        prop.getProperty("database"), 
                        GeneralFunction.udfGetInt(prop.getProperty("margin_kiri")), 
                        GeneralFunction.udfGetInt(prop.getProperty("margin_bawah")));
            }
            
        } catch (Exception ex) {
            Logger.getLogger(FrmSettingKoneksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
     public void simpanSettingKoneksi(SettingKoneksi oldData, SettingKoneksi newData) {
        if(oldData.equals(newData)){
            System.out.println("Data setting koneksi sama dan tidak ada yang diupdate");
            return;
        }
         OutputStream output = null;

        try {

            output = new FileOutputStream("setting.properties");

            // set the properties value
            prop.setProperty("database", newData.getDatabase());
            prop.setProperty("server", newData.getServer());
            prop.setProperty("port", newData.getPort());
            prop.setProperty("gudang", newData.getIdGudangDefault());
            prop.setProperty("tampikan_sebelum_cetak", newData.isTampilkanSebelumDicetak()? "1": "0");
            prop.setProperty("jenis_printer_kecil", newData.isPrinterKecilDotMatrix()? "1": "0");
            prop.setProperty("jenis_printer_besar", newData.isPrinterBesarDotMatrix()? "1": "0");
            prop.setProperty("printer_kecil", newData.getPrinterKecil());
            prop.setProperty("printer_besar", newData.getPrinterBesar());
            prop.setProperty("margin_kiri", String.valueOf(newData.getMarginKiri()));
            prop.setProperty("margin_bawah", String.valueOf(newData.getMarginBawah()));

            // save properties to project root folder
            prop.store(output, null);
            
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
     
     public List<SettingMenu> getSettingMenu(String menuGroup, String username) {
        String sQry="select list.id, menu_description ,coalesce(can_insert, false) as can_insert, coalesce(can_update, false) as can_update, " +
                "coalesce(can_delete, false) as can_delete, coalesce(can_read, false) as can_read, coalesce(can_print, false) as can_print, " +
                "coalesce(can_correction, false) as can_correction " +
                "from m_menu_list list  " +
                (menuGroup.equalsIgnoreCase("")?  "" : "inner join m_menu_list_grouping g on g.menu_id=list.id "
                + "and g.group_id='"+menuGroup+"' ") +
                "left join m_menu_authorization auth on auth.menu_id=list.id and user_name='"+username+"' " +
                "where module_name='POS' " +
                "order by kode";
        List<SettingMenu> results = new ArrayList<>();
        try {
            try (ResultSet rs = MainForm.conn.createStatement().executeQuery(sQry)) {
                while(rs.next()) {
                    results.add(new SettingMenu(
                            rs.getInt("id"),
                            username,
                            rs.getString("menu_description"),
                            rs.getBoolean("can_insert"),
                            rs.getBoolean("can_update"),
                            rs.getBoolean("can_delete"),
                            rs.getBoolean("can_read"),
                            rs.getBoolean("can_print"),
                            rs.getBoolean("can_correction")));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return results;
     }
     
     public void simpanSettingMenu(List<SettingMenu> oldData, List<SettingMenu> newData) {
        boolean dataSama = true;
         for(int i=0; i<newData.size(); i++){
            if(!newData.get(i).equals(oldData.get(i))) {
                dataSama = false;
                break;
            }
        }
        if(dataSama){
            System.out.println("Data setting menu sama dan tidak ada yang diupdate");
            return;
        }
         OutputStream output = null;

         ResultSet rs;
         boolean baru = false;
         for(SettingMenu sm: newData) {
             
            try {
                String sql = "select * from m_menu_authorization where menu_id="+sm.getMenuId() + " " +
                                "and user_name='"+sm.getUserName()+"'";
                rs = MainForm.conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)
                        .executeQuery(sql);
                if(rs.next()){
                    baru = true;
                }
                rs.updateInt("menu_id", sm.getMenuId());
                rs.updateString("user_name", sm.getUserName());
                rs.updateBoolean("can_insert", sm.isCanInsert());
                rs.updateBoolean("can_update", sm.isCanUpdate());
                rs.updateBoolean("can_delete", sm.isCanDelete());
                rs.updateBoolean("can_read", sm.isCanRead());
                rs.updateBoolean("can_print", sm.isCanPrint());
                rs.updateBoolean("can_correction", sm.isCanCorrection());
                
                if(baru){
                    rs.moveToInsertRow();
                }else{
                    rs.updateRow();
                }
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(KonfigurasiSistemDao.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
         if (output != null) {
             try {
                 output.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
    }
}
