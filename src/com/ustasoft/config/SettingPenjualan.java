/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.config;

/**
 *
 * @author ustadho
 */
public class SettingPenjualan {
    private final String ketPembayaran;
    private final String catatanKaki;
    private final String disiapkanOleh;
    private final String dibuatOleh;
    private final String diperiksaOleh;
    private final boolean tampilkanHpp;
    private final boolean tutupDialogBayarPrint;
    private final int jumlahCetakFaktur;

    public SettingPenjualan(String ketPembayaran, String catatanKaki, String disiapkanOleh, String dibuatOleh, 
            String diperiksaOleh, boolean tampilkanHpp, boolean tutupDialogBayarPrint, int jmlCetak) {
        this.ketPembayaran = ketPembayaran;
        this.catatanKaki = catatanKaki;
        this.disiapkanOleh = disiapkanOleh;
        this.dibuatOleh = dibuatOleh;
        this.diperiksaOleh = diperiksaOleh;
        this.tampilkanHpp = tampilkanHpp;
        this.tutupDialogBayarPrint = tutupDialogBayarPrint;
        this.jumlahCetakFaktur = jmlCetak;
    }

    public String getKetPembayaran() {
        return ketPembayaran;
    }

    public String getCatatanKaki() {
        return catatanKaki;
    }

    public String getDisiapkanOleh() {
        return disiapkanOleh;
    }

    public String getDibuatOleh() {
        return dibuatOleh;
    }

    public String getDiperiksaOleh() {
        return diperiksaOleh;
    }

    public boolean isTampilkanHpp() {
        return tampilkanHpp;
    }

    public boolean isTutupDialogBayarPrint() {
        return tutupDialogBayarPrint;
    }

    public int getJumlahCetakFaktur() {
        return jumlahCetakFaktur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SettingPenjualan ip = (SettingPenjualan) o;
        return  ketPembayaran.equals(ip.getKetPembayaran()) && 
                catatanKaki.equals(ip.getCatatanKaki()) && 
                disiapkanOleh.equals(ip.getDisiapkanOleh()) &&
                dibuatOleh.equals(ip.getDibuatOleh()) && 
                tampilkanHpp == ip.isTampilkanHpp() && 
                tutupDialogBayarPrint == ip.isTutupDialogBayarPrint()&& 
                diperiksaOleh.equals(ip.getDiperiksaOleh()) &&
                jumlahCetakFaktur == ip.getJumlahCetakFaktur();
    }
    
    
}
