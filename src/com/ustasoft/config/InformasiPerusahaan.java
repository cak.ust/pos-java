/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.config;

/**
 *
 * @author ustadho
 */
public class InformasiPerusahaan {
    private final String namaToko;
    private final String alamat1;
    private final String alamat2;
    private final int idKota;
    private final String kota;
    private final String telp1;
    private final String telp2;
    private final String email;
    private final String akunLabaDitahan;
    private final boolean tglSystemDefault;
    private final boolean statusShift;

    public InformasiPerusahaan(String namaToko, String alamat1, String alamat2, int idKota, String kota, String telp1, String telp2, String email, String akunLabaDitahan, boolean tglSystem, boolean statusShift) {
        this.namaToko = namaToko;
        this.alamat1 = alamat1;
        this.alamat2 = alamat2;
        this.idKota = idKota;
        this.kota = kota;
        this.telp1 = telp1;
        this.telp2 = telp2;
        this.email = email;
        this.akunLabaDitahan = akunLabaDitahan;
        this.tglSystemDefault = tglSystem;
        this.statusShift = statusShift;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public String getAlamat1() {
        return alamat1;
    }

    public String getAlamat2() {
        return alamat2;
    }

    public int getIdKota() {
        return idKota;
    }

    public String getKota() {
        return kota;
    }

    public String getTelp1() {
        return telp1;
    }

    public String getTelp2() {
        return telp2;
    }

    public String getEmail() {
        return email;
    }

    public String getAkunLabaDitahan() {
        return akunLabaDitahan;
    }

    public boolean isTglSystemDefault() {
        return tglSystemDefault;
    }

    public boolean isStatusShift() {
        return statusShift;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InformasiPerusahaan ip = (InformasiPerusahaan) o;
        return namaToko.equals(ip.getNamaToko()) &&
                alamat1.equals(ip.getAlamat1()) && 
                alamat2.equals(ip.getAlamat2()) && 
                idKota == ip.getIdKota() &&
                kota.equals(ip.getKota()) &&
                telp1.equals(ip.getTelp1()) && 
                telp2.equals(ip.getTelp2()) && 
                email.equals(ip.getEmail()) && 
                akunLabaDitahan.equalsIgnoreCase(ip.getAkunLabaDitahan()) && 
                tglSystemDefault == ip.isTglSystemDefault() && 
                statusShift == ip.isStatusShift();
    }
}
