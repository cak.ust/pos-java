/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.config;

/**
 *
 * @author ustadho
 */
public class SettingMenu {
    private final int menuId;
    private final String userName;
    private final String menuDescription;
    private final boolean canInsert;
    private final boolean canUpdate;
    private final boolean canDelete;
    private final boolean canRead;
    private final boolean canPrint;
    private final boolean canCorrection;

    public SettingMenu(int menuId, String userName, String menuDescription, boolean canInsert, boolean canUpdate, boolean canDelete, boolean canRead, boolean canPrint, boolean canCorrection) {
        this.menuId = menuId;
        this.userName = userName;
        this.menuDescription = menuDescription;
        this.canInsert = canInsert;
        this.canUpdate = canUpdate;
        this.canDelete = canDelete;
        this.canRead = canRead;
        this.canPrint = canPrint;
        this.canCorrection = canCorrection;
    }


    public int getMenuId() {
        return menuId;
    }

    public String getUserName() {
        return userName;
    }

    public String getMenuDescription() {
        return menuDescription;
    }

    public boolean isCanInsert() {
        return canInsert;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public boolean isCanCorrection() {
        return canCorrection;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SettingMenu ip = (SettingMenu) o;
        return  userName.equals(ip.getUserName())&& 
            canInsert == ip.isCanInsert()&& 
            canUpdate == ip.isCanUpdate() && 
            canDelete == ip.isCanDelete() && 
            canRead == ip.isCanRead() && 
            canPrint == ip.isCanPrint() &&
            canCorrection == ip.isCanCorrection();
        
    }
}
